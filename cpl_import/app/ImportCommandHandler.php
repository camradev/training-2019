<?php
namespace App;

use Exception;
use PDO;
use PDOException;
use Webmozart\Console\Api\Args\Args;
use Webmozart\Console\Api\Command\Command;
use Webmozart\Console\Api\IO\IO;

class ImportCommandHandler
{

    private $env;

    /**
     * Handle the Import command
     * @param Args $args
     * @param IO $io
     * @param Command $command
     * @return int
     */
    public function handle(Args $args, IO $io, Command $command)
    {
        $this->env = getenv('TRAININGPORTAL_ENV');

        // Connect to the databases
        $trainingPortalConnection = self::connectToTrainingPortalDatabase($io);
        if ($trainingPortalConnection == null) return 1;

        // Connect to FTP server
        $cplSftpConnection = self::connectToCplSftp($io);

        // Check for remote file and retrieve
        $this->RetrieveFile($io, $cplSftpConnection);


        $io->write('Closing connections...');
        // Close the db connection
        $trainingPortalConnection = null;
        // Close the ftp connection
        ftp_close($cplSftpConnection);
        $io->writeLine('<success>done</success>');
        return 0;
    }

    private static function connectToTrainingPortalDatabase(IO $io)
    {
        $trainingPortalServer = getenv('TRAININGPORTAL_SERVER');
        $trainingPortalDatabase = getenv('TRAININGPORTAL_DATABASE');
        $trainingPortalUsername = getenv('TRAININGPORTAL_USERNAME');
        $trainingPortalPassword = getenv('TRAININGPORTAL_PASSWORD');

        $io->writeLine("TRAININGPORTAL_SERVER=<info>$trainingPortalServer</info>");
        $io->writeLine("TRAININGPORTAL_DATABASE=<info>$trainingPortalDatabase</info>");
        $io->writeLine("TRAININGPORTAL_USERNAME=<info>$trainingPortalUsername</info>");
        $io->write("Opening TrainingPortal connection...");
        /** @var PDO $trainingPortalConnection */
        $trainingPortalConnection = null;
        try {
            $trainingPortalConnection = new PDO("mysql:host=$trainingPortalServer;dbname=$trainingPortalDatabase", $trainingPortalUsername, $trainingPortalPassword);
        } catch (PDOException $e) {
            $io->errorLine("<error>Error connecting: " . $e->getMessage() . "</error>");
            return null;
        }
        // Make exceptions thrown
        $trainingPortalConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $io->writeLine('<success>connected</success>');
        return $trainingPortalConnection;
    }

    private static function connectToCplSftp(IO $io)
    {
        $cplFtpHost = getenv('CPL_FTP_HOST');
        $cplFtpPort = getenv('CPL_FTP_PORT');
        $cplFtpUsername = getenv('CPL_FTP_USER');
        $cplFtpPassword = getenv('CPL_FTP_PASSWORD');

        $io->writeLine("CPL_FTP_HOST=<info>$cplFtpHost</info>");
        $io->writeLine("CPL_FTP_PORT=<info>$cplFtpPort</info>");
        $io->writeLine("CPL_FTP_USER=<info>$cplFtpUsername</info>");
        $io->write("Opening CPL FTP connection...");

        $cplFtpConnection = null;
        try {
            // set up basic ssl connection
            $cplFtpConnection = ftp_ssl_connect($cplFtpHost, $cplFtpPort);

            // login with username and password
            $login_result = ftp_login($cplFtpConnection, $cplFtpUsername, $cplFtpPassword);
        } catch (Exception $e) {
            $io->errorLine("<error>Error connecting: " . $e->getMessage() . "</error>");
            return null;
        }
        // Make exceptions thrown
        #$cplFtpConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $io->writeLine('<success>connected</success>');

        return $cplFtpConnection;
    }

    private function RetrieveFile(IO $io, $cplFtpConnection)
    {
        //enabling passive mode
        ftp_pasv($cplFtpConnection, true);

        try {
            $dir = '/' . $this->env;
            ftp_chdir($cplFtpConnection, $dir);
        } catch (Exception $e) {
            $io->errorLine("<error>Error changing remote directory: " . $e->getMessage() . "</error>");
            return null;
        }
        $io->writeLine("Remote directory changed to <info>$dir</info>");

        // Get file list
        $contents = ftp_nlist($cplFtpConnection, '.');
        $io->writeLine("Checking for files...");
        $matches = [];
        foreach ($contents as $value) {
            $match = [];
            preg_match("/^CAMRA_\d{4}\d{2}\d{2}\d{2}\d{2}.csv/", $value, $match);
            if ($match) {
                $matches[] = $match[0];
            }
        }
        $matchCount = count($matches);
        $io->writeLine("Files found: <info>" . $matchCount . "</info>");

        sort($matches);

        if ($matches) {
            $io->writeLine("Processing each file...");

            // Process each match
            foreach ($matches as $key => $file) {
                if ($key == $matchCount - 1) {
                    // File is the latest of the set, try download/delete
                    $io->writeLine("<info>$file</info> - is most recent");
                    $local_destination = '/tmp/' . $file;
                    // Try to download and save locally
                    $maxRetries = 5;
                    for ($retryCount = 1; $retryCount <= $maxRetries; $retryCount++) {
                        $io->write("...attempting download ($retryCount/$maxRetries)");
                        try {
                            $bool = ftp_get($cplFtpConnection, $local_destination, $file, FTP_BINARY);
                        } catch (Exception $ex) {
                            $bool = false;
                            $io->write("Caught exception: $ex->getMessage()");
                        }
                        if ($bool) {
                            $io->writeLine("...<success>success</success>");
                            break;
                        } else {
                            if ($retryCount==5){
                                $io->errorLine("...<error>failure</error>...aborting.");
                                return null;
                            } else {
                                $io->errorLine("...<error>failure</error>");
                            }
                        }
                    }

                    // Process it
                    $io->write("...importing contents into db");
                    if (1 == 1) {
                        $io->writeLine("...<success>success</success>");
                    } else {
                        $io->errorLine("...<error>failure</error>...aborting.");
                        return null;
                    }

                    $io->write("...archiving file locally");
                    // Create archive folder in current folder if missing
                    if (!is_dir('archive')) {
                        mkdir('archive', 0777, true);
                    }
                    // Archive it
                    if (rename('/tmp/' . $file, 'archive/' . $file)) {
                        $io->writeLine("...<success>success</success>");
                    } else {
                        $io->errorLine("...<error>failure</error>...aborting.");
                        return null;
                    }

                    $io->write("...deleting file from server");
                    // Try to delete the file
                    $result = ftp_delete($cplFtpConnection, $file);
                    if ($result) {
                        $io->writeLine("...<success>success</success>");
                    } else {
                        $io->errorLine("...<error>failure</error>...aborting.");
                        return null;
                    }
                } else {
                    // File isn't the latest
                    $io->writeLine("<info>$file</info> - is not most recent");
                    $io->write("...deleting");
                    // Try to delete the file
                    $result = ftp_delete($cplFtpConnection, $file);
                    if ($result) {
                        $io->writeLine("...<success>success</success>");
                    } else {
                        $io->errorLine("...<error>failure</error>...aborting.");
                        return null;
                    }
                }
            }
        }
    }
}