<?php
namespace App;
use Webmozart\Console\Api\Formatter\Style;
use Webmozart\Console\Config\DefaultApplicationConfig;

class CplImportApplicationConfig extends DefaultApplicationConfig
{
    protected function configure() {
        parent::configure();
        $this
            ->setName('cplimport')
            ->setDisplayName('CPL Course Status Import Tool (cplimport)')
            ->setVersion('0.1')
            ->addStyle(Style::tag('success')->fgGreen())
            ->addStyle(Style::tag('alert')->fgYellow())
            ->addStyle(Style::tag('info')->fgCyan())
        ;
        $this
            ->beginCommand('import')
            ->setDescription('Import status updates into training portal')
            ->setHandler(new ImportCommandHandler())
        ;
    }
}