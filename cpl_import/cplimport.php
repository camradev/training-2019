<?php
use App\CplImportApplicationConfig;
use Webmozart\Console\ConsoleApplication;
require __DIR__ . '/vendor/autoload.php';

loadAndCheckEnvironment();

$cli = new ConsoleApplication(new CplImportApplicationConfig());
$cli->run();

////////////////
// Script end //
////////////////

/**
 * Loads from the .env file and checks required values exist
 */
function loadAndCheckEnvironment()
{
    $dotEnv = new Dotenv\Dotenv(__DIR__);
    $dotEnv->load();
    $dotEnv
        ->required([
            'TRAININGPORTAL_SERVER',
            'TRAININGPORTAL_USERNAME',
            'TRAININGPORTAL_PASSWORD',
            'TRAININGPORTAL_DATABASE',
            'CPL_FTP_HOST',
            'CPL_FTP_PORT',
            'CPL_FTP_USER',
            'CPL_FTP_PASSWORD',
        ])
        ->notEmpty();

    $dotEnv
        ->required([
            'TRAININGPORTAL_ENV',
        ])
        ->allowedValues(['Live', 'Test']);



}
