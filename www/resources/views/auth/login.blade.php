@extends("layout")
@section("content")
    <form class="form-signin" method="POST" action="/auth/login">
        {!! csrf_field() !!}

        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="username" class="sr-only">Member ID</label>
        <input type="text" id="username" name="username" value="{{ old('username') }}" class="form-control"
               placeholder="Membership Number" required autofocus>

        <label for="password" class="sr-only">Password</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>

    @if(Session::has('message') || Session::has('errors'))
        <div id="flashModal" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title text-center">Access Denied</h4>
                    </div>
                    <div class="modal-body text-center">
                        <p>You do not have permissions to administrate this system.</p>

                        <p>Please contact the CAMRA IT support department.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    @endif
@endsection
@section('scripts')
    @if(Session::has('message') || Session::has('errors'))
        <script>
            $(document).ready(function () {
                $('#flashModal').modal('show');
            });
        </script>
    @endif
@endsection
