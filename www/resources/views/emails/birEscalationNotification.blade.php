<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CAMRA Training Portal - Budget Increase Request (Escalations)</title>
</head>
<body>
<p>Hi,</p>
<p>The following open budget increase requests are now over two weeks old and have been escalated:</p>
<p>With regards,</p>
<p>The CAMRA Support Team</p>

@foreach($requests as $request)
<hr>
<h3>Branch Name:</h3>
{{ $request->branch_name }}
<h3>Branch Member:</h3>
{{ $request->member_name }}
<h3>Request Description:</h3>
{{ $request->description ? $request->description : '<None specified>' }}
<h3>Request Date:</h3>
{{ $request->created_at }}
<p><a href="{{ route('manageBudgetDistribution') }}?regionFilter={{ $request->region_code }}"><button>Action Request</button></a></p>
@endforeach
</body>
</html>