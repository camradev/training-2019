<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CAMRA Training Portal - Allocation Notification</title>
</head>
<body>
<p>Hi {{ $memberForename }},</p>
<p>A training course has been allocated to you - please see below, the following details relating to the course and the provider.</p>
<p>With regards,</p>
<p>The CAMRA Support Team</p>
<hr>
<h3>Course Name:</h3>
{{ $courseName }}
<h3>Course Details:</h3>
{!! $courseEmailNotes !!}
<h3>Provider:</h3>
{{ $providerName }}
<h3>Provider Details:</h3>
{!! $providerEmailNotes !!}
</body>
</html>