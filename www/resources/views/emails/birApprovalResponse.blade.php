<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CAMRA Training Portal - Budget Increase Request</title>
</head>
<body>
<p>Hi {{ $memberForename }},</p>
<p>Your budget increase request has been reviewed, and was approved; additional budget was allocated.</p>
<p>With regards,</p>
<p>The CAMRA Support Team</p>
<hr>
<h3>Branch Name:</h3>
{{ $branchName }}
<h3>Request Description:</h3>
{{ $description ? $description : '<None specified>' }}
<h3>Status:</h3>
Approved
</body>
</html>