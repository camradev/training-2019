<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CAMRA Training Portal - Budget Increase Request</title>
</head>
<body>
<p>Hi {{ $volunteerDirectorForename }},</p>
<p>A budget increase request has been raised - please see the following details.</p>
<p><a href="{{ route('manageBudgetDistribution') }}?regionFilter={{ $regionCode }}"><button>Action Request</button></a></p>
<p>With regards,</p>
<p>The CAMRA Support Team</p>
<hr>
<h3>Branch Name:</h3>
{{ $branchName }}
<h3>Branch Member:</h3>
{{ $memberName }}
<h3>Request Description:</h3>
{{ $description ? $description : '<None specified>' }}
</body>
</html>