@extends("layout")
@section("content")
    <div class="container">
        <div class="row">
            <h2><a href="{{ route("budgetOptions") }}">Budget management</a> - Manage Budget Distribution</h2>
        </div>
        <div id="app" v-cloak>
            <div class="text-center" :class="{ hidden : finishedLoading}">
                <h2>Loading...</h2>
                <i class="fa fa-refresh fa-spin fa-4x"></i>
            </div>
            <div :class="{ hidden : !finishedLoading}">
                <div id="tpUnallocatedBudgetContainer">
                    <div>Unallocated budget: £@{{ unallocatedBudget.toLocaleString() }}</div>
                </div>

                <div id="tpReviewAndSaveChanges">
                    <a class="btn btn-default" href="#save">
                        <span class="glyphicon glyphicon-chevron-down"></span> Review and save changes
                    </a>
                </div>

                <div class="row">
                    <div class="form-group form-inline">
                        <label for="selectedRegion">Region/Festivals filter</label>
                        <select v-model="selectedRegion" id="selectedRegion" class="form-control">
                            <option value="">-- ALL --</option>
                            <option v-for="region in regionBudgets" :value="region.code">@{{ region.name }}</option>
                        </select>
                </div>

                <div class="row" v-for="region in regionBudgets | filterBy selectedRegion in 'code' ">
                    <h3>@{{ region.name }}</h3>

                    <div class="tpRegionContainer">
                        <div v-for="branch in region.branches" class="tpBranch"
                             :class="{'tpBranch--changed': budgetHasChanged(branch)}">
                            <div class="tpBranch__name">@{{ branch.name }}</div>
                            <div class="tpBranch__originalAmount">Original:
                                £@{{ branch.budget.originalAmount.toLocaleString() }}</div>
                            <div v-show="budgetHasChanged(branch)"
                                 class="tpBranch__amount">New: £@{{ branch.budget.amount.toLocaleString() }}</div>
                            <div v-show="budgetHasChanged(branch)"
                                 class="tpBranch__delta">@{{ (budgetDelta(branch.budget) > 0 ? '+' : '') + budgetDelta(branch.budget).toLocaleString() }}</div>
                            <div class="tpBranch__used">Used: £@{{ branch.current_budget_used.toLocaleString() }}</div>

                            <div class="progress tpBranch__progressBar">
                                <div class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100"
                                     style="min-width: 2em;" :style="{width: currentUsagePercentage(branch) + '%'}"
                                     :class="[
                                    currentUsagePercentage(branch) > 90
                                    ? 'progress-bar-danger'
                                    : currentUsagePercentage(branch) > 75
                                        ? 'progress-bar-warning'
                                        : 'progress-bar-success'
                                  ]"
                                        >
                                    @{{ currentUsagePercentage(branch) }}%
                                </div>
                            </div>

                            <div class="tpBranch__modifierButtons btn-group btn-group-xs btn-group-vertical"
                                 role="group">
                                <button :disabled="cannotIncreaseBudget(branch)" @click="addBudget(branch.budget)"
                                type="button" class="btn btn-default">
                                <span class="glyphicon glyphicon-plus"></span>
                                </button>
                                <button :disabled="cannotReduceBudget(branch)" @click="subBudget(branch.budget)"
                                type="button" class="btn btn-default">
                                <span class="glyphicon glyphicon-minus"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <H4 class="tpRegionTotal">@{{ region.name == 'Festivals' ? 'Festivals' : 'Region' }} Total Allocated: £@{{ region.branches | sumAllocated }}</H4>
                </div>
                <div class="row" v-show="changedBudgets.length > 0">
                    <h3>Budget changes summary</h3>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Region</th>
                            <th>Branch</th>
                            <th>Original</th>
                            <th>Change</th>
                            <th>New</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr v-for="change in changedBudgets"
                            :class="[budgetDelta(change.branch.budget) < 0 ? 'danger' : 'success' ]">
                            <td>@{{ change.region_name }}</td>
                            <td>@{{ change.branch.name }}</td>
                            <td>£@{{ change.branch.budget.originalAmount.toLocaleString() }}</td>
                            <td>
                                £@{{ (budgetDelta(change.branch.budget) > 0 ? '+' : '') + budgetDelta(change.branch.budget).toLocaleString() }}</td>
                            <td>£@{{ change.branch.budget.amount.toLocaleString() }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <div class="row">
                    <div class="panel panel-default tpBudgetIncreaseRequests">
                        <div class="panel-heading">
                            <h1 class="panel-title"><strong>Budget Increase Requests</strong></h1>
                        </div>
                        <table v-if="budgetIncreaseRequests.length > 0" class="table table-striped">
                            <thead>
                            <tr>
                                <th>Branch</th>
                                <th>Description</th>
                                <th>Member Name</th>
                                <th>Created</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="instance in budgetIncreaseRequests">
                                <td>@{{ instance.branch_name }}</td>
                                <td>@{{ instance.description }}</td>
                                <td>@{{ instance.member_name }} (@{{ instance.member_id }})</td>
                                <td>@{{ instance.created_at }}</td>
                                <td>
                                    <button class="btn btn-danger" @click="deny(instance)">Deny request</button></td>
                            </tr>
                            </tbody>
                        </table>
                        <div v-else>
                            <div class="panel-body">
                                No outstanding requests.
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <a name="save"></a>
                    <button class="pull-right btn"
                            :class="[!canSave ? 'disabled' : '' ,canSave ? 'btn-success' : 'btn-default']" @click="
                    saveBudgets()">
                    Save @{{ changedBudgets.length == 0 ? "(no changes made)" : "" }}
                    </button>
                </div>
            </div>
        </div>
    </div>

    @include('courses.denyBudgetIncreaseRequestModal')

@endsection
@section("scripts")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.10/vue.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        Vue.config.debug = true;

        Vue.filter('sumAllocated',function(list){
            return list.reduce(function(total,item){
                return total+item.budget.amount;
            },0);
        });

        var tpvm = new Vue({
            el: '#app',
            data: {
                unallocatedBudget: 0,
                regionBudgets: [],
                budgetIncreaseRequests: [],
                step: 10,
                finishedLoading: false,
                saving: false,
                selectedRegion: "{{ $regionFilter }}"
            },
            computed: {
                canSave: function () {
                    return !this.saving && this.changedBudgets.length > 0;
                },
                changedBudgets: function () {
                    var result = [];
                    this.regionBudgets.forEach(function (region) {
                        region.branches.forEach(function (branch) {
                            if (branch.budget.amount != branch.budget.originalAmount) {
                                result.push({region_name: region.name, branch: branch})
                            }
                        })
                    }.bind(this));
                    return result;
                }
            },
            methods: {
                addBudget: function (budget) {
                    budget.amount += this.step;
                    this.unallocatedBudget -= this.step;
                },
                subBudget: function (budget) {
                    budget.amount -= this.step;
                    this.unallocatedBudget += this.step;
                },
                budgetDelta: function (budget) {
                    return budget.amount - budget.originalAmount;
                },
                budgetHasChanged: function (branch) {
                    return branch.budget.amount != branch.budget.originalAmount;
                },
                currentUsagePercentage: function (branch) {
                    return branch.budget.amount == 0 ? 0 : ((branch.current_budget_used / branch.budget.amount) * 100.0).toLocaleString('en-gb', {maximumSignificantDigits: 2});
                },
                cannotReduceBudget: function (branch) {
                    // We cannot reduce the budget if the amount is already 0 or reducing by the step would make it less than the current budget aleady used
                    return branch.budget.amount == 0 || ( branch.budget.amount - this.step < branch.current_budget_used );
                },
                cannotIncreaseBudget: function (branch) {
                    // We cannot use budget if removing the step would go below 0
                    return this.unallocatedBudget - this.step < 0;
                },
                deny: function (request) {
                    // Set those properties in the modal dialog from the vue row
                    $('#denyModalDescription').val(request.description);
                    $('#denyModalBranchCode').val(request.branch_code);
                    $('#denyModalBranchName').val(request.branch_name);
                    $('#denyRequestModal').modal('show');
                },
                removeRequest: function (branchCode) {
                    // This is called by jQuery below. Hunts for the BIR array entry to remove based on the branchcode
                    var requestToRemove;
                    this.budgetIncreaseRequests.forEach(function (request) {
                        if (request.branch_code == branchCode) {
                            requestToRemove = request;
                        }
                    });
                    if (requestToRemove) {
                        this.budgetIncreaseRequests.$remove(requestToRemove);
                    }
                },
                saveBudgets: function () {
                    // Show the notification for general error
                    var notice = (new PNotify({
                        // Note: There is a dependency on the History module being compiled in, for this modal to work!
                        title: 'Confirmation Needed',
                        text: 'Are you sure?',
                        icon: 'glyphicon glyphicon-question-sign',
                        confirm: {confirm: true},
                        buttons: {closer: false, sticker: false},
                        //type: 'info',
                        addclass: 'stack-modal',
                        stack: {'dir1': 'down', 'dir2': 'right', 'modal': true},
                        history: {history: false}
                    })).get()
                            .on('pnotify.confirm', function () {
                                // Prep the data

                                // Disable the saving button
                                this.saving = true;
                                loadingPanel.show();

                                var budgetChanges = [];

                                this.changedBudgets.forEach(function (change) {
                                    budgetChanges.push({
                                        'branch_code': change.branch.code,
                                        'new_amount': change.branch.budget.amount,
                                        'request_id': this.getRequestIdByBranchCode(change.branch.code)
                                    });
                                }.bind(this));

                                $.post('{{ route("saveBudgetJson") }}', {budgetChanges: budgetChanges})
                                        .done(function () {
                                            this.loadBudgets();
                                            this.saving = false;
                                        }.bind(this))
                                        .fail(function (data) {
                                            console.log(data);
                                            alert("error: " + data.responseJSON.message);
                                            this.saving = false;
                                        }.bind(this))
                                        .always(function(){
                                            // Regardless of whether we successfully saved or failed, hide the loading panel.
                                            loadingPanel.hide();
                                        })
                            }.bind(this));
                },
                loadBudgets: function () {
                    this.finishedLoading = false;
                    $.getJSON('{{ route("getBudgetJson") }}', function (data) {
                        this.regionBudgets = data.regionBudgets;
                        // set the originalAmount to the same as the amount
                        this.regionBudgets.forEach(function (region) {
                            region.branches.forEach(function (branch) {
                                if (branch.budget == null) {
                                    branch.budget = {amount:0, originalAmount: 0}
                                } else
                                {
                                    branch.budget.originalAmount = branch.budget.amount;
                                }
                            });
                        });
                        this.unallocatedBudget = data.unallocatedBudget;
                        this.budgetIncreaseRequests = data.budgetIncreaseRequests;

                        this.finishedLoading = true;
                    }.bind(this));

                    {{--$.getJSON('{{ route("getBudgetIncreaseRequestsJson") }}', function (data) {--}}
                    {{--this.budgetIncreaseRequests = data.budgetIncreaseRequests;--}}
                    {{--this.finishedLoading = true;--}}
                    {{--}.bind(this));--}}
                },
                getRequestIdByBranchCode: function (branchCode) {
                    // Loop through the BIRs looking for the supplied branch code; if found, return the ID, otherwise null
                    for (var i = 0, len = this.budgetIncreaseRequests.length; i < len; i++) {
                        if (this.budgetIncreaseRequests[i].branch_code === branchCode)
                            return this.budgetIncreaseRequests[i].id; // Return as soon as the object is found
                    }
                }
            },
            created: function () {
                this.loadBudgets();
            }
        });

        // On document ready
        $(function () {
            // Override form submission for the modal deny request form
            $('#denyBudgetIncreaseRequestForm').submit(function (e) {
                e.preventDefault();
                // Grab values from the form
                var branchCode = $('#denyModalBranchCode').val();
                var branchName = $('#denyModalBranchName').val();
                var reasonCode = $('#denyModalReason').val();

                this.saving = true;

                // Make the ajax request
                $.post('{{ route("denyBudgetIncreaseRequest") }}', {
                    denyModalBranchCode: branchCode,
                    denyModalBranchName: branchName,
                    denyModalReason: reasonCode
                })
                        .done(function (data) {
                            //console.log(data);

                            $('#denyRequestModal').modal('hide');

                            // Show the notification for successful deny
                            var notice = new PNotify({
                                title: 'Request Updated',
                                text: data.message,
                                type: 'success',
                                animate: {animate: true, in_class: 'slideInDown', out_class: 'slideOutUp'},
                                buttons: {closer: false, sticker: false}
                            });
                            notice.get().click(function () {
                                notice.remove();
                            });

                            // Tell Vue to remove the request with the given branchcode
                            tpvm.removeRequest($('#denyModalBranchCode').val());

                            this.saving = false;
                        }.bind(this))
                        .fail(function (data) {
                            //console.log(data);

                            // Show the notification for general error
                            var notice = new PNotify({
                                title: 'Request Failed',
                                text: data.responseJSON ? data.responseJSON.message : (data.status + ' ' + data.statusText),
                                type: 'error',
                                animate: {animate: true, in_class: 'slideInDown', out_class: 'slideOutUp'},
                                hide: false,
                                buttons: {closer: false, sticker: false}
                            });
                            notice.get().click(function () {
                                notice.remove();
                            });

                            $('#denyRequestModal').modal('hide');
                            this.saving = false;
                        }.bind(this))
                    // There's a second onSubmit handler that shows loading panel so always ensure this runs last to close it
                        .always(function () {
                            loadingPanel.hide();
                        }
                );
            })
        });

    </script>
@endsection