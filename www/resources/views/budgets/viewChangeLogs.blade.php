@extends("layout")
@section("head")
    <style>
        .tpLogTableContainer {
            margin: 10px;
        }

        #logTable select {
            color: black;
        }

        td.tpAmount:before {
            content: "£";
        }

        .tpAmount {

        }
    </style>

@endsection
@section("content")
    <div class="container-fluid">
        <div class="row tpLogTableContainer">
            <h2><a href="{{ route("budgetOptions") }}">Budget management</a> - Budget change logs</h2>

            <div class="form-group form-inline">
                <label for="tpYearSelection">Year</label>
                <select id="tpYearSelection" class="form-control">
                    @foreach ($years as $year)
                        <option>{{ $year }}</option>
                    @endforeach
                </select>
            </div>

            <div id="logTable"></div>

        </div>
    </div>
@endsection
@section("scripts")
    <script src="/jtable.2.4.0/jquery.jtable.js" type="text/javascript"></script>

    <script>
        $(document).ready(function () {
            var tableContainer = $('#logTable');
            tableContainer.jtable({
                title: 'Budget Changes',
                actions: {
                    listAction: '{{ route("viewBudgetChangeLogsJson") }}?_token={{  csrf_token() }}',
                },
                sorting: false,
                paging: true,
                fields: {
                    id: {
                        key: true,
                        title: "Id",
                        width: '5%'
                    },
                    budget_event_id: {
                        title: "Event Id",
                        display: function (data) {
                            return "<span title='" + data.record.budget_event_id + "'>..." + data.record.budget_event_id.substr(-4) + '</span>';
                        },
                        listClass: "text-nowrap text-right",
                        width: '5%'
                    },
                    branch_code: {
                        title: "Branch/Festival Code",
                        listClass: "text-nowrap"
                    },
                    branch_description: {
                        title: "Description",
                        listClass: "text-nowrap"
                    },
                    budget_year: {
                        title: "Year",
                        width: '5%',
                        listClass: "text-center"
                    },
                    old_amount: {
                        title: "Old Amount",
                        listClass: "tpAmount text-right"
                    },
                    new_amount: {
                        title: "New Amount",
                        listClass: "tpAmount text-right"
                    },
                    increase_request_id: {
                        title: "Increase Request ID",
                        listClass: "text-right"
                    },
                    changed_by_member_id: {
                        title: "Member Id",
                        listClass: "text-center"
                    },
                    changed_by_member_name: {
                        title: "Member Name",
                        listClass: "text-nowrap"
                    },
                    updated_at: {
                        title: 'Date',
                        type: 'time',
                        listClass: "text-nowrap"
                    }
                }
            });
            tableContainer.jtable('load');

            $("#tpYearSelection").change(function () {
                var year = $(this).val();
                $("#logTable").jtable('load', {filterYear: year});
            })
        });
    </script>
@endsection