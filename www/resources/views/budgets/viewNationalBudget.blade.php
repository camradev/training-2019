@extends("layout")
@section("content")
    <div class="container">
        <div class="row">
            <h2><a href="{{ route("budgetOptions") }}">Budget management</a> - National Budget</h2>
        </div>
        <div class="row">
            <div id="app" class="col-sm-8 col-sm-offset-2" v-cloak>
                @if (isset($budgetAmount))
                    <div class="row">
                        <div class="well">
                            <p>
                                This page is used to update the national budget.<br/>
                                Please note that it is not possible to reduce the budget below the current allocation<br/>
                                To reduce the budget further, please use the
                                <a href="{{ route("manageBudgetDistribution") }}">budget distribution tool</a> to deallocate existing budget.
                            </p>
                        </div>

                        <form class="form-inline text-center" action="{{ route("updateNationalBudget") }}"
                              method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="year" value="{{ $year }}"/>
                            <div class="form-group{{ $errors->has('nationalBudget') ? ' has-error' : '' }}">
                                {{--<label class="control-label" for="nationalBudget">National Budget ({{ $budget->year }})</label>--}}

                                <div class="input-group">
                                    <div class="input-group-addon">£</div>
                                    <input type="text" class="form-control tpNationalBudgetInput" name="nationalBudget"
                                           placeholder="enter an amount"
                                           v-model="budgetAmount" number>
                                </div>
                            </div>
                            <button type="submit" name="submit" class="btn" :class="[canUpdate ? 'btn-success' : 'btn-default']"
                                    :disabled="!canUpdate" @click="confirmChanges($event)">
                                Update@{{ changeMade ? '' : '(no change made)'  }}
                            </button>
                        </form>
                        @if ($errors->has('nationalBudget'))
                            {!! $errors->first('nationalBudget', '<div class="text-center"><div class="tpErrorMessage">:message</div></div>') !!}
                        @endif
                    </div>
                    <div class="row">
                        <div class="tpNationalBudgetChangeTableContainer col-md-6 col-md-offset-3">
                            <div class="row">
                                <div class="col-md-6 text-right">
                                    Original<br/>
                                    Change<br/>
                                    New
                                </div>
                                <div class="col-md-6">
                                    £@{{ originalAmount.toLocaleString() }}<br/>
                                    £@{{ (budgetAmount - originalAmount).toLocaleString() }}<br/>
                                    £@{{ budgetAmount.toLocaleString() }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div id="usageChartBudgetAllocation" style="width: 450px; height: 400px;"></div>
                        </div>
                        <div class="col-md-6">
                            <div id="usageChartAllocationUsage" style="width: 450px; height: 400px;"></div>
                        </div>
                    </div>

                @else
                    <div class="alert alert-danger">
                        There appears to be no national budget defined for the year {{ $year  }}.
                        Please contact budget support to help resolve this issue.
                    </div>
                @endif
            </div>

        </div>
    </div>
@endsection
@section("scripts")
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.10/vue.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("visualization", "1", {packages: ["corechart"]});
        google.setOnLoadCallback(drawChartBudgetAllocation);
        google.setOnLoadCallback(drawChartAllocationUsage);

        function drawChartBudgetAllocation() {
            var data = google.visualization.arrayToDataTable([
                ['Type', 'Amount'],
                ['Allocated', {{ $allocated }}],
                ['Available', {{ $budgetAmount - $allocated }}]
            ]);

            // Create a formatter to have a £ and no decimal places
            var formatter = new google.visualization.NumberFormat({prefix: '£', fractionDigits: 0});
            // Apply the format to column 1 (0-indexed)
            formatter.format(data, 1);

            var options = {
                title: 'Current budget allocation',
                pieStartAngle: 50,
                pieHole: 0.5,
                slices: {1: {offset: 0.2}},
                pieSliceText: 'value',
                chartArea: {width: '90%', height: '60%'},
                legend: {
                    position: 'right'
                }
            };

            var chart = new google.visualization.PieChart(document.getElementById('usageChartBudgetAllocation'));
            chart.draw(data, options);
        }

        function drawChartAllocationUsage() {
            var data = google.visualization.arrayToDataTable([
                ['Type', 'Amount'],
                ['Spent', {{ $spent }}],
                ['Available', {{ $allocated - $spent }}]
            ]);

            // Create a formatter to have a £ and no decimal places
            var formatter = new google.visualization.NumberFormat({prefix: '£', fractionDigits: 0});
            // Apply the format to column 1 (0-indexed)
            formatter.format(data, 1);

            var options = {
                title: 'Allocated',
                pieStartAngle: 50,
                pieHole: 0.5,
                colors:['#dc3912','green'],
                slices: {1: {offset: 0.2}},
                pieSliceText: 'value',
                chartArea: {width: '90%', height: '60%'},
                legend: {
                    position: 'right'
                }
            };

            var chart = new google.visualization.PieChart(document.getElementById('usageChartAllocationUsage'));
            chart.draw(data, options);
        }

        Vue.config.debug = true;
        new Vue({
            el: '#app',
            data: {
                originalAmount: {{ $budgetAmount }},
                budgetAmount: '{{ !empty(old('nationalBudget')) ? old('nationalBudget') : $budgetAmount }}',
                allocated: {{ $allocated }}

            },
            computed: {
                canUpdate: function () {
                    return this.changeMade && this.budgetAmount >= this.allocated;
                },
                changeMade: function () {
                    return this.budgetAmount != this.originalAmount;
                }
            },
        methods: {
            confirmChanges: function (event) {
                if (!confirm('Are you sure?')) {
                    event.preventDefault();
                }
            }
        }
        });
    </script>
@endsection