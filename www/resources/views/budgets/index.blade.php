@extends("layout")
@section("content")
    <div class="container">
        <div class="row">
            <h2>Budget management</h2>
        </div>
        <div class="row">

            <div class="col-md-4">
                <h3>Manage budget distribution</h3>
                <p>Allows re-distribution of budget between branches or extra allocation from any remaining national budget.</p>
                <p><a class="btn btn-default" role="button"
                      href="{{ route("manageBudgetDistribution") }}">Manage budgets &raquo;</a></p>
            </div>
            <div class="col-md-4">
                <h3>Change national budget</h3>
                <p>Allows re-distribution of budget between branches or extra allocation from any remaining national budget.</p>
                <p><a class="btn btn-default" role="button"
                      href="{{ route("viewNationalBudget") }}">Change national budget &raquo;</a></p>
            </div>
            <div class="col-md-4">
                <h3>Budget change logs</h3>
                <p>View the logs of any changes to budgets through the budget distribution interface.</p>
                <p><a class="btn btn-default" role="button"
                      href="{{ route("viewBudgetChangeLogs") }}">View budget change logs &raquo;</a></p>
            </div>
        </div>
    </div>
@endsection