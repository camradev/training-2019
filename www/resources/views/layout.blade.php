<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Training Portal</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="/jtable.2.4.0/themes/metro/blue/jtable.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css">
    <link rel="stylesheet" href="/css/camra-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ URL::asset('/css/app.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('/css/all.css') }}">
    @yield('head')
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><span><img class="logo pull-left" src="/img/logo.png" alt="logo"></span>Training
                Management Portal</a>
        </div>
        @can('access-system')
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/">Dashboard</a></li>
                <li><a href="{{ route("courseAllocations") }}">Course Allocations</a></li>
                @can('view-budgets')
                <li><a href="{{ route("budgetOptions") }}">Budget Management</a></li>
                @endcan
                @can('view-reports')
                <li><a href="{{ route("reports") }}">Reports</a></li>
                @endcan
                <li><a href="/">Help</a></li>
                <li><a href="/auth/logout">@if (Auth::check()) [{{ Auth::user()->forename }} {{ Auth::user()->surname }}
                        ] @endif Logout</a></li>
            </ul>
        </div>
        @endcan
    </div>
</nav>
<div class="main-body">
    @include("flash")
    @yield('content')
</div>

<footer class="footer">
    <div class="container">
        <p class="text-muted text-center">Version: {{ Config::get('app.version') }}</p>
    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="{{ URL::asset('/js/all.js') }}"></script>

<script src="{{ URL::asset('js/jquery.table2excel.js') }}"></script>

<!-- Include Required Prerequisites -->
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script type="text/javascript">
    PNotify.prototype.options.styling = "bootstrap3";

    // Create the please wait modal after you submit a modal.
    var loadingPanel;
    loadingPanel = loadingPanel || (function () {
                var lpDialog = $("" +
                        "<div class='modal' id='lpDialog' data-backdrop='static' data-keyboard='false'>" +
                        "<div class='modal-dialog' >" +
                        "<div class='modal-content'>" +
                        "<div class='modal-header'><b>Processing...</b></div>" +
                        "<div class='modal-body'>" +
                        "<div class='progress'>" +
                        "<div class='progress-bar progress-bar-striped active' role='progressbar' aria-valuenow='100' aria-valuemin='100' aria-valuemax='100' style='width:100%'> "+
                        "Please Wait..."+
                        "</div>"+
                        "</div>"+
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</div>");
                return {
                    show: function () {
                        lpDialog.modal('show');
                    },
                    hide: function () {
                        lpDialog.modal('hide');
                    },

                };
            })();
</script>
@yield('scripts')
</body>
</html>