@extends("layout")
@section("content")
    <div class="container-fluid">
        <h1>Welcome to the training portal</h1>

        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                    <div class="col-lg-12">

                        <h3>Course Allocations</h3>
                        @include("courses.allocate")
                    </div>
                </div>
                @if (Auth::user()->isRegionAdmin())
                <div class="row">
                    <div class="col-lg-12">
                        @include("courses.regionSummary")
                    </div>
                </div>
                @endif
            </div>
            <div class="col-lg-3">
                <div class="row">
                    @can('change-budgets')
                    <div class="col-lg-12">
                        @include("courses.budgetAdminsBudgetIncreaseRequests")
                    </div>
                    @endcan
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        @include("courses.remainingBudgetAndBranchBudgetRequests")
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        @include("courses.providerSummary")
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
