@extends("layout")
@section("content")
    <style>
        #report1 {
            margin: 10px;
        }

        .branch-detail-row {
            display: none;
        }

        .branch-detail-row td {
            font-style: italic;
        }

        .first-branch-row {
            border-top: 2px solid grey;
        }

        .last-branch-row {
            border-bottom: 2px solid grey;
        }
    </style>

    <div class="container-fluid" id="report1">
        <div class="row">
            <h1><a href="{{ route('reports') }}">Reports</a> - Region Summary</h1>

            <form id="reportForm">
                <div class="form-group form-inline form-group-sm">
                    <div class="col-md-3">
                        <label for="daterange">Date
                            <div id="daterange" name="daterange" class="form-control">
                                <i class="glyphicon glyphicon-calendar"></i>
                                <span>Choose date</span> <i class="caret"></i>
                            </div>
                        </label>
                        <input type="hidden" name="startDate" id="startDate" class="form-control"
                               value="{{$startDate}}">
                        <input type="hidden" name="endDate" id="endDate" class="form-control" value="{{$endDate}}">
                    </div>
                    <div class="col-md-9">
                        <a href="{{ route('viewReportRegionSummaryForCsvExport', compact('startDate','endDate')) }}"
                           class="btn btn-primary pull-right" id="btnExport" data-toggle="tooltip"
                           title="Export as CSV">
                            Export as CSV <span class="glyphicon glyphicon-download-alt"></span>
                        </a>
                    </div>
                </div>
            </form>
        </div>

        <div class="row">
            @if($results)
                <table class="table table-striped table-bordered table-condensed">
                    <tr class="text-center">
                        <th class="text-center">Name</th>
                        <th class="text-center">Budget</th>
                        <th class="text-center">Total Course Allocations Cost</th>
                        <th class="text-center">Total Spent</th>
                        <th class="text-center">Courses Not Started</th>
                        <th class="text-center">Courses Started</th>
                        <th class="text-center">Courses Completed</th>
                    </tr>
                    @foreach($results as $region_code => $region_results)
                        <tr class="region-row">
                            <td>
                                <strong>{{ $region_results['name'] }}</strong>
                                <span class="pull-right">
                                    @if(count($region_results['branches']))
                                        <button id="{{$region_code}}" class="region-button btn btn-default">
                                            <i class="fa fa-chevron-down"></i>
                                        </button>
                                    @endif
                                </span></td>
                            <td class="text-right">&pound;{{number_format($region_results['regionTotalBudgetPounds'])}}</td>
                            <td class="text-right">&pound;{{number_format($region_results['regionTotalAllocationPounds'])}}</td>
                            <td class="text-right">&pound;{{number_format($region_results['regionTotalSpentPounds'])}}</td>
                            <td class="text-right">{{number_format($region_results['regionTotalCoursesNotStartedCount'])}}</td>
                            <td class="text-right">{{number_format($region_results['regionTotalCoursesStartedCount'])}}</td>
                            <td class="text-right">{{number_format($region_results['regionTotalCoursesCompletedCount'])}}</td>
                        </tr>
                        @foreach($region_results['branches'] as $branch_code => $branch_results)
                            <tr class="region_{{$region_code}} branch-detail-row">
                                <td class="text-right">{{$branch_results['name']}}</td>
                                <td class="text-right">&pound;{{number_format($branch_results['budget_pounds'])}}</td>
                                <td class="text-right">&pound;{{number_format($branch_results['total_allocation_pounds'])}}</td>
                                <td class="text-right">&pound;{{number_format($branch_results['total_spent_pounds'])}}</td>
                                <td class="text-right">{{number_format($branch_results['courses_not_started_count'])}}</td>
                                <td class="text-right">{{number_format($branch_results['courses_started_count'])}}</td>
                                <td class="text-right">{{number_format($branch_results['courses_completed_count'])}}</td>
                            </tr>
                        @endforeach

                    @endforeach
                </table>
            @else
                <table class="table table-striped table-bordered table-condensed">
                    <tr class="text-center">
                        <th class="text-center">Name</th>
                        <th class="text-center">Budget</th>
                        <th class="text-center">Total Course Allocations Cost</th>
                        <th class="text-center">Total Spent</th>
                        <th class="text-center">Courses Not Started</th>
                        <th class="text-center">Courses Started</th>
                        <th class="text-center">Courses Completed</th>
                    </tr>
                    <tr class="text-center">
                        <td colspan="7">No results found.</td>
                    </tr>
                </table>
            @endif
        </div>


    </div>
@endsection

@section("scripts")
    @parent
    <script>
        $(document).ready(function () {
            //////////////////////////////////////////
            // start rangepicker config
            var start_date;
            var end_date;

            start_date = moment('{{$startDate}}');
            end_date = moment('{{$endDate}}');
            //console.log(start_date);
            //console.log(end_date);

            dateRangeSelector = $('#daterange');
            dateRangeSelector.daterangepicker(
                    {
                        locale: {
                            format: 'YYYY-MM-DD'
                        },
                        ranges: {
                            @foreach($dates as $monthOffset => $date)
                            '{{ $date }}': [moment().startOf('month').subtract({{ $monthOffset }}, 'months').startOf('month'), moment().startOf('month').subtract({{ $monthOffset }}, 'months').endOf('month')],
                            @endforeach
                            'Current Month-to-Date': [moment().startOf('month'), moment().endOf('day')],
                            'Current Year-to-Date': [moment().startOf('year'), moment().endOf('day')],
                            'Previous year': [moment().startOf('year').subtract(1, 'years'), moment().endOf('year').subtract(1, 'years')]
                        },
                        startDate: start_date,
                        endDate: end_date
//                        ,
//                        isInvalidDate: function (start, end) {
//                            return moment(start).year() == moment(end).year();
//                        }
                    },
                    function (start, end) {
                        // This gets executed when a new date is picked
                        dateRangeSelector.find('span').html('Loading...');
                        // Update dates in the two hidden inputs and submit form
                        $('#startDate').val(start.format('YYYY-MM-DD'));
                        $('#endDate').val(end.format('YYYY-MM-DD'));
                        $('#reportForm').submit();
                    }
            );

            // Set the daterange text to the new dates
            dateRangeSelector.find('span').html(start_date.format('MMM D, YYYY') + ' - ' + end_date.format('MMM D, YYYY'));

            // end rangepicker config
            //////////////////////////////////////////////

//            // The report controller will handle if the date range spans multiple budget years, and reload with a flash message
//            dateRangeSelector.change(function () {
////                startDateYear = moment(picker.startDate.format('YYYY-MM-DD')).year();
////                endDateYear = moment(picker.endDate.format('YYYY-MM-DD')).year();
//
////                if (startDateYear != endDateYear) {
////
////                    alert('The start date and end date range must be within the same year, due to the use of year-based budgets; resetting report to Month-to-Date defaults.');
////                    // Change the date range
////                    $('#startDate').val(moment().startOf('month').format('YYYY-MM-DD'));
////                    $('#endDate').val( moment().endOf('day').format('YYYY-MM-DD'));
////                    // Update the daterange text
////                    dateRangeSelector.find('span').html(moment().startOf('month').format('MMM D, YYYY') + ' - ' + moment().endOf('day').format('MMM D, YYYY'));
////                }
//
//                $('#reportForm').submit();
//            });

            // Assign a click event to all the region buttons to toggle the hidden class on rows with region_xxx
            $('button.region-button').click(function () {
                // Find the row
                var row = $('.region_' + this.id);
                // Toggle showing the row
                row.toggle();
                // Update the chevron
                $(this).html("<i class='fa fa-chevron-" + (row.is(":hidden") ? "down" : "up") + "'></i>");

            });

            // Set the last row for each region's branch detail to have a specific class
            var regionButtons = $('.region-button');
            regionButtons.each(function () {
                $('tr.region_' + this.id).first().addClass("first-branch-row");
            });
            regionButtons.each(function () {
                $('tr.region_' + this.id).last().addClass("last-branch-row");
            });

        });
    </script>
@endsection
