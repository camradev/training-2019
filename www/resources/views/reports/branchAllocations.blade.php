@extends("layout")
@section("content")
    <div class="container-fluid">
        <h1><a href="{{ route('reports') }}">Reports</a> - Branch Allocations</h1>
        <div class="row">
            <div class="col-md-12">

                <style type="text/css">
                    .smallId {
                        font-size: 8pt;
                    }
                </style>

                <div class="row">
                    <form id="reportForm">
                        <div class="form-group form-inline form-group-sm">
                            <div class="col-md-3">
                                <label for="daterange">Date
                                    <div id="daterange" name="daterange" class="form-control">
                                        <i class="glyphicon glyphicon-calendar"></i>
                                        <span>Choose date</span> <i class="caret"></i>
                                    </div>
                                </label>
                                <input type="hidden" name="startDate" id="startDate" class="form-control" value="{{$startDate}}">
                                <input type="hidden" name="endDate" id="endDate" class="form-control" value="{{$endDate}}">
                            </div>
                            <div class="col-md-3">
                                <label for="statusName">Status
                                    <select class="form-control" id="statusName" name="statusName">
                                        <option @if($statusName==null)selected="selected" @endif value="">&lt;All&gt;</option>
                                        <option @if($statusName=="allocated")selected="selected" @endif value="allocated">Allocated</option>
                                        <option @if($statusName=="started")selected="selected" @endif value="started">Started</option>
                                        <option @if($statusName=="finished")selected="selected" @endif value="finished">Finished</option>
                                    </select>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <a href="{{ route('viewBranchAllocationsForCsvExport', compact('statusName','startDate','endDate')) }}"
                                   class="btn btn-primary pull-right" id="btnExport" data-toggle="tooltip" title="Export as CSV">
                                    Export as CSV <span class="glyphicon glyphicon-download-alt"></span>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>

                <div>
                    @if($results)
                        @foreach($results as $region_code => $region_results)
                            <h3>{{ $region_results['name'] }}</h3>
                            <table class="table" style="margin-left: 50px;">
                                <tr>
                                    <td style="border-top:none;">
                                        @foreach($region_results['branches'] as $branch_code => $branch_results)
                                        <h3>{{ $branch_results['name'] }}</h3>
                                        <table class="table table-striped table-bordered table-condensed">
                                            <tr class="text-center">
                                                <th class="text-center">Provider</th>
                                                <th class="text-center">Member Name (ID)</th>
                                                <th class="text-center">Course Cost</th>
                                                <th class="text-center">Status</th>
                                            </tr>
                                            @foreach($branch_results['allocations'] as $row)
                                                <tr>
                                                    <td>{{$row->course_provider_name}}</td>
                                                    <td>{{$row->member_name}} <span class="smallId">({{$row->member_id}})</span></td>
                                                    <td class="text-right">&pound;{{number_format($row->sum_course_cost_pounds)}}</td>
                                                    <td class="text-center">{{$row->status_name}}</td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td class="text-right" colspan="2"><strong>Summary for {{ $branch_results['name'] }}</strong></td>
                                                <td class="text-right">
                                                    <strong>&pound;</strong></td>
                                                <td></td>
                                            </tr>
                                        </table>
                                        @endforeach
                                        <h4><strong>Summary for region: &pound;{{ $region_results['regionTotal'] }}</strong></h4>
                                    </td>
                                </tr>
                            </table>

                        @endforeach
                    @else
                        <table class="table table-striped table-bordered table-condensed">
                            <tr class="text-center">
                                <th class="text-center">Provider</th>
                                <th class="text-center">Member Name (ID)</th>
                                <th class="text-center">Course Cost</th>
                                <th class="text-center">Status</th>
                            </tr>
                            <tr class="text-center">
                                <td colspan="4">No results found.</td>
                            </tr>
                        </table>
                    @endif
                </div>

            </div>
        </div>
    </div>
@endsection

@section("scripts")
    @parent
    <script>
        $(document).ready(function () {
            //////////////////////////////////////////
            // start rangepicker config
            var start_date;
            var end_date;

            start_date = moment('{{$startDate}}');
            end_date = moment('{{$endDate}}');
            //console.log(start_date);
            //console.log(end_date);

            dateRangeSelector = $('#daterange');
            dateRangeSelector.daterangepicker(
                    {
                        locale: {
                            format: 'YYYY-MM-DD'
                        },
                        ranges: {
                            @foreach($dates as $monthOffset => $date)
                            '{{ $date }}': [moment().startOf('month').subtract({{ $monthOffset }}, 'months').startOf('month'), moment().startOf('month').subtract({{ $monthOffset }}, 'months').endOf('month')],
                            @endforeach
                            'Current Month-to-Date': [moment().startOf('month'), moment().endOf('day')],
                            'Current Year-to-Date': [moment().startOf('year'), moment().endOf('day')],
                            'Previous year': [moment().startOf('year').subtract(1, 'years'), moment().endOf('year').subtract(1, 'years')]
                        },
                        startDate: start_date,
                        endDate: end_date
                    },
                    function (start, end) {
                        // This gets executed when a new date is picked
                        dateRangeSelector.find('span').html('Loading...');
                        // Update dates in the two hidden inputs and submit form
                        $('#startDate').val(start.format('YYYY-MM-DD'));
                        $('#endDate').val(end.format('YYYY-MM-DD'));
                        $('#reportForm').submit();
                    }
            );

            // Set the daterange text to the new dates
            dateRangeSelector.find('span').html(start_date.format('MMM D, YYYY') + ' - ' + end_date.format('MMM D, YYYY'));

            // end rangepicker config
            //////////////////////////////////////////////
            // When filter changes, submit the form
            $('#statusName').change(function () {
                $('#reportForm').submit();
            });
            dateRangeSelector.change(function () {
                $('#reportForm').submit();
            });

        });
    </script>
@endsection
