@extends("layout")
@section("content")
    <div class="container">
        <div class="row">
            <h2>Reports</h2>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h3>All allocations, by Month desc</h3>
                <p>All course allocation detail for a given month</p>
                <p><a class="btn btn-default" role="button"
                      href="{{ route("viewReportAllAllocations") }}">View report &raquo;</a></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h3>Branch allocations, by Region</h3>
                <p>All branch allocations summarised by member total cost for a given month, grouped by region.</p>
                <p><a class="btn btn-default" role="button"
                      href="{{ route("viewReportBranchAllocations") }}">View report &raquo;</a></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <h3>Summary of budget/course activity, by Region</h3>
                <p>Summarised course expenditure against budgets, with course completion stats, grouped by region.</p>
                <p><a class="btn btn-default" role="button"
                      href="{{ route("viewReportRegionSummary") }}">View report &raquo;</a></p>
            </div>
        </div>
    </div>
@endsection