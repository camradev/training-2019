@section('scripts')
    @foreach (['danger', 'warning', 'success', 'info'] as $key)
        @if(Session::has('alert-' . $key))
            <script>
                {{--IIFE to keep temp variables in private scope, so function can be repeated. Runs immediately.--}}
                {{--https://en.wikipedia.org/wiki/Immediately-invoked_function_expression--}}
                (function () {
                    var title = 'Notice';
                    var type = 'info';
                    var hide = true;
                    switch ('{{$key}}') {
                        case 'danger':
                            title = 'Error';
                            type = 'error';
                            hide = false;
                            break;
                        case 'warning':
                            title = 'Warning';
                            type = 'error';
                            hide = false;
                            break;
                        case 'success':
                            title = 'Success';
                            type = 'success';
                            break;
                        case 'info':
                            break;
                    }

                    // Show the notification
                    var notice = new PNotify({
                        title: title,
                        text: '{{ Session::get('alert-' . $key) }}',
                        type: type,
                        hide: hide,
                        animate: {animate: true, in_class: 'slideInDown', out_class: 'slideOutUp'},
                        buttons: {sticker: false}
                    });
                    notice.get().click(function () {
                        notice.remove();
                    });
                })();
            </script>
        @endif
    @endforeach
@append

{{--@foreach (['danger', 'warning', 'success', 'info'] as $key)
    @if(Session::has('alert-' . $key))

        <div class="container-fluid">
            <p class="alert alert-{{ $key }}">{{ Session::get('alert-' . $key) }}
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            </p>
        </div>
    @endif
@endforeach
--}}