<div class="form-group form-inline form-group-sm">
    <div class="row filters">
        <div class="col-sm-3">
            <label for="memberIdFilter">Member #</label>
            <div class="input-group">
                <input class="form-control filters" name="memberIdFilter" id="memberIdFilter" placeholder="Member #" maxlength="6" value="{{ $filters["memberIdFilter"] }}">
                <div class="input-group-addon"><i id="clearMemberIdFilter" class="fa fa-times-circle filterclear"></i></div>
            </div>
        </div>
        <div class="col-sm-3">
            <label for="memberNameFilter">Member Name</label>
            <div class="input-group">
                <input class="form-control filters" name="memberNameFilter" id="memberNameFilter" placeholder="Member Name" value="{{ $filters["memberNameFilter"] }}">
                <div class="input-group-addon"><i id="clearMemberNameFilter" class="fa fa-times-circle filterclear"></i></div>
            </div>
        </div>
        <div class="col-sm-3">
            <label for="branchNameFilter">Branch Name</label>
            <div class="input-group">
                <input class="form-control filters" name="branchNameFilter" id="branchNameFilter" placeholder="Branch Name" value="{{ $filters["branchNameFilter"] }}">
                <div class="input-group-addon"><i id="clearBranchNameFilter" class="fa fa-times-circle filterclear"></i></div>
            </div>
        </div>
        <div class="col-sm-3">
            <label for="courseNameFilter">Course Name</label>
            <div class="input-group">
                <input class="form-control filters" name="courseNameFilter" id="courseNameFilter" placeholder="Course Name" value="{{ $filters["courseNameFilter"] }}">
                <div class="input-group-addon"><i id="clearCourseNameFilter" class="fa fa-times-circle filterclear"></i></div>
            </div>
        </div>
    </div>
</div>


<div id="allocationsTable"></div>

<!-- Modal content-->
<div class="modal fade" id="modalMailerWarning" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Notification Mailer</h4>
            </div>
            <div class="modal-body">
                <p id="modalMessage"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">OK</button>
            </div>
        </div>

    </div>
</div>
@section("scripts")
    @parent
    <script src="/jtable.2.4.0/jquery.jtable.js" type="text/javascript"></script>
    <script>
        function pad(num, size){ return ('000000000' + num).substr(-size); }
        {{-- http://jtable.org/ApiReference/GeneralOptions --}}
        {{-- http://jtable.org/ApiReference/FieldOptions --}}
        {{-- http://jtable.org/ApiReference/GeneralOptions --}}
        $(document).ready(function () {
            var tableContainer = $('#allocationsTable');
            tableContainer.jtable({
                title: 'Course allocations',
                paging: true,
                pageSize: 15,
                messages: {
                    addNewRecord: 'Create new allocation'
                },
                actions: {
                    listAction: '{{ route("allocationsList") }}?_token={{  csrf_token() }}',
                    @if(!(Auth::user()->isSuperViewer() && !Auth::user()->isSuperUser()))
                    createAction: '{{ route("allocationsCreate") }}?_token={{  csrf_token() }}',
                    @endif
                    updateAction: '{{ route("allocationsUpdate") }}?_token={{  csrf_token() }}',
                    {{--deleteAction: '{{ route("allocationsDelete") }}?_token={{  csrf_token() }}'--}}
                },
                sorting: true,
                defaultSorting: "created_at desc",
                fields: {
                    id: {
                        key: true,
                        list: false
                    },
                    member_id: {
                        title: 'Member #',
                        edit: false,
                        display: function(data){
                            return pad(data.record.member_id, 6);
                        }
                    },
                    member_name: {
                        title: 'Member Name',
                        create: false,
                        edit: false
                    },
                    branch_name: {
                        title: 'Branch',
                        create: false,
                        edit: false
                    },
                    provider_id: {
                        title: 'Course provider',
                        options: '{{ route("providerOptions") }}?_token={{  csrf_token() }}',
                        list: false,
                        edit: false
                    },
                    course_name: {
                        title: 'Course',
                        create: false,
                        edit: false,
                        display: function (data) {
                            return data.record.course_name + ' (' + data.record.course_provider_name + ')';
                        },
                        listClass: "text-nowrap"
                    },
                    course_id: {
                        list: false,
                        edit: false,
                        title: 'Course',
                        dependsOn: 'provider_id',
                        options: function (data) {
                            if (data.source == 'list') {
                                // This could be used to return all courses, but we don't use it
                                return null;
                            }
                            return '{{ route("courseOptions") }}?providerId=' + data.dependedValues.provider_id + '&_token={{  csrf_token() }}'
                        }
                    },
                    course_cost_pounds: {
                        title: 'Cost',
                        create: false,
                        edit: false,
                        display: function (data) {
                            if (data && data.record) {
                                return "£" + data.record.course_cost_pounds;
                            }
                        },
                        listClass: "text-right"
                    },
                    status_id: {
                        title: 'Status',
                        list: false,
                        options: '{{ route("statusOptions") }}?_token={{  csrf_token() }}'
                    },
                    status_name: {
                        title: 'Status',
                        create: false,
                        edit: false
                    },
                    created_at: {
                        title: 'Date',
                        type: 'time',
                        create: false,
                        edit: false,
                        listClass: "text-nowrap"
                    },
                    budget_source: {
                        title: 'Budget',
                        display: function (data) {
                            if (data && data.record) {
                                //console.log(data.record);
                                // Capitalize first letter
                                return data.record.budget_source.charAt(0).toUpperCase() + data.record.budget_source.slice(1);
                            }
                        },
                        listClass: "text-center",
                        options: function (data) {
                            if (data.source == 'list') {
                                return [];
                            }
                            return '{{ route("budgetSourceOptions") }}?_token={{  csrf_token() }}';
                        }
                    }
                },
                recordAdded: function (event, data) {
                    if (typeof data.record._validationExceptionMessage !== 'undefined') {
                        //alert(data.record._validationExceptionMessage);
                        $('#modalMessage').html(data.record._validationExceptionMessage);
                        $('#modalMailerWarning').modal('show');
                    }
                    // Reload the page with the same filters applied
                    url = window.location.href.split('?')[0] +
                            '?memberIdFilter=' + $('#memberIdFilter').val() +
                            '&memberNameFilter=' + $('#memberNameFilter').val() +
                            '&branchNameFilter=' + $('#branchNameFilter').val() +
                            '&courseNameFilter=' + $('#courseNameFilter').val();
                    window.location.href = url;
                },
                formCreated: function (event, data) {
                    // Only potentially show the budget dropdown if not the create form
                    if (data.formType === "create") {
                        var options = data.form.find(":input#Edit-budget_source option");

                        // If we only have 1 option and its value is "branch" then we can hide
                        if (options.length === 1 && $(options[0]).val() === "branch") {
                            // Hide the div which has the branch source dropdown
                            data.form.find("div:contains('Budget')").hide();
                        }
                    } else {
                        // Hide anyway on Edit
                        data.form.find("div:contains('Budget')").hide();
                    }
                }
            });

            tableContainer.jtable('load');

            function loadTableWithFilters(){
                $("#allocationsTable").jtable("load", {
                    memberIdFilter: $("#memberIdFilter").val(),
                    memberNameFilter: $("#memberNameFilter").val(),
                    courseNameFilter: $("#courseNameFilter").val(),
                    branchNameFilter: $("#branchNameFilter").val()
                });
            }

            // Prep clear buttons
            $("#clearMemberIdFilter").click(function () {
                $("#memberIdFilter").val("");
                loadTableWithFilters();
            });
            $("#clearMemberNameFilter").click(function () {
                $("#memberNameFilter").val("");
                loadTableWithFilters();
            });
            $("#clearCourseNameFilter").click(function () {
                $("#courseNameFilter").val("");
                loadTableWithFilters();
            });
            $("#clearBranchNameFilter").click(function () {
                $("#branchNameFilter").val("");
                loadTableWithFilters();
            });

            // Prep filter class to update table results based on all filters.
            $(".filters").keyup(function () {
                loadTableWithFilters();
            });

            // Apply any filters which might be present on load.
            loadTableWithFilters();
        });
    </script>
@endsection