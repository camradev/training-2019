<!-- Modal -->
<div class="modal fade" id="editRequestModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Edit Budget Increase Request
                </h4>
            </div>

            <form role="form" id="editBudgetIncreaseRequestForm" action="{{route('editBudgetIncreaseRequest')}}">

                <!-- Modal Body -->
                <div class="modal-body">

                    <div class="form-group">
                        <label for="editModalDescription">Request Description (max 250 characters)</label>
                        <textarea class="form-control noresize" id="editModalDescription" rows="3" name="editModalDescription" maxlength="250"></textarea>
                        <input class="form-control" type="hidden" id="editModalBranchCode" name="editModalBranchCode">
                    </div>
                    <!--button type="submit" class="btn btn-default">Submit</button-->
                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary" id="editBudgetRequestFormSubmit">
                        Save changes
                    </button>
                </div>
            </form>
            @section('scripts')
                <script>
                    $(function(){
                        $('#editBudgetIncreaseRequestForm').submit(function (e){
                            $('#editRequestModal').modal('hide');
                            loadingPanel.show();
                        });
                    });
                </script>
            @append
        </div>
    </div>
</div>
