@if (isset($regionSummary['branches']))
<div class="panel panel-default dbPanel">
    <div class="panel-heading">
        <h3 class="panel-title">Budget allocation/usage summary for {{ Auth::user()->getAdminRegionNames() }}</h3>
    </div>
    <div class="panel-body">
        <table class="table table-condensed dbTable">
            <thead>
            <tr>
                <th>Branch name</th>
                <th class="text-right">Allocation</th>
                <th class="text-right">Usage</th>
                <th class="text-right">Progress</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($regionSummary['branches'] as $code => $branch)
            <tr>
                <td>{{ $branch['name'] }}</td>
                <td class="text-right">&pound;{{ number_format($branch['budget_allocated']) }}</td>
                <td class="text-right">&pound;{{ number_format($branch['budget_used']) }}</td>
                <td class="text-right">
                    <div class="progress dbProgress">
                        <div class="progress-bar {{
                                                        ($branch['progress_pct'] > 90
                                                        ? 'progress-bar-danger'
                                                        : $branch['progress_pct'] > 75
                                                            ? 'progress-bar-warning'
                                                            : 'progress-bar-success')
                                                    }}" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width:{{$branch['progress_pct']}}%;">
                            {{ ($branch['budget_allocated'] != 0) ? number_format($branch['progress_pct']).'%' : 'N/A'}}
                        </div>
                    </div>
                </td>
            </tr>
            @endforeach
            <tr>
                <td class="text-right"><i>Summary</i></td>
                <td class="text-right"><i>&pound;{{ number_format($regionSummary['total_allocation']) }}</i></td>
                <td class="text-right"><i>&pound;{{ number_format($regionSummary['total_usage']) }}</i></td>
                <td class="text-right">
                    <div class="progress dbProgress">
                        <div class="progress-bar {{
                                                        ($regionSummary['total_progress_pct'] > 90
                                                        ? 'progress-bar-danger'
                                                        : $regionSummary['total_progress_pct'] > 75
                                                            ? 'progress-bar-warning'
                                                            : 'progress-bar-success')
                                                    }}" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em; width:{{$regionSummary['total_progress_pct']}}%;">
                            {{ ($regionSummary['total_allocation'] != 0) ? number_format($regionSummary['total_progress_pct']).'%' : 'N/A'}}
                        </div>
                    </div>
                </td>
            </tr>

            </tbody>
        </table>
    </div>
</div>
@else
No regional summary information available at this time.
@endif