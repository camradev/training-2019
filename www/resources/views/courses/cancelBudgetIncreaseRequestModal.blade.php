<!-- Modal -->
<div class="modal fade" id="cancelRequestModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Cancel Budget Increase Request
                </h4>
            </div>

            <form role="form" id="cancelBudgetIncreaseRequestForm" action="{{route('cancelBudgetIncreaseRequest')}}">

                <!-- Modal Body -->
                <div class="modal-body">

                    <div class="form-group">
                        <label>Are you sure you want to cancel this budget increase request?</label>
                        <input class="form-control" type="hidden" id="cancelModalBranchCode" name="cancelModalBranchCode">
                    </div>
                    <!--button type="submit" class="btn btn-default">Submit</button-->
                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default" id="cancelBudgetRequestFormSubmit">
                        Yes
                    </button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">
                        No
                    </button>

                </div>
            </form>
            @section('scripts')
                <script>
                    $(function(){
                        $('#cancelBudgetIncreaseRequestForm').submit(function (e){
                            $('#cancelRequestModal').modal('hide');
                            loadingPanel.show();
                        });
                    });
                </script>
            @append
        </div>
    </div>
</div>
