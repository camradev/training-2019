<h3>Budget Increase Requests</h3>
@if (isset($requests) && count($requests) > 0)
    @foreach ($requests as $key => $details)
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">{{ $details->branch_name }}</h3>
            </div>
            <div class="panel-body">
                <input type="hidden" id="requestBranchCode" name="requestBranchCode" value="{{ $details->branch_code }}">
                <input type="hidden" id="requestBranchName" name="requestBranchName" value="{{ $details->branch_name }}">
                <input type="hidden" id="requestDescription" name="requestDescription" value="{{ $details->description }}">
                <table width="100%">
                    <tr>
                        <td style="padding-top: 10px;" colspan="2">Requested by {{ $details->member_name }} on {{ $details->created_at->toFormattedDateString() }}</td>
                    </tr>
                    <tr>
                        <td style="padding-top: 10px;">
                            <button class="btn manageRequestButton" onclick="document.location.href = '{{ route('manageBudgetDistribution') }}?regionFilter={{ $details->region_code }}';">Action Request</button>
                        </td>
                        <td class="text-right" style="padding-top: 10px;">
                            <button class="btn denyRequestButton" data-toggle="modal" data-target="#denyRequestModal">Deny Request</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        @include('courses.denyBudgetIncreaseRequestModal')

    @endforeach
@else
    There are no pending requests at this time.
@endif

@section("scripts")
    @parent
    <script>
        $(document).ready(function() {
            ////
            // This function sets the modal dialogs to have the correct inputs for the current budget increase request
            $('.denyRequestButton').click(function(){
                var parentDiv = $(this).closest('div');
                myBranchCode = parentDiv.find('#requestBranchCode').val();
                myBranchName = parentDiv.find('#requestBranchName').val();
                myDescription = parentDiv.find('#requestDescription').val();
                // Set those properties in the modal dialog
                $('#denyModalBranchCode').val(myBranchCode);
                $('#denyModalBranchName').val(myBranchName);
                $('#denyModalDescription').val(myDescription);
            });

            //
            ////
        });
    </script>
@endsection