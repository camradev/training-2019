<!-- Modal -->
<div class="modal fade" id="denyRequestModal" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close"
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Cancel</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Deny Budget Increase Request
                </h4>
            </div>

            <form role="form" id="denyBudgetIncreaseRequestForm" method="post" action="{{route('denyBudgetIncreaseRequest')}}">

                <!-- Modal Body -->
                <div class="modal-body">

                    <div class="form-group">
                        <label for="denyRequestDescription">Request Description (max 250 characters)</label>
                        <textarea class="form-control noresize" id="denyModalDescription" rows="3" name="denyModalDescription" maxlength="250" readonly></textarea>
                        <label for="denyRequestReason">Reason for Denial (optional - max 250 characters)</label>
                        <textarea class="form-control noresize" id="denyModalReason" rows="3" name="denyModalReason" maxlength="250"></textarea>
                        <input class="form-control" type="hidden" id="denyModalBranchCode" name="denyModalBranchCode">
                        <input class="form-control" type="hidden" id="denyModalBranchName" name="denyModalBranchName">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    </div>
                    <!--button type="submit" class="btn btn-default">Submit</button-->
                </div>

                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-primary" id="denyBudgetRequestFormSubmit">
                        Save changes
                    </button>
                </div>
            </form>
            @section('scripts')
                @parent
                <script>
                    $(function(){
                        $('#denyBudgetIncreaseRequestForm').submit(function (e){
                            $('#denyRequestModal').modal('hide');
                            loadingPanel.show();
                        });
                    });
                </script>
            @append
        </div>
    </div>
</div>
