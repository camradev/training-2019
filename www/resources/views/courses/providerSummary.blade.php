<h3>Provider/Course summary</h3>
@if (isset($providerSummary) && count($providerSummary) > 0)
    <div id="usageChart" style="width: 300px; height: 300px; margin: 0 auto;"></div>
    @foreach ($providerSummary as $provider => $course_details)
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">{{ $provider }}</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-condensed">
                    <thead>
                    <tr>
                        <th>Course</th>
                        <th class="text-right">Count</th>
                        <th class="text-right">Cost</th>
                    </tr>
                    </thead>
                    <tbody>
                   @foreach ($course_details['courses'] as $course)
                        <tr>
                            <td>{{ $course->course_name }}</td>
                            <td class="text-right">{{ $course->course_count }}</td>
                            <td class="text-right">£{{ number_format($course->cost) }}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td class="text-right"><i>Summary</i></td>
                        <td class="text-right"><i>{{ $course_details['count'] }}</i></td>
                        <td class="text-right"><i>£{{ number_format($course_details['total_cost']) }}</i></td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
    @endforeach
@else
    No provider summary information available at this time.
@endif
@section('scripts')
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("visualization", "1", {packages: ["corechart"]});
        google.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Type', 'Amount'],
                    @foreach ($providerSummary as $provider => $course_details)
                    ['{{ $provider }}', {{ $course_details['total_cost'] }}],
                    @endforeach
            ]);

            // Create a formatter to have a £ and no decimal places
            var formatter = new google.visualization.NumberFormat({prefix: '£', fractionDigits: 0});
            // Apply the format to column 1 (0-indexed)
            formatter.format(data, 1);

            var options = {
                pieStartAngle: 50,
                pieHole: 0.5,
                pieSliceText: 'value',
                chartArea: {'width': '100%', 'height': '80%'},
                legend: {position: 'top', maxLines: 4}
            };

            var chart = new google.visualization.PieChart(document.getElementById('usageChart'));
            chart.draw(data, options);
        }
    </script>
@endsection