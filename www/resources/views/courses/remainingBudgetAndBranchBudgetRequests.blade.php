<h3>Budget usage summary</h3>
@if (isset($budgetStats) && count($budgetStats) > 0)
@foreach ($budgetStats as $message => $stats)
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">{{ $message }}</h3>
    </div>
    <div class="panel-body">
        @if (isset($stats['branch_code']))
        <input type="hidden" id="requestBranchCode" name="requestBranchCode" value="{{ $stats['branch_code'] }}">
        <input type="hidden" id="requestBranchName" name="requestBranchName" value="{{ isset($stats['branch_name']) ?  $stats['branch_name'] : "" }}">
        <input type="hidden" id="requestDescription" name="requestDescription" value="{{ isset($stats['budget_increase_description']) ? $stats['budget_increase_description'] : "" }}">
        @endif
        <table width="100%">
            <tr>
                <td style="padding-right: 10px;" nowrap>Total: &pound;{{ number_format($stats['total']) }}</td>
                <td width="100%" nowrap>Remaining: &pound;{{ number_format($stats['remaining']) }}</td>
            </tr>
            @if (isset($stats['branch_code']) && !strstr($stats['branch_code'], 'FESTIVAL'))
                @if (!isset($stats['budget_increase_raised']) && isset($stats['branch_code']))
                <tr>
                    <td class="text-center" style="padding-top: 10px;" colspan="2">
                        <button class="btn addRequestButton" data-toggle="modal" data-target="#addRequestModal">Request Budget Increase</button>
                    </td>
                </tr>
                @elseif (isset($stats['budget_increase_raised']) && isset($stats['branch_code']))
                <tr>
                    <td style="padding-top: 10px;" colspan="2"><strong>An increase was requested on {{ $stats['budget_increase_raised'] }}</strong></td>
                </tr>
                <tr>
                    <td style="padding-top: 10px;">
                        <button class="btn editRequestButton" data-toggle="modal" data-target="#editRequestModal">Edit Request</button>
                    </td>
                    <td class="text-right" style="padding-top: 10px;">
                        <button class="btn cancelRequestButton" data-toggle="modal" data-target="#cancelRequestModal">Cancel Request</button>
                    </td>
                </tr>
                @endif
            @endif
        </table>
    </div>
</div>

@if (isset($stats['branch_code']) && !strstr($stats['branch_code'], 'FESTIVAL'))
    @if (!isset($stats['budget_increase_raised']) && isset($stats['branch_code']))
        @include('courses.addBudgetIncreaseRequestModal')
    @elseif (isset($stats['budget_increase_raised']) && isset($stats['branch_code']))
        @include('courses.editBudgetIncreaseRequestModal')
        @include('courses.cancelBudgetIncreaseRequestModal')
    @endif
@endif

@endforeach
@else
    No budget remaining information available at this time.
@endif

@section("scripts")
    @parent
    <script>
        $(document).ready(function() {
            ////
            // These functions set the modal dialogs to have the correct inputs for the current budget increase request
            $('.addRequestButton').click(function(){
                // Find the Div parent of the button, and then find the child Inputs holding the values we need to add to the modal
                var parentDiv = $(this).closest('div');
                myBranchCode = parentDiv.find('#requestBranchCode').val();
                myBranchName = parentDiv.find('#requestBranchName').val();
                // Set those properties in the modal dialog
                $('#addModalBranchCode').val(myBranchCode);
                $('#addModalBranchName').val(myBranchName);
            });

            $('.editRequestButton').click(function(){
                // Find the Div parent of the button, and then find the child Inputs holding the values we need to add to the modal
                var parentDiv = $(this).closest('div');
                myBranchCode = parentDiv.find('#requestBranchCode').val();
                myDescription = parentDiv.find('#requestDescription').val();
                // Set those properties in the modal dialog
                $('#editModalBranchCode').val(myBranchCode);
                $('#editModalDescription').val(myDescription);
            });

            $('.cancelRequestButton').click(function(){
                // Find the Div parent of the button, and then find the child Inputs holding the values we need to add to the modal
                var parentDiv = $(this).closest('div');
                myBranchCode = parentDiv.find('#requestBranchCode').val();
                // Set those properties in the modal dialog
                $('#cancelModalBranchCode').val(myBranchCode);
            });
            //
            ////
        });
    </script>
@endsection