@extends("layout")
@section("content")
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>Course Allocations</h3>
                        @include("courses.allocate")
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
