<?php

use App\Admin;

class BudgetsControllerAuthTest extends TestCaseWithDatabaseTransactions
{

    protected function configureDatabase()
    {
        parent::configureDatabase();
    }

    public function testAuth_TryAccessBudgetsRoutesWithoutAuth_RedirectedToLogin()
    {
        // All requests should redirect to Login screen

        // Act
        $this->visit(route('budgetOptions'))
           ->see('Sign in')
           ->seePageIs('/auth/login');

        // Act
        $this->visit(route('manageBudgetDistribution'))
            ->see('Sign in')
            ->seePageIs('/auth/login');

        // Act
        $this->visit(route('getBudgetJson'))
            ->see('Sign in')
            ->seePageIs('/auth/login');

        // Act
        $this->visit(route('saveBudgetJson'))
            ->see('Sign in')
            ->seePageIs('/auth/login');

        // Act
        $this->visit(route('viewNationalBudget'))
            ->see('Sign in')
            ->seePageIs('/auth/login');

        // Act
        $this->visit(route('updateNationalBudget'))
            ->see('Sign in')
            ->seePageIs('/auth/login');

        // Act
        $this->visit(route('viewBudgetChangeLogs'))
            ->see('Sign in')
            ->seePageIs('/auth/login');
    }

}