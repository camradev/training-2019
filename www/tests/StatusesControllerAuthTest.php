<?php

class StatusesControllerAuthTest extends TestCaseWithDatabaseTransactions
{

    protected function configureDatabase()
    {
        parent::configureDatabase();
    }

    public function testAuth_TryAccessStatusOptionsRouteWithoutAuth_RedirectedToLogin()
    {
        // Arrange

        Session::start();

        $parameters = [
            '_token' => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('statusOptions'), $parameters);

        // Assert
        $this->assertResponseStatus(302);
        $this->assertRedirectedTo('/auth/login');
    }

}