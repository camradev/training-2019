<?php

use App\Budget;
use App\Course;
use App\CourseAllocation;
use App\CourseProvider;
use Carbon\Carbon;

class DashboardControllerRegionalSummaryTest extends TestCaseWithDatabaseTransactions
{
    // Branch Secretary
    public $branchAdminUsername = '214103'; // Based on user 006671, branch is RED

    public $regionAdminUsername = '111678';

    public static $year;

    protected function configureDatabase()
    {
        parent::configureDatabase();

        self::$year = Carbon::now()->year;

        Eloquent::unguard();
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes' => 'Some notes']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes' => 'Some notes', 'cost_pounds' => 11]);
        Budget::create(['branch_code' => 'RED', 'year' => self::$year, 'amount' => 105]);
        Eloquent::reguard();

    }

    public function testRegionalSummary_NoAllocations_ReturnsNoSummary()
    {

        // Login and request unfiltered list of allocations
        Session::start();

        Auth::loginUsingId($this->regionAdminUsername);
        Auth::user()->regionAdminRegionList = ['EANG' => 'East Anglia'];
        Auth::user()->regionAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            '_token' => csrf_token()
        ];

        // Act
        $this->get(route('dashboard'), $parameters);

        // Assert
        $this->assertContains('Redditch &amp; Bromsgrove', $this->response->getContent());
    }

    public function testRegionalSummary_TwoAllocations_ReturnsSummary()
    {
        Eloquent::unguard();
        CourseAllocation::create([
            'id' => 1,
            'member_id' => '172316',
            'member_name' => 'Peter Sutton',
            'branch_id' => 'RED',
            'branch_name' => 'Redditch & Bromsgrove',
            'course_id' => 4,
            'status_id' => 4,
            'created_at' => self::$year.'-01-01 01:01:01',
            'updated_at' => self::$year.'-01-01 01:01:01'
        ]);
        CourseAllocation::create([
            'id' => 2,
            'member_id' => '226995',
            'member_name' => 'Jo Day',
            'branch_id' => 'RED',
            'branch_name' => 'Redditch & Bromsgrove',
            'course_id' => 4,
            'status_id' => 4,
            'created_at' => self::$year.'-01-01 01:01:01',
            'updated_at' => self::$year.'-01-01 01:01:01'
        ]);
        Eloquent::reguard();

        // Login and request unfiltered list of allocations
        Session::start();

        Auth::loginUsingId($this->regionAdminUsername);
        Auth::user()->regionAdminRegionList = ['EANG' => 'East Anglia'];
        Auth::user()->regionAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            '_token' => csrf_token()
        ];

        // Act
        $this->get(route('dashboard'), $parameters);

        // Assert
        $this->assertContains('Budget allocation/usage summary for East Anglia', $this->response->getContent());
        $this->assertContains('Redditch &amp; Bromsgrove', $this->response->getContent());
        // The Allocation and Usage for this branch
        $this->assertContains('105', $this->response->getContent());
        $this->assertContains('22', $this->response->getContent());

    }

//    public function testFilteredList_NoFilterApplied_ReturnsTheTwoAllocations()
//    {
//        // Login and request unfiltered list of allocations
//        Session::start();
//
//        Auth::loginUsingId($this->branchAdminUsername);
//        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];
//
//        $parameters = [
//            'jtStartIndex' => 0,
//            'jtPageSize' => 2,
//            '_token' => csrf_token()
//        ];
//
//        // Assert
//        // There should be two allocations in the db
//        $aAllocationCount = CourseAllocation::count();
//        $this->assertEquals(2, $aAllocationCount);
//
//        // Act
//        // List the allocations without a member filter
//        $this->post(route('allocationsList'), $parameters);
//
//        // Assert
//        // Check 2 records returned
//        $this->seeJsonEqualsTryNumericCheck([
//            "Result" => "OK",
//            "TotalRecordCount" => 2,
//            "Records" => [
//                [
//                    "id" => 1,
//                    "member_id" => "172316",
//                    "member_name" => "Peter Sutton",
//                    "branch_name" => "Redditch & Bromsgrove",
//                    "created_at" => '2015-01-01 01:01:01',
//                    "course_name" => "P4 Course four",
//                    "course_cost_pounds" => 10.00,
//                    "status_id" => 4,
//                    "status_name" => "Failed",
//                    "course_provider_name" => "Provider four"
//                ],
//                [
//                    "id" => 2,
//                    "member_id" => "226995",
//                    "member_name" => "Jo Day",
//                    "branch_name" => "Redditch & Bromsgrove",
//                    "created_at" => '2015-01-01 01:01:01',
//                    "course_name" => "P4 Course four",
//                    "course_cost_pounds" => 10.00,
//                    "status_id" => 4,
//                    "status_name" => "Failed",
//                    "course_provider_name" => "Provider four"
//                ]
//            ]
//        ]);
//
//    }
//
//    public function testFilteredList_FilterAppliedOnFirstMember_ReturnsOnlyTheFirstAllocation()
//    {
//        Session::start();
//
//        Auth::loginUsingId($this->branchAdminUsername);
//        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];
//
//        $parameters = [
//            'jtStartIndex' => 0,
//            'jtPageSize' => 1,
//            'memberIdFilter' => '172316',
//            '_token' => csrf_token()
//        ];
//
//        // Assert
//        // There should be two allocations in the db
//        $aAllocationCount = CourseAllocation::count();
//        $this->assertEquals(2, $aAllocationCount);
//
//        // Act
//        // List the allocations without a member filter
//        $this->post(route('allocationsList'), $parameters);
//
//        // Assert
//        // Check correct record returned
//        $this->seeJsonEqualsTryNumericCheck([
//            "Result" => "OK",
//            "TotalRecordCount" => 1,
//            "Records" => [
//                [
//                    "id" => 1,
//                    "member_id" => "172316",
//                    "member_name" => "Peter Sutton",
//                    "branch_name" => "Redditch & Bromsgrove",
//                    "created_at" => '2015-01-01 01:01:01',
//                    "course_name" => "P4 Course four",
//                    "course_cost_pounds" => 10.00,
//                    "status_id" => 4,
//                    "status_name" => "Failed",
//                    "course_provider_name" => "Provider four"
//                ]
//            ]
//        ]);
//
//    }
//
//    public function testFilteredList_FilterAppliedOnSecondMember_ReturnsOnlyTheSecondAllocation()
//    {
//        Session::start();
//
//        Auth::loginUsingId($this->branchAdminUsername);
//        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];
//
//        $parameters = [
//            'jtStartIndex' => 0,
//            'jtPageSize' => 1,
//            'memberIdFilter' => '226995',
//            '_token' => csrf_token()
//        ];
//
//        // Assert
//        // There should be two allocations in the db
//        $aAllocationCount = CourseAllocation::count();
//        $this->assertEquals(2, $aAllocationCount);
//
//        // Act
//        // List the allocations without a member filter
//        $this->post(route('allocationsList'), $parameters);
//
//        // Assert
//        // Check correct record returned
//        $this->seeJsonEqualsTryNumericCheck([
//            "Result" => "OK",
//            "TotalRecordCount" => 1,
//            "Records" => [
//                [
//                    "id" => 2,
//                    "member_id" => "226995",
//                    "member_name" => "Jo Day",
//                    "branch_name" => "Redditch & Bromsgrove",
//                    "created_at" => '2015-01-01 01:01:01',
//                    "course_name" => "P4 Course four",
//                    "course_cost_pounds" => 10.00,
//                    "status_id" => 4,
//                    "status_name" => "Failed",
//                    "course_provider_name" => "Provider four"
//                ]
//            ]
//        ]);
//
//    }
//
//    public function testFilteredList_FilterAppliedOnNonExistentMember_ReturnsNoAllocations()
//    {
//        Session::start();
//
//        Auth::loginUsingId($this->branchAdminUsername);
//        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];
//
//        $parameters = [
//            'jtStartIndex' => 0,
//            'jtPageSize' => 10,
//            'memberIdFilter' => '777777',
//            '_token' => csrf_token()
//        ];
//
//        // Assert
//        // There should be two allocations in the db
//        $aAllocationCount = CourseAllocation::count();
//        $this->assertEquals(2, $aAllocationCount);
//
//        // Act
//        // List the allocations without a member filter
//        $this->post(route('allocationsList'), $parameters);
//
//        // Assert
//        // Check correct record returned
//        $this->seeJsonEqualsTryNumericCheck([
//            "Result" => "OK",
//            "TotalRecordCount" => 0,
//            "Records" => []
//        ]);
//
//    }

}