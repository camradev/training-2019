<?php

use App\Admin;
use App\Budget;
use App\Course;
use App\CourseAllocation;
use App\CourseProvider;
use App\Log;
use Carbon\Carbon;

class LogTest extends TestCaseWithDatabaseTransactions
{
    // Ryan
    public $superUserUsername = '999993';
    public $superUserPassword = 'CPLTEST3';

    protected function configureDatabase()
    {
        parent::configureDatabase();

        Admin::create([
            'member_id' => $this->superUserUsername, //999993
            'can_edit'  => 1
        ]);
    }

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testLog_CreateUpdateDeleteAllocationByWebUi_LogEntryCreatedAsUser()
    {
        // Arrange
        Eloquent::unguard();
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes'=> 'Some notes.']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes'=> 'Some notes.', 'cost_pounds' => 10]);
        Course::create(['id' => 5, 'name' => 'P5 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx5', 'email_notes'=> 'Some notes.', 'cost_pounds' => 10]);
        Budget::create(['branch_code'=>'BEN', 'year'=>Carbon::now()->year, 'amount' => 100]);
        Eloquent::reguard();

        Session::start();

        Auth::loginUsingId($this->superUserUsername);

        $this->assertTrue(Auth::user()->isSuperViewer());
        $this->assertTrue(Auth::user()->isSuperUser());

        $parameters = [
            'member_id'   => '212924',
            'provider_id' => 4,
            'course_id'   => 4,
            'status_id'   => 4,
            'budget_source' => 'branch',
            '_token'      => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('allocationsCreate'), $parameters);


        // Assert
        $this->assertResponseOk();

        // Get the id of the created record so we can compare.
        $latestAllocation = CourseAllocation::latest('id')->first();
        // Get the latest log entry.
        $latestLog = Log::latest('id')->first();
        $initialAllocationUpdatedAt = $latestAllocation->updated_at;
        // Compare latest record to latest log entry
        $this->assertEquals($latestAllocation->id, $latestLog->entity_primary_key);
        $this->assertEquals('App\CourseAllocation', $latestLog->entity_type);

        $this->assertEquals("create: branch_id => BEN, branch_name => North Bedfordshire, budget_source => branch, course_id => ".$parameters['course_id'].", created_at => $latestAllocation->created_at, id => $latestAllocation->id, member_id => ".$parameters['member_id'].", member_name => Steve Humeniuk, status_id => $latestAllocation->status_id, updated_at => $latestAllocation->updated_at", $latestLog->change_description);
        $this->assertEquals('webui', $latestLog->changed_in);
        $this->assertEquals(Auth::user()->id, $latestLog->changed_by_member_id);
        $this->assertEquals(Auth::user()->forename . " " . Auth::user()->surname, $latestLog->changed_by_member_name);
        #$this->assertEquals($latestAllocation->created_at, $latestLog->created_at);
        #$this->assertEquals($latestAllocation->updated_at, $latestLog->updated_at);

        // Act
        // Now update it
        $newCourseId = 5;
        $latestAllocation->course_id = $newCourseId;
        $latestAllocation->save();

        // Get the latest log entry to recompare
        $latestLog = Log::latest('id')->first();

        // Assert
        // Compare latest record to latest log entry
        $this->assertEquals($latestAllocation->id, $latestLog->entity_primary_key);
        $this->assertEquals('App\CourseAllocation', $latestLog->entity_type);
        # Just in case the create and insert occur in a different second, append the updated_at:
        $updated_at = $latestAllocation->updated_at != $initialAllocationUpdatedAt ? ", updated_at: $initialAllocationUpdatedAt => $latestAllocation->updated_at" : "";
        $this->assertEquals("update: course_id: ".$parameters['course_id']." => $newCourseId" . $updated_at, $latestLog->change_description);
        $this->assertEquals('webui', $latestLog->changed_in);
        $this->assertEquals(Auth::user()->id, $latestLog->changed_by_member_id);
        $this->assertEquals(Auth::user()->forename . " " . Auth::user()->surname, $latestLog->changed_by_member_name);
//        $this->assertEquals($latestAllocation->created_at, $latestLog->created_at);
//        $this->assertEquals($latestAllocation->updated_at, $latestLog->updated_at);

        // Act
        $allocationId = $latestAllocation->id;
        $latestAllocation->delete();

        // Get the latest log entry to recompare
        $latestLog = Log::latest('id')->first();

        // Assert
        // Confirm there are no allocations.
        $this->assertNull(CourseAllocation::latest('id')->first());
        // Compare latest record to latest log entry
        $this->assertEquals($allocationId, $latestLog->entity_primary_key);
        $this->assertEquals('App\CourseAllocation', $latestLog->entity_type);
        $this->assertEquals("delete: branch_id => BEN, branch_name => North Bedfordshire, budget_source => branch, course_id => ".$newCourseId.", created_at => $latestAllocation->created_at, ended_at => , id => $latestAllocation->id, member_id => ".$parameters['member_id'].", member_name => Steve Humeniuk, started_at => , status_id => $latestAllocation->status_id, updated_at => $latestAllocation->updated_at", $latestLog->change_description);        $this->assertEquals('webui', $latestLog->changed_in);
        $this->assertEquals(Auth::user()->id, $latestLog->changed_by_member_id);
        $this->assertEquals(Auth::user()->forename . " " . Auth::user()->surname, $latestLog->changed_by_member_name);
        #$this->assertEquals($latestLog->created_at, $latestAllocation->created_at);
        #$this->assertEquals($latestLog->updated_at, $latestAllocation->updated_at);

    }

    public function testLog_CreateAllocationAdminByProviderFileImport_LogEntryCreatedAsProvider()
    {
        // Arrange
        Eloquent::unguard();
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes'=> 'Some notes.']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes'=> 'Some notes.', 'cost_pounds' => 10]);
        CourseAllocation::create(['id'          => 4,
                                  'member_id'   => '111111',
                                  'member_name' => 'Bob Smith',
                                  'branch_id'   => 'BED',
                                  'branch_name'  => 'North Bedfordshire',
                                  'course_id'   => 4,
                                  'status_id'   => 1,
                                  'created_at'  => '2015-10-01 12:34:44',
                                  'updated_at'  => '2015-10-01 12:34:44'
        ]);
        Eloquent::reguard();

        // Get the id of the created record so we can compare.
        $latestAllocation = CourseAllocation::latest('id')->first();
        // Get the latest log entry.
        $latestLog = Log::latest('id')->first();

        // Compare latest record to latest log entry
        $this->assertEquals($latestAllocation->id, $latestLog->entity_primary_key);
        $this->assertEquals('App\CourseAllocation', $latestLog->entity_type);
        $this->assertEquals("create: branch_id => BED, branch_name => North Bedfordshire, course_id => 4, created_at => 2015-10-01 12:34:44, id => 4, member_id => 111111, member_name => Bob Smith, status_id => 1, updated_at => 2015-10-01 12:34:44", $latestLog->change_description);
        $this->assertEquals('provider_file_import', $latestLog->changed_in);
        $this->assertEquals(0, $latestLog->changed_by_member_id);
        $this->assertEquals(0, $latestLog->changed_by_member_name);
        #$this->assertEquals($latestLog->created_at, $latestAllocation->created_at);
        #$this->assertEquals($latestLog->updated_at, $latestAllocation->updated_at);
    }
}
