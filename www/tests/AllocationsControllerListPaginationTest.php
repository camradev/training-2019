<?php

use App\Budget;
use App\Course;
use App\CourseAllocation;
use App\CourseProvider;
use Carbon\Carbon;

class AllocationsControllerListPaginationTest extends TestCaseWithDatabaseTransactions
{
    // Branch Secretary
    public $branchAdminUsername = '999991'; // Based on user 006671, branch is RED
    public $branchAdminPassword = 'CPLTEST1';

    protected function configureDatabase()
    {
        parent::configureDatabase();

        Eloquent::unguard();
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes' => 'Some notes']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes' => 'Some notes', 'cost_pounds' => 10]);
        Budget::create(['branch_code' => 'RED', 'year' => Carbon::now()->year, 'amount' => 100]);
        CourseAllocation::create([
            'id' => 1,
            'member_id' => '172316',
            'member_name' => 'Peter Sutton',
            'branch_id' => 'RED',
            'branch_name' => 'Redditch & Bromsgrove',
            'course_id' => 4,
            'status_id' => 4,
            'created_at' => '2015-01-01 01:01:01',
            'updated_at' => '2015-01-01 01:01:01'
        ]);
        CourseAllocation::create([
            'id' => 2,
            'member_id' => '226995',
            'member_name' => 'Jo Day',
            'branch_id' => 'RED',
            'branch_name' => 'Redditch & Bromsgrove',
            'course_id' => 4,
            'status_id' => 4,
            'created_at' => '2015-01-01 01:01:01',
            'updated_at' => '2015-01-01 01:01:01'
        ]);
        Eloquent::reguard();

    }

    public function testListPagination_StartIndexAtFirstAllocation_ReturnsTheTwoAllocations()
    {
        // Login and request unfiltered list of allocations
        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            'jtStartIndex' => 0,
            'jtPageSize' => 2,
            '_token' => csrf_token()
        ];

        // Act
        // List the allocations
        $this->post(route('allocationsList'), $parameters);

        // Assert
        // Check both records returned
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "OK",
            "TotalRecordCount" => 2,
            "Records" => [
                [
                    "id" => 1,
                    "member_id" => "172316",
                    "member_name" => "Peter Sutton",
                    "branch_name" => "Redditch & Bromsgrove",
                    "created_at" => '2015-01-01 01:01:01',
                    "course_name" => "P4 Course four",
                    "course_cost_pounds" => 10.00,
                    "status_id" => 4,
                    "status_name" => "Failed",
                    "budget_source" => "branch",
                    "course_provider_name" => "Provider four"
                ],
                [
                    "id" => 2,
                    "member_id" => "226995",
                    "member_name" => "Jo Day",
                    "branch_name" => "Redditch & Bromsgrove",
                    "created_at" => '2015-01-01 01:01:01',
                    "course_name" => "P4 Course four",
                    "course_cost_pounds" => 10.00,
                    "status_id" => 4,
                    "status_name" => "Failed",
                    "budget_source" => "branch",
                    "course_provider_name" => "Provider four"
                ]
            ]
        ]);

    }

    public function testListPagination_StartIndexAtSecondAllocation_ReturnsOnlyTheSecondAllocation()
    {
        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            'jtStartIndex' => 1,
            'jtPageSize' => 1,
            '_token' => csrf_token()
        ];

        // Act
        // List the allocations
        $this->post(route('allocationsList'), $parameters);

        // Assert
        // Check correct record returned
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "OK",
            "TotalRecordCount" => 2,
            "Records" => [
                [
                    "id" => 2,
                    "member_id" => "226995",
                    "member_name" => "Jo Day",
                    "branch_name" => "Redditch & Bromsgrove",
                    "created_at" => '2015-01-01 01:01:01',
                    "course_name" => "P4 Course four",
                    "course_cost_pounds" => 10.00,
                    "status_id" => 4,
                    "status_name" => "Failed",
                    "budget_source" => "branch",
                    "course_provider_name" => "Provider four"
                ]
            ]
        ]);

    }

    public function testListPagination_StartIndexAtThirdAllocation_ReturnsNoAllocations()
    {
        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            'jtStartIndex' => 3,
            'jtPageSize' => 10,
            '_token' => csrf_token()
        ];

        // Act
        // List the allocations
        $this->post(route('allocationsList'), $parameters);

        // Assert
        // Check correct record returned
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "OK",
            "TotalRecordCount" => 2,
            "Records" => []
        ]);

    }

    public function testListPagination_SetPageSizeToOneTestIndexOffsets_ReturnsFirstAllocation()
    {
        // Login and request unfiltered list of allocations
        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            'jtStartIndex' => 0,
            'jtPageSize' => 1,
            '_token' => csrf_token()
        ];

        // Act
        // List the allocations
        $this->post(route('allocationsList'), $parameters);

        // Assert
        // Check first allocation returned
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "OK",
            "TotalRecordCount" => 2,
            "Records" => [
                [
                    "id" => 1,
                    "member_id" => "172316",
                    "member_name" => "Peter Sutton",
                    "branch_name" => "Redditch & Bromsgrove",
                    "created_at" => '2015-01-01 01:01:01',
                    "course_name" => "P4 Course four",
                    "course_cost_pounds" => 10.00,
                    "status_id" => 4,
                    "status_name" => "Failed",
                    "budget_source" => "branch",
                    "course_provider_name" => "Provider four"
                ]
            ]
        ]);

        // Arrange
        // Increase index offset
        $parameters['jtStartIndex'] = 1;

        // Act
        // List the allocations
        $this->post(route('allocationsList'), $parameters);

        // Assert
        // Check 2nd allocation returned
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "OK",
            "TotalRecordCount" => 2,
            "Records" => [
                [
                    "id" => 2,
                    "member_id" => "226995",
                    "member_name" => "Jo Day",
                    "branch_name" => "Redditch & Bromsgrove",
                    "created_at" => '2015-01-01 01:01:01',
                    "course_name" => "P4 Course four",
                    "course_cost_pounds" => 10.00,
                    "status_id" => 4,
                    "status_name" => "Failed",
                    "budget_source" => "branch",
                    "course_provider_name" => "Provider four"
                ]
            ]
        ]);

        // Arrange
        // Increase index offset to beyond allocations
        $parameters['jtStartIndex'] = 2;

        // Act
        // List the allocations
        $this->post(route('allocationsList'), $parameters);

        // Assert
        // Check 2nd allocation returned
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "OK",
            "TotalRecordCount" => 2,
            "Records" => []
        ]);

    }

}