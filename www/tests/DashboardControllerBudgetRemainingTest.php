<?php

use App\Admin;
use App\Budget;
use App\Course;
use App\CourseAllocation;
use App\CourseProvider;
use Carbon\Carbon;

class DashboardControllerBudgetRemainingTest extends TestCaseWithDatabaseTransactions
{

    public static $year;

    // Branch Secretary
    public $branchAdminUsername = '214103'; // Based on user 006671, branch is RED

    public $regionAdminUsername = '234546'; // WMID region only

    public $branchAndRegionAdminUsername = '032905'; // Branch admin for ABC and region admin for Wales

    public $superAdminUsername = '489748'; //


    protected function configureDatabase()
    {
        parent::configureDatabase();

        self::$year = Carbon::now()->year;

        Eloquent::unguard();
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes' => 'Some notes']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes' => 'Some notes', 'cost_pounds' => 10]);
        // Add two Wales branch budgets
        Budget::create(['branch_code' => 'ABC', 'year' => self::$year, 'amount' => 102]);
        Budget::create(['branch_code' => 'BRR', 'year' => self::$year, 'amount' => 204]);
        // add 1 East Anglia budget
        Budget::create(['branch_code' => 'RED', 'year' => self::$year, 'amount' => 306]);
        //Budget::create(['branch_code' => 'NATIONAL', 'year' => Carbon::now()->year, 'amount' => 9999]);
        Eloquent::reguard();

    }

    ///////////////////////
    // Branch admin only //
    ///////////////////////

    public function testBudgetRemaining_BranchAdminNoAllocations_SuccessReturnsOnlyUnusedBranchBudget(){
        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            '_token' => csrf_token()
        ];

        // Act
        $this->get(route('dashboard'), $parameters);

        // Assert
        $this->assertNotContains('Abercolwyn branch budget', $this->response->getContent());
        $this->assertNotContains('Brecknockshire branch budget', $this->response->getContent());

        $this->assertContains('Redditch &amp; Bromsgrove branch budget', $this->response->getContent());
        $this->assertContains('306', $this->response->getContent());
    }

    public function testBudgetRemaining_BranchAdminWithAllocations_ReturnsBranchBudgetRemaining(){
        // Arrange
        Eloquent::unguard();

        CourseAllocation::create([
            'id' => 2,
            'member_id' => '226995',
            'member_name' => 'Jo Day',
            'branch_id' => 'RED',
            'branch_name' => 'Redditch & Bromsgrove',
            'course_id' => 4,
            'status_id' => 4,
            'created_at' => self::$year.'-01-01 01:01:01',
            'updated_at' => self::$year.'-01-01 01:01:01'
        ]);
        Eloquent::reguard();

        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            '_token' => csrf_token()
        ];

        // Act
        $this->get(route('dashboard'), $parameters);

        // Assert
        $this->assertNotContains('Abercolwyn branch budget', $this->response->getContent());
        $this->assertNotContains('Brecknockshire branch budget', $this->response->getContent());

        $this->assertContains('Redditch &amp; Bromsgrove branch budget', $this->response->getContent());
        $this->assertContains('296', $this->response->getContent());
    }

    ///////////////////////
    // Region admin only //
    ///////////////////////

    public function testBudgetRemaining_RegionAdminNoAllocations_ReturnsUnusedRegionBudget(){
        Session::start();

        Auth::loginUsingId($this->regionAdminUsername);
        Auth::user()->regionAdminList = ['WMID' => 'West Midlands'];
        Auth::user()->regionAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            '_token' => csrf_token()
        ];

        // Act
        $this->get(route('dashboard'), $parameters);

        // Assert
        $this->assertNotContains('Abercolwyn branch budget', $this->response->getContent());
        $this->assertNotContains('Brecknockshire branch budget', $this->response->getContent());

        $this->assertContains('West Midlands region budget', $this->response->getContent());
        $this->assertContains('306', $this->response->getContent());
    }

    public function testBudgetRemaining_RegionAdminWithAllocations_ReturnsRegionBudgetRemaining(){
        // Arrange
        Eloquent::unguard();

        CourseAllocation::create([
            'id' => 2,
            'member_id' => '226995',
            'member_name' => 'Jo Day',
            'branch_id' => 'RED',
            'branch_name' => 'Redditch & Bromsgrove',
            'course_id' => 4,
            'status_id' => 4,
            'created_at' => self::$year.'-01-01 01:01:01',
            'updated_at' => self::$year.'-01-01 01:01:01'
        ]);
        Eloquent::reguard();

        Session::start();

        Auth::loginUsingId($this->regionAdminUsername);
        Auth::user()->regionAdminList = ['WMID' => 'West Midlands'];
        Auth::user()->regionAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            '_token' => csrf_token()
        ];

        // Act
        $this->get(route('dashboard'), $parameters);

        // Assert
        $this->assertNotContains('Abercolwyn branch budget', $this->response->getContent());
        $this->assertNotContains('Brecknockshire branch budget', $this->response->getContent());
        $this->assertNotContains('Redditch &amp; Bromsgrove branch budget', $this->response->getContent());

        $this->assertContains('West Midlands region budget', $this->response->getContent());
        $this->assertContains('296', $this->response->getContent());
    }

    /////////////////////////////
    // Branch and Region admin //
    /////////////////////////////

    public function testBudgetRemaining_BranchAndRegionAdminNoAllocations_ReturnsUnusedBranchBudget(){
        // Arrange
        Eloquent::unguard();

        // Also Add an allocation for a non-wales branch, to show region tally not affected and branch/region doesn't appear
        CourseAllocation::create([
            'id' => 2,
            'member_id' => '226995',
            'member_name' => 'Jo Day',
            'branch_id' => 'RED',
            'branch_name' => 'Redditch & Bromsgrove',
            'course_id' => 4,
            'status_id' => 4,
            'created_at' => self::$year.'-01-01 01:01:01',
            'updated_at' => self::$year.'-01-01 01:01:01'
        ]);
        Eloquent::reguard();

        // Login and request unfiltered list of allocations
        Session::start();

        Auth::loginUsingId($this->branchAndRegionAdminUsername);

        Auth::user()->branchAdminBranchList = ['ABC' => 'Abercolwyn', 'BRR' => 'Brecknockshire'];
        Auth::user()->regionAdminList = ['SMWL' => 'Wales'];
        Auth::user()->regionAdminBranchList = [
            'ABC' => 'Abercolwyn',
            'BRR' => 'Brecknockshire',
            'CAR' => 'Cardiff',
            'CAT' => 'Carmarthenshire',
            'CER' => 'Bae Ceredigion',
            'CLN' => 'Vale of Clwyd',
            'GLM' => 'Mid Glamorgan',
            'MON' => 'Montgomeryshire',
            'NEB' => 'Neath & Port Talbot',
            'NEW' => 'Gwent',
            'PEM' => 'Pembroke',
            'RAD' => 'Radnorshire',
            'SIR' => 'Gwynedd A Mon',
            'SWS' => 'Swansea',
            'TEI' => 'Teifi Valley',
            'VAL' => 'Vale of Glamorgan and Bridgend'
            ];

        $parameters = [
            '_token' => csrf_token()
        ];

        // Act
        $this->get(route('dashboard'), $parameters);

        // Assert
        // Branch RED Or region WMID should not appear.
        $this->assertNotContains('Redditch &amp; Bromsgrove', $this->response->getContent());
        $this->assertNotContains('West Midlands', $this->response->getContent());

        // Branch ABC should appear with unused budget.
        $this->assertContains('Abercolwyn', $this->response->getContent());
        $this->assertContains('102', $this->response->getContent());

        // Branch BRR should also appear with unused budget.
        $this->assertContains('Brecknockshire', $this->response->getContent());
        $this->assertContains('204', $this->response->getContent());

        // Region SMWL should also appear and have the total of ABC and BRR, as no allocations assigned, and the only 2x branch budgets added.
        $this->assertContains('Wales', $this->response->getContent());
        $this->assertContains('306', $this->response->getContent());

        $this->assertContains('Budget allocation/usage summary', $this->response->getContent());
    }

    public function testBudgetRemaining_BranchAndRegionAdminWithAllocations_ReturnsBranchBudgetRemaining(){

        // Arrange
        Eloquent::unguard();

        // add ABC allocation
        CourseAllocation::create([
            'id' => 1,
            'member_id' => '109781',
            'member_name' => 'John Bennison',
            'branch_id' => 'ABC',
            'branch_name' => 'Abercolwyn',
            'course_id' => 4,
            'status_id' => 4,
            'created_at' => self::$year.'-01-01 01:01:01',
            'updated_at' => self::$year.'-01-01 01:01:01'
        ]);
        // Also Add an allocation for a non-wales branch, to show region tally not affected
        CourseAllocation::create([
            'id' => 2,
            'member_id' => '226995',
            'member_name' => 'Jo Day',
            'branch_id' => 'RED',
            'branch_name' => 'Redditch & Bromsgrove',
            'course_id' => 4,
            'status_id' => 4,
            'created_at' => self::$year.'-01-01 01:01:01',
            'updated_at' => self::$year.'-01-01 01:01:01'
        ]);
        Eloquent::reguard();

        // Login and request unfiltered list of allocations
        Session::start();

        Auth::loginUsingId($this->branchAndRegionAdminUsername);

        Auth::user()->branchAdminBranchList = ['ABC' => 'Abercolwyn', 'BRR' => 'Brecknockshire'];
        Auth::user()->regionAdminList = ['SMWL' => 'Wales'];
        Auth::user()->regionAdminBranchList = [
            'ABC' => 'Abercolwyn',
            'BRR' => 'Brecknockshire',
            'CAR' => 'Cardiff',
            'CAT' => 'Carmarthenshire',
            'CER' => 'Bae Ceredigion',
            'CLN' => 'Vale of Clwyd',
            'GLM' => 'Mid Glamorgan',
            'MON' => 'Montgomeryshire',
            'NEB' => 'Neath & Port Talbot',
            'NEW' => 'Gwent',
            'PEM' => 'Pembroke',
            'RAD' => 'Radnorshire',
            'SIR' => 'Gwynedd A Mon',
            'SWS' => 'Swansea',
            'TEI' => 'Teifi Valley',
            'VAL' => 'Vale of Glamorgan and Bridgend'
        ];

        $parameters = [
            '_token' => csrf_token()
        ];

        // Act
        $this->get(route('dashboard'), $parameters);

        // Assert
        // Branch RED Or region WMDI should not appear.
        $this->assertNotContains('Redditch &amp; Bromsgrove', $this->response->getContent());
        $this->assertNotContains('West Midlands', $this->response->getContent());

        // Branch ABC should appearm with usage of 10 due to 1 allocation.
        $this->assertContains('Abercolwyn', $this->response->getContent());
        $this->assertContains('92', $this->response->getContent());

        // Branch BRR should also appear but have 123 remaining, as no allocations assigned.
        $this->assertContains('Brecknockshire', $this->response->getContent());
        $this->assertContains('204', $this->response->getContent());

        // Region SMWL should also appear and have the total of ABC and BRR, as one allocation assigned, and the only 2x branch budgets added.
        $this->assertContains('Wales', $this->response->getContent());
        $this->assertContains('296', $this->response->getContent());

        $this->assertContains('Budget allocation/usage summary', $this->response->getContent());
    }

    public function testBudgetRemaining_SuperAdminNoAllocations_ReturnsUnusedTotalBudget(){
        // Arrange
        Admin::create([
            'member_id' => '489748',
            'can_edit' => 1,
            'notes' => ''
        ]);

        // Login and request unfiltered list of allocations
        Session::start();

        Auth::loginUsingId($this->superAdminUsername);

        $parameters = [
            '_token' => csrf_token()
        ];

        // Act
        $this->get(route('dashboard'), $parameters);

        // Assert
        // Branch RED Or region EANG should not appear.
        $this->assertNotContains('Redditch &amp; Bromsgrove', $this->response->getContent());
        $this->assertNotContains('East Anglia', $this->response->getContent());
        $this->assertNotContains('Abercolwyn', $this->response->getContent());
        $this->assertNotContains('Brecknockshire', $this->response->getContent());
        $this->assertNotContains('Wales', $this->response->getContent());

        $this->assertContains('Allocated national budget', $this->response->getContent());
        $this->assertContains('612', $this->response->getContent());
    }

    public function testBudgetRemaining_SuperAdminWithAllocation_ReturnsTotalBudgetRemaining(){
        // Arrange
        Eloquent::unguard();

        // add ABC allocation
        CourseAllocation::create([
            'id' => 1,
            'member_id' => '109781',
            'member_name' => 'John Bennison',
            'branch_id' => 'ABC',
            'branch_name' => 'Abercolwyn',
            'course_id' => 4,
            'status_id' => 4,
            'created_at' => self::$year.'-01-01 01:01:01',
            'updated_at' => self::$year.'-01-01 01:01:01'
        ]);
        Eloquent::reguard();

        Admin::create([
            'member_id' => '489748',
            'can_edit' => 1,
            'notes' => ''
        ]);

        // Login and request unfiltered list of allocations
        Session::start();

        Auth::loginUsingId($this->superAdminUsername);

        $parameters = [
            '_token' => csrf_token()
        ];

        // Act
        $this->get(route('dashboard'), $parameters);

        // Assert
        // Branch RED Or region EANG should not appear.
        $this->assertNotContains('Redditch &amp; Bromsgrove', $this->response->getContent());
        $this->assertNotContains('East Anglia', $this->response->getContent());
        $this->assertNotContains('Abercolwyn', $this->response->getContent());
        $this->assertNotContains('Brecknockshire', $this->response->getContent());
        $this->assertNotContains('Wales', $this->response->getContent());

        $this->assertContains('Allocated national budget', $this->response->getContent());
        $this->assertContains('602', $this->response->getContent());
    }

}