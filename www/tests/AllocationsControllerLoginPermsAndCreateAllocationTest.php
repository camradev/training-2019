<?php

use App\Admin;
use App\Budget;
use App\Course;
use App\CourseAllocation;
use App\CourseProvider;
use Carbon\Carbon;

class AllocationsControllerLoginPermsAndCreateAllocationTest extends TestCaseWithDatabaseTransactions
{
    // Branch Secretary
    public $branchAdminUsername = '006671'; // Based on user 006671, branch is RED
    // Regional Director
    public $regionAdminUsername = '111678'; // Based on user 111678, two regions, covering 20 branches.
    // Ryan
    public $superUserUsername = '999993'; // SuperUser
    // Dan
    public $superViewerUsername = '999994'; // SuperViewer
    public $superViewerPassword = 'CPLTEST4';

    public $basicUserUsername = '999995'; // No perms

    protected function configureDatabase()
    {
        parent::configureDatabase();

        Admin::create([
            'member_id' => $this->superUserUsername, //999993
            'can_edit'  => 1
        ]);

        Admin::create([
            'member_id' => $this->superViewerUsername, //999994
            'can_edit'  => 0
        ]);

        Admin::create([
            'member_id' => 'bob', //No member record in db for this user, only in Admins
            'can_edit'  => 0
        ]);
    }

    ##################
    ## Branch Admin ##
    ##################

    public function testCreate_BranchAdminCreatesAllocationForMemberInCoveredBranch_Success()
    {

        // Arrange

        Eloquent::unguard();
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes' => 'Some notes']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes' => 'Some notes', 'cost_pounds' => 10]);
        Budget::create(['branch_code'=>'RED', 'year'=>Carbon::now()->year, 'amount' => 100]);
        Eloquent::reguard();

        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            'member_id'   => '172316', // Branch is RED
            'provider_id' => 4,
            'course_id'   => 4,
            'status_id'   => 1,
            'budget_source' => 'branch',
            '_token'      => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('allocationsCreate'), $parameters);

        //dd($this->response->content());
        // Assert
        $this->assertResponseOk();

        // Get the id of the created record so we can compare.
        $latestAllocation = CourseAllocation::latest()->first();

        // Compare latest record to compare to response
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "OK",
            "Record" => [
                "id"                   => $latestAllocation["id"],
                "member_id"            => "172316",
                "member_name"          => "Peter Sutton",
                "branch_name"          => "Redditch & Bromsgrove",
                "created_at"           => $latestAllocation["created_at"]->toDateTimeString(),
                "course_name"          => "P4 Course four",
                "course_cost_pounds"   => 10.00,
                "status_id"            => 1,
                "status_name"          => "Allocated",
                "budget_source" => "branch",
                "course_provider_name" => "Provider four"
            ]
        ]);
    }

    public function testCreate_BranchAdminCreatesAllocationForMemberNotInCoveredBranch_ErrorNotEnoughPerms()
    {

        // Arrange

        Eloquent::unguard();
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes' => 'Some notes']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes' => 'Some notes', 'cost_pounds' => 10]);
        Eloquent::reguard();

        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            'member_id'   => "111678",
            'provider_id' => 4,
            'course_id'   => 4,
            'status_id'   => 1,
            'budget_source' => 'branch',
            '_token'      => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('allocationsCreate'), $parameters);
#dump($this->response->content());

        // Assert
        $this->assertResponseOk();

        // Get the id of the created record so we can compare.
        $latestAllocation = CourseAllocation::latest()->first();
        $this->assertNull($latestAllocation);

        // Compare latest record to compare to response
        $this->seeJsonEqualsTryNumericCheck([
            "Result"  => "ERROR",
            "Message" => "You do not have the required permissions to allocate courses for member with ID '111678'."
        ]);

    }

    ##################
    ## Region Admin ##
    ##################

    public function testCreate_RegionAdminCreatesAllocationForMemberInCoveredBranch_Success()
    {

        // Arrange

        Eloquent::unguard();
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes' => 'Some notes']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes' => 'Some notes', 'cost_pounds' => 10]);
        Budget::create(['branch_code'=>'RED', 'year'=>Carbon::now()->year, 'amount' => 100]);
        Eloquent::reguard();

        Session::start();

        Auth::loginUsingId($this->regionAdminUsername);
        Auth::user()->regionAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        #dd(Auth::user()->branchAdminBranchList);

        $parameters = [
            'member_id'   => "006671",
            'provider_id' => 4,
            'course_id'   => 4,
            'status_id'   => 1,
            'budget_source' => 'branch',
            '_token'      => csrf_token()
        ];

        #dd(Auth::user()->branchAdminBranchList);

        // Act
        // Create new allocation
        $this->post(route('allocationsCreate'), $parameters);

        // Assert
        $this->assertResponseOk();
        // Get the id of the created record so we can compare.
        $latestAllocation = CourseAllocation::latest()->first();

        // Compare latest record to compare to response
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "OK",
            "Record" => [
                "id"                   => $latestAllocation["id"],
                "member_id"            => "006671",
                "member_name"          => "Gerard Quinn",
                "branch_name"          => "Redditch & Bromsgrove",
                "created_at"           => $latestAllocation["created_at"]->toDateTimeString(),
                "course_name"          => "P4 Course four",
                "course_cost_pounds"   => 10.00,
                "status_id"            => 1,
                "status_name"          => "Allocated",
                "budget_source" => "branch",
                "course_provider_name" => "Provider four"
            ]
        ]);
    }

    public function testCreate_RegionAdminCreatesAllocationForMemberNotInCoveredBranch_ErrorNotEnoughPerms()
    {
        // Arrange
        Eloquent::unguard();
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes' => 'Some notes']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes' => 'Some notes', 'cost_pounds' => 10]);
        Budget::create(['branch_code' => 'RED', 'amount' => 100, 'year' => Carbon::now()->year]);
        Eloquent::reguard();

        Session::start();

        Auth::loginUsingId($this->regionAdminUsername);
        Auth::user()->regionAdminList = ['EANG' => 'East Anglia'];
        Auth::user()->regionAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            'member_id'   => '111678', // Branch is KLN
            'provider_id' => 4,
            'course_id'   => 4,
            'status_id'   => 1,
            'budget_source' => 'branch',
            '_token'      => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('allocationsCreate'), $parameters);
#dump($this->response->content());
        // Assert
        $this->assertResponseOk();

        // Get the id of the created record so we can compare.
        $latestAllocation = CourseAllocation::latest()->first();
        #dd($latestAllocation);

        // Compare latest record to compare to response
        $this->seeJsonEqualsTryNumericCheck([
            "Result"  => "ERROR",
            "Message" => "You do not have the required permissions to allocate courses for member with ID '111678'."
        ]);
    }

    #################
    ## SuperViewer ##
    #################

    public function testCreate_ValidSuperViewerOnlyTryingToCreate_CannotSeeCreateAction()
    {

        // Check doesn't have the Create action
        $credentials = [
            'username' => $this->superViewerUsername,
            'password' => $this->superViewerPassword,
        ];

        // Check that logging in was successful and we don't see the Create Action, so button is missing.
        $this->visit('/auth/login')
            ->see('Sign in')
            ->submitForm('Sign in', $credentials)
            ->seePageIs('/')
            ->dontSee('createAction:');

        # Confirm was only SuperViewer
        $this->assertTrue(Auth::user()->isSuperViewer());
        $this->assertFalse(Auth::user()->isSuperUser());
    }

    ###############
    ## SuperUser ##
    ###############

    public function testCreate_SuperUserCreatesAllocationForAnyMember_SuccessAllocationCreated()
    {

        // Arrange

        Eloquent::unguard();
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes' => 'Some notes']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes' => 'Some notes', 'cost_pounds' => 10]);
        Budget::create(['branch_code'=>'BEN', 'year'=>Carbon::now()->year, 'amount' => 100]);
        Eloquent::reguard();

        Session::start();

        Auth::loginUsingId($this->superUserUsername);

        $this->assertTrue(Auth::user()->isSuperViewer());
        $this->assertTrue(Auth::user()->isSuperUser());

        #dd(Auth::user()->branchAdminBranchList);

        $parameters = [
            'member_id'   => '212924',
            'provider_id' => 4,
            'course_id'   => 4,
            'status_id'   => 1,
            'budget_source' => 'branch',
            '_token'      => csrf_token()
        ];

        #dd(Auth::user()->branchAdminBranchList);

        // Act
        // Create new allocation
        $this->post(route('allocationsCreate'), $parameters);
        #dump($this->response->content());

        // Assert
        $this->assertResponseOk();
        #dd($this->response->content());

        // Get the id of the created record so we can compare.
        $latestAllocation = CourseAllocation::latest()->first();

        // Compare latest record to compare to response
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "OK",
            "Record" => [
                "id"                   => $latestAllocation["id"],
                "member_id"            => "212924",
                "member_name"          => "Steve Humeniuk",
                "branch_name"          => "North Bedfordshire",
                "created_at"           => $latestAllocation["created_at"]->toDateTimeString(),
                "course_name"          => "P4 Course four",
                "course_cost_pounds"   => 10.00,
                "status_id"            => 1,
                "status_name"          => "Allocated",
                "budget_source" => "branch",
                "course_provider_name" => "Provider four"
            ]
        ]);
    }

    public function testCreate_CreateAllocationWhereMemberHasInvalidEmailAddress_CreatedAndSeeInvalidMailMessageDialog()
    {

        // Arrange

        Eloquent::unguard();
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes' => 'Some notes']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes' => 'Some notes', 'cost_pounds' => 10]);
        Budget::create(['branch_code'=>'AAA', 'year'=>Carbon::now()->year, 'amount' => 100]);
        Eloquent::reguard();

        Session::start();

        Auth::loginUsingId($this->superUserUsername);

        $this->assertTrue(Auth::user()->isSuperViewer());
        $this->assertTrue(Auth::user()->isSuperUser());

        #dd(Auth::user()->branchAdminBranchList);

        $parameters = [
            'member_id'   => "172964",
            'provider_id' => 4,
            'course_id'   => 4,
            'status_id'   => 1,
            'budget_source' => 'branch',
            '_token'      => csrf_token()
        ];

        #dd(Auth::user()->branchAdminBranchList);

        // Act
        // Create new allocation
        $this->post(route('allocationsCreate'), $parameters);
        #dump($this->response->content());

        // Assert
        $this->assertResponseOk();
        #dd($this->response->content());

        // Get the id of the created record so we can compare.
        $latestAllocation = CourseAllocation::latest()->first();

        // Compare latest record to compare to response
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "OK",
            "Record" => [
                "id"                          => $latestAllocation["id"],
                "member_id"                   => "172964",
                "member_name"                 => "Gary Poulter",
                "branch_name"                 => "Arun and Adur",
                "created_at"                  => $latestAllocation["created_at"]->toDateTimeString(),
                "course_name"                 => "P4 Course four",
                "course_cost_pounds"          => 10.00,
                "status_id"                   => 1,
                "status_name"                 => "Allocated",
                "course_provider_name"        => "Provider four",
                "budget_source" => "branch",
                "_validationExceptionMessage" => "The allocation was successful, however, could not notify member Gary Poulter (172964) of allocation due to invalid email address: poulter"
            ]
        ]);
    }

    public function testCreate_CreateAllocationWhereMemberHasEmptyEmailAddress_CreatedAndSeeEmptyMailMessageDialog()
    {

        // Arrange

        Eloquent::unguard();
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes' => 'Some notes']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes' => 'Some notes', 'cost_pounds' => 10]);
        Budget::create(['branch_code'=>'RED', 'year'=>Carbon::now()->year, 'amount' => 100]);
        Eloquent::reguard();

        Session::start();

        Auth::loginUsingId($this->superUserUsername);

        $this->assertTrue(Auth::user()->isSuperViewer());
        $this->assertTrue(Auth::user()->isSuperUser());

        #dd(Auth::user()->branchAdminBranchList);

        $parameters = [
            'member_id'   => '105681',
            'provider_id' => 4,
            'course_id'   => 4,
            'status_id'   => 1,
            'budget_source' => 'branch',
            '_token'      => csrf_token()
        ];

        #dd(Auth::user()->branchAdminBranchList);

        // Act
        // Create new allocation
        $this->post(route('allocationsCreate'), $parameters);
        #dump($this->response->content());

        // Assert
        $this->assertResponseOk();
        #dd($this->response->content());

        // Get the id of the created record so we can compare.
        $latestAllocation = CourseAllocation::latest()->first();

        // Compare latest record to compare to response
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "OK",
            "Record" => [
                "id"                          => $latestAllocation["id"],
                "member_id"                   => "105681",
                "member_name"                 => "Gail Vickers",
                "branch_name"                 => "Redditch & Bromsgrove",
                "created_at"                  => $latestAllocation["created_at"]->toDateTimeString(),
                "course_name"                 => "P4 Course four",
                "course_cost_pounds"          => 10.00,
                "status_id"                   => 1,
                "status_name"                 => "Allocated",
                "budget_source" => "branch",
                "course_provider_name"        => "Provider four",
                "_validationExceptionMessage" => "The allocation was successful, however, could not notify member Gail Vickers (105681) of allocation due to missing email address."
            ]
        ]);
    }

    public function testCreate_CreateAllocationWhereInsufficientBudgetAvailable_ErrorInsufficientBudget()
    {

        // Arrange

        $year = Carbon::now()->year;

        Eloquent::unguard();
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes' => 'Some notes']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes' => 'Some notes', 'cost_pounds' => 10]);
        Course::create(['id' => 5, 'name' => 'P4 Course five', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx5', 'email_notes' => 'Some notes', 'cost_pounds' => 10]);
        Budget::create(['branch_code'=>'BEN', 'year'=>Carbon::now()->year, 'amount' => 10]);
        CourseAllocation::create([
            'id'          => 1,
            'member_id'   => '212924',
            'member_name' => 'Steve Humeniuk',
            'branch_id'   => 'BEN',
            'branch_name' => 'North Bedfordshire',
            'course_id'   => 4,
            'status_id'   => 4,
            'budget_source' => 'branch',
            'created_at'  => "$year-01-01 10:10:10",
            'updated_at'  => "$year-01-01 10:10:10"
        ]);
        Eloquent::reguard();

        // Get the id of the created record so we can confirm 1st allocation did occur.
        $allocationCount = CourseAllocation::count();
        // There should be one allocation.
        $this->assertEquals(1, $allocationCount);

        Session::start();

        Auth::loginUsingId($this->superUserUsername);

        $this->assertTrue(Auth::user()->isSuperViewer());
        $this->assertTrue(Auth::user()->isSuperUser());

        $parameters = [
            'member_id'   => '212924',
            'provider_id' => 4,
            'course_id'   => 5,
            'status_id'   => 1,
            'budget_source' => 'branch',
            '_token'      => csrf_token()
        ];

        // Act
        // Try to add a second allocation, there should be no budget left
        $this->post(route('allocationsCreate'), $parameters);

        // Assert
        $this->assertResponseStatus(200);

        // There should only be the original allocation.
        $allocationCount = CourseAllocation::count();
        $this->assertEquals(1, $allocationCount);

        // Get the id of the created record so we can confirm 2nd allocation did not occur.
        $latestAllocation = CourseAllocation::latest('id')->first();
        // There should only be the original allocation.
        $this->assertEquals(1, $latestAllocation->id);

        // Error should be returned
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "ERROR",
            "Message" => "Insufficient budget available to allocate this course."
        ]);
    }

    public function testCreate_CreateSecondAllocationForSameCourseWhereFirstOneStillOpen_ErrorIncompleteAllocationAlreadyExists()
    {

        // Arrange

        $year = Carbon::now()->year;

        Eloquent::unguard();
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes' => 'Some notes']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes' => 'Some notes', 'cost_pounds' => 10]);
        Course::create(['id' => 5, 'name' => 'P4 Course five', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx5', 'email_notes' => 'Some notes', 'cost_pounds' => 10]);
        Budget::create(['branch_code'=>'BEN', 'year'=>Carbon::now()->year, 'amount' => 10]);
        CourseAllocation::create([
            'id'          => 1,
            'member_id'   => '212924',
            'member_name' => 'Steve Humeniuk',
            'branch_id'   => 'BEN',
            'branch_name' => 'North Bedfordshire',
            'course_id'   => 4,
            'status_id'   => 1,
            'created_at'  => "$year-01-01 10:10:10",
            'updated_at'  => "$year-01-01 10:10:10"
        ]);
        Eloquent::reguard();

        // Get the id of the created record so we can confirm 1st allocation did occur.
        $allocationCount = CourseAllocation::count();
        // There should be one allocation.
        $this->assertEquals(1, $allocationCount);

        Session::start();

        Auth::loginUsingId($this->superUserUsername);

        $this->assertTrue(Auth::user()->isSuperViewer());
        $this->assertTrue(Auth::user()->isSuperUser());

        $parameters = [
            'member_id'   => '212924',
            'provider_id' => 4,
            'course_id'   => 4,
            'status_id'   => 1,
            'budget_source' => 'branch',
            '_token'      => csrf_token()
        ];

        // Act
        // Try to add a second allocation, there should be no budget left
        $this->post(route('allocationsCreate'), $parameters);

        // Assert
        $this->assertResponseStatus(200);

        // There should only be the original allocation.
        $allocationCount = CourseAllocation::count();
        $this->assertEquals(1, $allocationCount);

        // Get the id of the created record so we can confirm 2nd allocation did not occur.
        $latestAllocation = CourseAllocation::latest('id')->first();
        // There should only be the original allocation.
        $this->assertEquals(1, $latestAllocation->id);

        // Error should be returned
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "ERROR",
            "Message" => "An incomplete allocation already exists for course ID '4', against member with ID '212924' - the course must be completed before a new allocation can be made."
        ]);
    }
}