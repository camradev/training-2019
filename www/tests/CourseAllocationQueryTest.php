<?php

use App\Budget;
use App\Course;
use App\CourseAllocation;
use App\CourseProvider;
use Carbon\Carbon;

class CourseAllocationQueryTest extends TestCaseWithDatabaseTransactions
{
    private static $year;

    // Regional Director
    public $regionAdminUsername = '999992';

    protected function configureDatabase()
    {
        parent::configureDatabase();

        self::$year = Carbon::now()->year;

        Eloquent::unguard();
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes' => 'Some notes']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes' => 'Some notes', 'cost_pounds' => 10]);
        Budget::create(['branch_code' => 'RED', 'year' => self::$year, 'amount' => 100]);
        Eloquent::reguard();
    }

    public function testQuery_ConfirmRegionalSummaryMethodReturnsExpected__RedirectedToLogin()
    {
        // Arrange
        Eloquent::unguard();
        CourseAllocation::create([
            'id' => 1,
            'member_id' => '172316',
            'member_name' => 'Peter Sutton',
            'branch_id' => 'RED',
            'branch_name' => 'Redditch & Bromsgrove',
            'course_id' => 4,
            'status_id' => 4,
            'created_at' => self::$year."-01-01 01:01:01",
            'updated_at' => self::$year."-01-01 01:01:01"
        ]);
        CourseAllocation::create([
            'id' => 2,
            'member_id' => '226995',
            'member_name' => 'Jo Day',
            'branch_id' => 'RED',
            'branch_name' => 'Redditch & Bromsgrove',
            'course_id' => 4,
            'status_id' => 4,
            'created_at' => self::$year."-01-01 01:01:01",
            'updated_at' => self::$year."-01-01 01:01:01"
        ]);
        Eloquent::reguard();


        Session::start();

        Auth::loginUsingId($this->regionAdminUsername);
        Auth::user()->regionAdminList = ['EANG' => 'East Anglia'];
        Auth::user()->regionAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        // Get the allocations summary
        $regionalBranchSummary = CourseAllocation::getAllocationRegionalSummaryForYear(Carbon::now()->year);

        // Assert
        // There should be only one branch entry, with a budget and a usage for the two courses.
        $this->assertEquals(1, count($regionalBranchSummary));
        $this->assertEquals("RED", $regionalBranchSummary[0]->branch_code);
        $this->assertEquals(100, $regionalBranchSummary[0]->budget_allocated);
        $this->assertEquals(20, $regionalBranchSummary[0]->budget_used);
    }
}