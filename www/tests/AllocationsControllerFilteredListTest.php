<?php

use App\Budget;
use App\Course;
use App\CourseAllocation;
use App\CourseProvider;
use Carbon\Carbon;

class AllocationsControllerFilteredListTest extends TestCaseWithDatabaseTransactions
{
    // Branch Secretary
    public $branchAdminUsername = '999991'; // Based on user 006671, branch is RED
    public $branchAdminPassword = 'CPLTEST1';

    protected function configureDatabase()
    {
        parent::configureDatabase();

        Eloquent::unguard();
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes' => 'Some notes']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes' => 'Some notes', 'cost_pounds' => 10]);
        Budget::create(['branch_code' => 'RED', 'year' => Carbon::now()->year, 'amount' => 100]);
        CourseAllocation::create([
            'id' => 1,
            'member_id' => '172316',
            'member_name' => 'Peter Sutton',
            'branch_id' => 'RED',
            'branch_name' => 'Redditch & Bromsgrove',
            'course_id' => 4,
            'status_id' => 4,
            'created_at' => '2015-01-01 01:01:01',
            'updated_at' => '2015-01-01 01:01:01'
        ]);
        CourseAllocation::create([
            'id' => 2,
            'member_id' => '226995',
            'member_name' => 'Jo Day',
            'branch_id' => 'RED',
            'branch_name' => 'Redditch & Bromsgrove',
            'course_id' => 4,
            'status_id' => 4,
            'created_at' => '2015-01-01 01:01:01',
            'updated_at' => '2015-01-01 01:01:01'
        ]);
        Eloquent::reguard();

    }

    public function testFilteredList_NoFilterApplied_ReturnsTheTwoAllocations()
    {
        // Login and request unfiltered list of allocations
        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            'jtStartIndex' => 0,
            'jtPageSize' => 2,
            '_token' => csrf_token()
        ];

        // Assert
        // There should be two allocations in the db
        $aAllocationCount = CourseAllocation::count();
        $this->assertEquals(2, $aAllocationCount);

        // Act
        // List the allocations without a member filter
        $this->post(route('allocationsList'), $parameters);

        // Assert
        // Check 2 records returned
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "OK",
            "TotalRecordCount" => 2,
            "Records" => [
                [
                    "id" => 1,
                    "member_id" => "172316",
                    "member_name" => "Peter Sutton",
                    "branch_name" => "Redditch & Bromsgrove",
                    "created_at" => '2015-01-01 01:01:01',
                    "course_name" => "P4 Course four",
                    "course_cost_pounds" => 10.00,
                    "status_id" => 4,
                    "status_name" => "Failed",
                    "budget_source" => "branch",
                    "course_provider_name" => "Provider four"
                ],
                [
                    "id" => 2,
                    "member_id" => "226995",
                    "member_name" => "Jo Day",
                    "branch_name" => "Redditch & Bromsgrove",
                    "created_at" => '2015-01-01 01:01:01',
                    "course_name" => "P4 Course four",
                    "course_cost_pounds" => 10.00,
                    "status_id" => 4,
                    "status_name" => "Failed",
                    "budget_source" => "branch",
                    "course_provider_name" => "Provider four"
                ]
            ]
        ]);

    }

    public function testFilteredList_FilterAppliedOnFirstMember_ReturnsOnlyTheFirstAllocation()
    {
        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            'jtStartIndex' => 0,
            'jtPageSize' => 1,
            'memberIdFilter' => '172316',
            '_token' => csrf_token()
        ];

        // Assert
        // There should be two allocations in the db
        $aAllocationCount = CourseAllocation::count();
        $this->assertEquals(2, $aAllocationCount);

        // Act
        // List the allocations without a member filter
        $this->post(route('allocationsList'), $parameters);

        // Assert
        // Check correct record returned
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "OK",
            "TotalRecordCount" => 1,
            "Records" => [
                [
                    "id" => 1,
                    "member_id" => "172316",
                    "member_name" => "Peter Sutton",
                    "branch_name" => "Redditch & Bromsgrove",
                    "created_at" => '2015-01-01 01:01:01',
                    "course_name" => "P4 Course four",
                    "course_cost_pounds" => 10.00,
                    "status_id" => 4,
                    "status_name" => "Failed",
                    "budget_source" => "branch",
                    "course_provider_name" => "Provider four"
                ]
            ]
        ]);

    }

    public function testFilteredList_FilterAppliedOnSecondMember_ReturnsOnlyTheSecondAllocation()
    {
        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            'jtStartIndex' => 0,
            'jtPageSize' => 1,
            'memberIdFilter' => '226995',
            '_token' => csrf_token()
        ];

        // Assert
        // There should be two allocations in the db
        $aAllocationCount = CourseAllocation::count();
        $this->assertEquals(2, $aAllocationCount);

        // Act
        // List the allocations without a member filter
        $this->post(route('allocationsList'), $parameters);

        // Assert
        // Check correct record returned
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "OK",
            "TotalRecordCount" => 1,
            "Records" => [
                [
                    "id" => 2,
                    "member_id" => "226995",
                    "member_name" => "Jo Day",
                    "branch_name" => "Redditch & Bromsgrove",
                    "created_at" => '2015-01-01 01:01:01',
                    "course_name" => "P4 Course four",
                    "course_cost_pounds" => 10.00,
                    "status_id" => 4,
                    "status_name" => "Failed",
                    "budget_source" => "branch",
                    "course_provider_name" => "Provider four"
                ]
            ]
        ]);

    }

    public function testFilteredList_FilterAppliedOnNonExistentMember_ReturnsNoAllocations()
    {
        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            'jtStartIndex' => 0,
            'jtPageSize' => 10,
            'memberIdFilter' => '777777',
            '_token' => csrf_token()
        ];

        // Assert
        // There should be two allocations in the db
        $aAllocationCount = CourseAllocation::count();
        $this->assertEquals(2, $aAllocationCount);

        // Act
        // List the allocations without a member filter
        $this->post(route('allocationsList'), $parameters);

        // Assert
        // Check correct record returned
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "OK",
            "TotalRecordCount" => 0,
            "Records" => []
        ]);

    }

}