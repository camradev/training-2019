<?php

use App\Admin;

class AdminsControllerPermsTest extends TestCaseWithDatabaseTransactions
{
    // Branch Secretary
    public $branchAdminUsername = '999991'; // Based on user 006671, branch is RED
    public $branchAdminPassword = 'CPLTEST1';
    // Regional Director
    public $regionAdminUsername = '999992'; // Based on user 111678, two regions, covering 20 branches.
    public $regionAdminPassword = 'CPLTEST2';
    // Ryan
    public $superUserUsername = '999993';
    public $superUserPassword = 'CPLTEST3';
    // Dan
    public $superViewerUsername = '999994';
    public $superViewerPassword = 'CPLTEST4';

    public $basicUserUsername = '999995';
    public $basicUserPassword = 'CPLTEST5';

    protected function configureDatabase()
    {
        parent::configureDatabase();

        Admin::create([
            'member_id' => $this->superUserUsername, //999993
            'can_edit'  => 1
        ]);

        Admin::create([
            'member_id' => $this->superViewerUsername, //999994
            'can_edit'  => 0
        ]);

        Admin::create([
            'member_id' => 'bob', //No member record in db for this user, only in Admins
            'can_edit'  => 0
        ]);
    }

    public function testLogin_UserInMembersDbAndBranchAdmin_SuccessSeePortal()
    {

        // Login for branch or region admin tries to hit members table, so disabled for now and using loginUsingId to bypass

//        $credentials = [
//            'username' => $this->branchAdminUsername, // 999991
//            'password' => $this->branchAdminPassword,
//        ];
//
//        // Check that logging in was successful and we landed on dashboard.
//        $this->visit('/auth/login');
//
//        $this->see('Sign in')
//            ->submitForm('Sign in', $credentials)
//            ->seePageIs('/')
//            ->see('Welcome to the training portal');
//

        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            '_token'      => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('allocationsList'), $parameters);

        $this->visit('/')
            ->seePageIs('/')
            ->see('Welcome to the training portal');
        #dump($this->response->content());
        // Assert
        $this->assertResponseOk();

        $this->assertTrue(Auth::user()->isBranchAdmin());
        $this->assertFalse(Auth::user()->isRegionAdmin());
        $this->assertFalse(Auth::user()->isSuperViewer());
        $this->assertFalse(Auth::user()->isSuperUser());
    }

    public function testLogin_UserInMembersDbAndRegionAdmin_SuccessSeePortal()
    {
        // Login for branch or region admin tries to hit members table, so disabled for now and using loginUsingId to bypass

//        $credentials = [
//            'username' => $this->regionAdminUsername,// 999992
//            'password' => $this->regionAdminPassword,
//        ];
//
//        // Check that logging in was successful and we landed on dashboard.
//        $this->visit('/auth/login')
//            ->see('Sign in')
//            ->submitForm('Sign in', $credentials)
//            ->seePageIs('/')
//            ->see('Welcome to the training portal');

        // Arrange
        Session::start();

        Auth::loginUsingId($this->regionAdminUsername);
        Auth::user()->adminRegionList = ['EANG' => 'East Anglia'];
        Auth::user()->regionAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            '_token'      => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('allocationsList'), $parameters);

        $this->visit('/')
            ->seePageIs('/')
            ->see('Welcome to the training portal');

        // Assert
        $this->assertResponseOk();

        $this->assertFalse(Auth::user()->isBranchAdmin());
        $this->assertTrue(Auth::user()->isRegionAdmin());
        $this->assertFalse(Auth::user()->isSuperViewer());
        $this->assertFalse(Auth::user()->isSuperUser());
    }

    public function testLogin_UserInMembersDbAndValidSuperUser_SuccessSeePortal()
    {
        // Arrange
        $credentials = [
            'username' => $this->superUserUsername,
            'password' => $this->superUserPassword,
        ];

        // Act
        // Check that logging in was successful and we landed on dashboard.
        $this->visit('/auth/login')
            ->see('Sign in')
            ->submitForm('Sign in', $credentials)
            ->seePageIs('/')
            ->see('Welcome to the training portal');

        // Assert
        $this->assertFalse(Auth::user()->isBranchAdmin());
        $this->assertFalse(Auth::user()->isRegionAdmin());
        $this->assertTrue(Auth::user()->isSuperViewer());
        $this->assertTrue(Auth::user()->isSuperUser());
    }

    public function testLogin_UserInMembersDbAndValidSuperViewer_SuccessSeePortal()
    {
        // Arrange
        $credentials = [
            'username' => $this->superViewerUsername,
            'password' => $this->superViewerPassword,
        ];

        // Act
        // Check that logging in was successful and we landed on dashboard.
        $this->visit('/auth/login')
            ->see('Sign in')
            ->submitForm('Sign in', $credentials)
            ->seePageIs('/')
            ->see('Welcome to the training portal');

        // Assert
        $this->assertFalse(Auth::user()->isBranchAdmin());
        $this->assertFalse(Auth::user()->isRegionAdmin());
        $this->assertTrue(Auth::user()->isSuperViewer());
        $this->assertFalse(Auth::user()->isSuperUser());
    }

    public function testLogin_UserInMembersDbButNoAdminPerms_FailsLoginNotEnoughPermsMessage()
    {
        // Arrange
        $credentials = [
            'username' => $this->basicUserUsername,
            'password' => $this->basicUserPassword,
        ];

        // Act
        // Check that logging in was successful and we landed on dashboard.
        $this->visit('/auth/login')
            ->see('Sign in')
            ->submitForm('Sign in', $credentials)
            ->seePageIs('/auth/login')
            ->see('Access Denied');

        // Assert
        $this->assertNull(Auth::user());
    }

    public function testLogin_UserNotInMembersDb_FailsLoginNotEnoughPermsMessage()
    {
        // Arrange
        $credentials = [
            'username' => 'bob',
            'password' => 'bob',
        ];

        // Act
        // Check that logging in was successful and we landed on dashboard.
        $this->visit('/auth/login')
            ->see('Sign in')
            ->submitForm('Sign in', $credentials)
            ->seePageIs('/auth/login')
            ->see('Access Denied');

        // Assert
        $this->assertNull(Auth::user());
    }

    public function testLogin_UserNotInMembersDbButIsSuper_FailsLoginNotEnoughPermsMessage()
    {
        // Arrange
        $credentials = [
            'username' => 'bob',
            'password' => 'bob',
        ];

        // Assert
        // Check that logging in was successful and we landed on dashboard.
        $this->visit('/auth/login')
            ->see('Sign in')
            ->submitForm('Sign in', $credentials)
            ->seePageIs('/auth/login')
            ->see('Access Denied');

        // Assert
        $this->assertNull(Auth::user());
    }

    public function testLogin_ConfirmLinksAreOnlyVisibleAfterLogin_Success()
    {
        // Arrange
        $credentials = [
            'username' => $this->superViewerUsername,
            'password' => $this->superViewerPassword,
        ];

        // Act
        // Check that logging in was successful and we landed on dashboard.
        $this->visit('/auth/login')
            ->see('Sign in')
            ->dontSee('Dashboard')
            ->dontSee('Course Allocations')
            ->dontSee('Budget Management')
            ->dontSee('Help')
            ->dontSee('Logout')
            ->submitForm('Sign in', $credentials)
            ->seePageIs('/')
            ->see('Welcome to the training portal')
            ->see('Dashboard')
            ->see('Course Allocations')
            ->see('Budget Management')
            ->see('Help')
            ->see('Logout');
    }

}