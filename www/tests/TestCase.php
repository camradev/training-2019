<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        // Add foreign key constraints if using SQLite, this needs to happen after the bootstrap to be able to use DB
        if (DB::connection() instanceof \Illuminate\Database\SQLiteConnection) {
            DB::statement(DB::raw('PRAGMA foreign_keys=1'));
        }

        return $app;
    }

    /**
     * Compares the attribute names on a json object against an expected list and reports any differences with a fail
     * @param $jsonObject
     * @param $expectedAttributes
     */
    protected function assertJsonObjectHasAttributes($jsonObject, $expectedAttributes)
    {
        $actualAttributes = array_keys(get_object_vars($jsonObject));
        $compareFailedMessage = [];
        // If there is no difference in the array then the following will produce empty arrays, regardless of order
        $resultAgainstExpected = array_diff($expectedAttributes, $actualAttributes);
        $resultAgainstActual = array_diff($actualAttributes, $expectedAttributes);
        if (!empty($resultAgainstExpected)) {
            $compareFailedMessage[] = "The following keys were in expected, but not in actual: \"" . implode("\", \"", $resultAgainstExpected) . "\"";
        }
        if (!empty($resultAgainstActual)) {
            $compareFailedMessage[] = "The following keys were in actual, but not in expected: \"" . implode("\", \"", $resultAgainstActual) . "\"";
        }
        if (!empty($compareFailedMessage)) {
            $this->fail(implode(PHP_EOL, $compareFailedMessage));
        }
    }

    /**
     * Assert that the response contains an exact JSON array.
     *
     * @param  array $data
     * @return $this
     */
    public function seeJsonEqualsTryNumericCheck(array $data)
    {
        // Try and do the normal seeJsonEquals logic
        $actual = json_encode(array_sort_recursive(
            json_decode($this->response->getContent(), true)
        ));

        // If they match then just return
        if (json_encode(array_sort_recursive($data)) === $actual) {
//            echo "Just matched fine";
            return $this;
        }

//        echo "Trying JSON_NUMERIC_CHECK";
        $cleanedActual = json_encode(array_sort_recursive(
            json_decode($this->response->getContent(), true)
        ), JSON_NUMERIC_CHECK);

        if (json_encode(array_sort_recursive($data), JSON_NUMERIC_CHECK) === $cleanedActual) {
//            echo "Just matched fine after JSON_NUMERIC_CHECK";
            return $this;
        }

        // Fail out on the original $actual
        $this->assertEquals(json_encode(array_sort_recursive($data)), $actual);

        return $this;
    }
}
