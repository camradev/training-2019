<?php

use App\Admin;
use App\Budget;
use App\BudgetChange;
use Carbon\Carbon;

class BudgetsControllerBudgetDistributionTest extends TestCaseWithDatabaseTransactions
{

    // Branch Secretary
    public $branchAdminUsername = '999991'; // Based on user 006671, branch is RED
    public $branchAdminPassword = 'CPLTEST1';
    // Regional Director
    public $regionAdminUsername = '999992'; // Based on user 111678, two regions, covering 20 branches.
    public $regionAdminPassword = 'CPLTEST2';
    // Ryan
    public $superUserUsername = '999993'; // SuperUser
    public $superUserPassword = 'CPLTEST3';
    // Dan
    public $superViewerUsername = '999994'; // SuperViewer
    public $superViewerPassword = 'CPLTEST4';

    public $basicUserUsername = '999995'; // No perms
    public $basicUserPassword = 'CPLTEST5';

    protected function configureDatabase()
    {
        parent::configureDatabase();

        Admin::create([
            'member_id' => $this->superUserUsername, //999993
            'can_edit'  => 1
        ]);

        Admin::create([
            'member_id' => $this->superViewerUsername, //999994
            'can_edit'  => 0
        ]);

        Admin::create([
            'member_id' => 'bob', //No member record in db for this user, only in Admins
            'can_edit'  => 0
        ]);
    }

    public function testNavigation_BranchAdminLoggedInCannotSeeBudgetManagementLinkOrVisitLinkManually_SuccessRedirectedBackToOriginatingPage()
    {
        # Note: SuperViewer cannot actually update the budget though - that is outside scope of this test.

        // Arrange
        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        // Check that logging in was successful and we don't see the Create Action, so button is missing.
        $this->visit(route('dashboard'))
            ->seePageIs('/')
            ->dontSee('Budget Management')
            ->visit(route('manageBudgetDistribution'))
            ->seePageIs('/');
    }

    public function testNavigation_RegionAdminLoggedInCannotSeeBudgetManagementLinkOrVisitLinkManually_SuccessRedirectedBackToOriginatingPage()
    {

        // Arrange
        Eloquent::unguard();
        Budget::create(['branch_code' => 'RED', 'amount' => 100, 'year' => Carbon::now()->year]);
        Eloquent::reguard();

        Session::start();

        Auth::loginUsingId($this->regionAdminUsername);
        Auth::user()->regionAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];
        Auth::user()->adminRegionList = [
            'EANG' => "East Anglia"
        ];

        // Check that logging in was successful and we don't see the Create Action, so button is missing.
        $this->visit(route('dashboard'))
            ->seePageIs('/')
            ->dontSee('Budget Management')
            ->visit(route('manageBudgetDistribution'))
            ->seePageIs('/');
    }

    public function testNavigation_SuperViewerLoggedInCanSeeBudgetManagementLinksAndFollowLink_Success()
    {
        // Arrange
        Session::start();

        Auth::loginUsingId($this->superViewerUsername);

        // Check that logging in was successful and we don't see the Create Action, so button is missing.
        $this->visit(route('dashboard'))
            ->seePageIs('/')
            ->see('Budget Management')
            ->visit(route('manageBudgetDistribution'))
            ->seePageIs('/budgets/manage');
    }

    public function testStatus_BranchAdminUpdatesBudgetManagementViaEndpoint_ErrorNotEnoughPerms()
    {
        // Arrange

        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            '_token' => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('saveBudgetJson'), $parameters);

        // Assert
        $this->assertResponseStatus('403');
        $this->seeJsonEquals([
            "message" => "User does not have permissions to change budgets."
        ]);
    }

    public function testStatus_RegionAdminUpdatesBudgetManagementViaEndpoint_ErrorNotEnoughPerms()
    {
        // Arrange
        Eloquent::unguard();
        Budget::create(['branch_code' => 'RED', 'amount' => 100, 'year' => Carbon::now()->year]);
        Eloquent::reguard();

        Session::start();

        Auth::loginUsingId($this->regionAdminUsername);
        Auth::user()->regionAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];
        Auth::user()->adminRegionList = [
            'EANG' => "East Anglia"
        ];

        $parameters = [
            '_token' => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('saveBudgetJson'), $parameters);

        // Assert
        $this->assertResponseStatus('403');
        $this->seeJsonEquals([
            "message" => "User does not have permissions to change budgets."
        ]);
    }

    public function testStatus_SuperViewerUpdatesBudgetManagementViaEndpoint_ErrorNotEnoughPerms()
    {
        // Arrange

        Session::start();

        Auth::loginUsingId($this->superViewerUsername);

        $parameters = [
            '_token' => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('saveBudgetJson'), $parameters);

        // Assert
        $this->assertResponseStatus('403');
        $this->seeJsonEquals([
            "message" => "User does not have permissions to change budgets."
        ]);
    }

    public function testNavigation_SuperUserLoggedInCanSeeBudgetManagementLinksAndFollow_Success()
    {

        // Arrange
        Session::start();

        Auth::loginUsingId($this->superUserUsername);

        // Check that logging in was successful and we don't see the Create Action, so button is missing.
        $this->visit(route('dashboard'))
            ->seePageIs('/')
            ->see('Budget Management')
            ->visit(route('manageBudgetDistribution'))
            ->seePageIs('/budgets/manage');
    }

    public function testStatus_SuperUserUpdatesBudgetManagementViaEndpoint_SuccessAndChangeLogUpdated()
    {
        // Arrange
        $year = Carbon::today()->year;
        Eloquent::unguard();
        Budget::create([
            'branch_code' => 'BEN',
            'amount'      => 2000,
            'year'        => $year,
        ]);
        Eloquent::reguard();

        // Assert
        $this->assertEquals(2000, Budget::whereBranchCode('BEN')->first()->amount);

        // Arrange
        Session::start();

        Auth::loginUsingId($this->superUserUsername);

        $parameters = [
            'budgetChanges' => [
                ['branch_code' => 'BEN', 'new_amount' => 450]
            ],
            '_token' => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('saveBudgetJson'), $parameters);

        // Assert
        $this->assertResponseStatus('200');
        $this->seeJsonEquals([
            "Result" => "OK"
        ]);
        // Check budget written to DB as expected
        $this->assertEquals(450, Budget::whereBranchCode('BEN')->first()->amount);

        // Check change log updated
        $latestBudgetChangeLogEntry = BudgetChange::orderBy('created_at', 'desc')->whereBranchCode('BEN')->first()->toArray();
        $this->seeJson([
            'old_amount'=>2000,
            'new_amount' => 450,
            'id' => '1',
            //'budget_event_id' => '9f1d41f9-077e-41e1-b3fc-f6de051ef3c7',
            'budget_id' => '1',
            'branch_code' => 'BEN',
            'branch_description' => 'North Bedfordshire',
            'changed_by_member_id' => '999993',
            'changed_by_member_name' => 'cpltest3 test',
            'budget_year' => '2015',
        ], $latestBudgetChangeLogEntry);
    }
}