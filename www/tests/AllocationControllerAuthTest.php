<?php

class AllocationsControllerAuthTest extends TestCaseWithDatabaseTransactions
{

    protected function configureDatabase()
    {
        parent::configureDatabase();
    }

    public function testAuth_TryAccessListRouteWithoutAuth_RedirectedToLogin()
    {
        // Arrange

        Session::start();

        $parameters = [
            '_token' => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('allocationsList'), $parameters);

        // Assert
        $this->assertResponseStatus(302);
        $this->assertRedirectedTo('/auth/login');
    }

    public function testAuth_TryAccessCreateRouteWithoutAuth_RedirectedToLogin()
    {

        // Arrange

        Session::start();

        $parameters = [
            'member_id'   => '105681', // Branch is BEN
            'provider_id' => 4,
            'course_id'   => 4,
            'status_id'   => 1,
            '_token'      => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('allocationsCreate'), $parameters);

        // Assert
        $this->assertResponseStatus(302);
        $this->assertRedirectedTo('/auth/login');
    }

    public function testAuth_TryAccessUpdateRouteWithoutAuth_RedirectedToLogin()
    {

        // Arrange

        Session::start();

        $parameters = [
            'member_id'   => '105681', // Branch is BEN
            'provider_id' => 4,
            'course_id'   => 4,
            'status_id'   => 2,
            '_token'      => csrf_token()
        ];

        // Act
        // Update an allocation
        $this->post(route('allocationsCreate'), $parameters);

        // Assert
        $this->assertResponseStatus(302);
        $this->assertRedirectedTo('/auth/login');

    }

}