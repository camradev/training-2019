<?php

class CoursesControllerAuthTest extends TestCaseWithDatabaseTransactions
{

    protected function configureDatabase()
    {
        parent::configureDatabase();
    }

    public function testAuth_TryAccessCourseOptionsRoutesWithoutAuth_RedirectedToLogin()
    {
        // Arrange

        Session::start();

        $parameters = [
            '_token' => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('courseOptions'), $parameters);

        // Assert
        $this->assertResponseStatus(302);
        $this->assertRedirectedTo('/auth/login');
    }
}