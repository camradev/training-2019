<?php

use App\CamraUser;

class CamraUserTest extends TestCaseWithDatabaseTransactions
{

    protected function configureDatabase()
    {
        parent::configureDatabase();
    }

    public function testCamraUser_CombineZeroBranchAdminBranchesAndZeroRegionAdminBranches_ReturnValidArray() {
        // Arrange
        $user = new CamraUser([]);
        $user->branchAdminBranchList = [];
        $user->regionAdminBranchList = [];

        // Act
        $results = $user->getAdminCombinedBranchList();

        // Assert
        $this->assertEquals(
            [],
            $results
        );
    }

    public function testCamraUser_CombineZeroBranchAdminBranchesAndOneRegionAdminBranches_ReturnValidArray() {
        // Arrange
        $user = new CamraUser([]);
        $user->branchAdminBranchList = [];
        $user->regionAdminBranchList = [
            'RED' => 'Redditch & Bromsgrove'
        ];

        // Act
        $results = $user->getAdminCombinedBranchList();

        // Assert
        $this->assertEquals(
            [
                'RED' => 'Redditch & Bromsgrove'
            ],
            $results
        );

        // Act
        $results = $user->getAdminCombinedBranchIdList();

        // Assert
        $this->assertEquals(
            [
                'RED'
            ],
            $results
        );
    }

    public function testCamraUser_CombineOneBranchAdminBranchesAndOneRegionAdminBranches_ReturnValidArray() {
        // Arrange
        $user = new CamraUser([]);
        $user->branchAdminBranchList = [
            'ABC' => 'Abercolwyn'
        ];
        $user->regionAdminBranchList = [
            'RED' => 'Redditch & Bromsgrove'
        ];

        // Act
        $results = $user->getAdminCombinedBranchList();

        // Assert
        $this->assertEquals(
            [
                'ABC' => 'Abercolwyn',
                'RED' => 'Redditch & Bromsgrove'
            ],
            $results
        );

        // Act
        $results = $user->getAdminCombinedBranchIdList();
        sort($results);

        // Assert
        $this->assertEquals(
            [
                'ABC',
                'RED'
            ],
            $results
        );
    }

    public function testCamraUser_CombineOneBranchAdminBranchesAndZeroRegionAdminBranches_ReturnValidArray() {
        // Arrange
        $user = new CamraUser([]);
        $user->branchAdminBranchList = [
            'ABC' => 'Abercolwyn'
        ];
        $user->regionAdminBranchList = [];

        // Act
        $results = $user->getAdminCombinedBranchList();

        // Assert
        $this->assertEquals(
            [
                'ABC' => 'Abercolwyn'
            ],
            $results
        );

        // Act
        $results = $user->getAdminCombinedBranchIdList();

        // Assert
        $this->assertEquals(
            [
                'ABC'
            ],
            $results
        );
    }

    public function testCamraUser_CombineTwoBranchAdminBranchesAndTwoRegionAdminBranches_ReturnValidArray() {
        // Arrange
        $user = new CamraUser([]);
        $user->branchAdminBranchList = [
            'ABC' => 'Abercolwyn',
            'BEN' => 'North Bedfordshire',
        ];
        $user->regionAdminBranchList = [
            'RED' => 'Redditch & Bromsgrove',
            'DUM' => 'Dumfries & Stewartry',
        ];

        // Act
        $results = $user->getAdminCombinedBranchList();

        // Assert
        $this->assertEquals(
            [
                'ABC' => 'Abercolwyn',
                'BEN' => 'North Bedfordshire',
                'RED' => 'Redditch & Bromsgrove',
                'DUM' => 'Dumfries & Stewartry',
            ],
            $results
        );

        // Act
        $results = $user->getAdminCombinedBranchIdList();
        sort($results);

        // Assert
        $this->assertEquals(
            [
                'ABC',
                'BEN',
                'DUM',
                'RED',
            ],
            $results
        );
    }

}