<?php

class ProvidersControllerAuthTest extends TestCaseWithDatabaseTransactions
{

    protected function configureDatabase()
    {
        parent::configureDatabase();
    }

    public function testAuth_TryAccessProviderOptionsRouteWithoutAuth_RedirectedToLogin()
    {
        // Arrange

        Session::start();

        $parameters = [
            '_token' => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('providerOptions'), $parameters);

        // Assert
        $this->assertResponseStatus(302);
        $this->assertRedirectedTo('/auth/login');
    }

}