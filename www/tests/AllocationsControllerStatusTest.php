<?php

use App\Admin;
use App\Budget;
use App\Course;
use App\CourseAllocation;
use App\CourseProvider;
use Carbon\Carbon;

class AllocationsControllerStatusTest extends TestCaseWithDatabaseTransactions
{
    // Branch Secretary
    public $branchAdminUsername = '214103'; // Based on user 214103, branch is RED

    protected function configureDatabase()
    {
        parent::configureDatabase();
    }

    public function testStatus_CreateAllocationWithValidStatus_Success()
    {

        // Arrange

        Eloquent::unguard();
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes' => 'Some notes']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes' => 'Some notes', 'cost_pounds' => 10]);
        Budget::create(['branch_code'=>'RED', 'year'=>Carbon::now()->year, 'amount' => 100]);
        Eloquent::reguard();

        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            'member_id'   => '172316', // Branch is BEN
            'provider_id' => 4,
            'course_id'   => 4,
            'status_id'   => 4,
            'budget_source' => 'branch',
            '_token'      => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('allocationsCreate'), $parameters);

        // Assert
        $this->assertResponseOk();

        // Get the id of the created record so we can compare.
        $latestAllocation = CourseAllocation::latest()->first();

        // Compare latest record to compare to response
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "OK",
            "Record" => [
                "id"                   => $latestAllocation["id"],
                "member_id"            => "172316",
                "member_name"          => "Peter Sutton",
                "branch_name"          => "Redditch & Bromsgrove",
                "created_at"           => $latestAllocation["created_at"]->toDateTimeString(),
                "course_name"          => "P4 Course four",
                "course_cost_pounds"   => 10.00,
                "status_id"            => 4,
                "status_name"          => "Failed",
                "budget_source" => "branch",
                "course_provider_name" => "Provider four"
            ]
        ]);
    }

    public function testStatus_CreateAllocationWithInvalidStatus_ErrorInvalidStatus()
    {

        // Arrange
        Eloquent::unguard();
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes' => 'Some notes']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes' => 'Some notes', 'cost_pounds' => 10]);
        Eloquent::reguard();

        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            'member_id'   => '105681',
            'provider_id' => 4,
            'course_id'   => 4,
            'status_id'   => 5,
            'budget_source'   => 'branch',
            '_token'      => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('allocationsCreate'), $parameters);

        // Assert
        $this->assertResponseOK();

        // Get the id of the created record so we can compare.
        $latestAllocation = CourseAllocation::latest()->first();
        $this->assertNull($latestAllocation);

        // Compare latest record to compare to response
        $this->seeJsonEqualsTryNumericCheck([
            "Result"  => "ERROR",
            "Message" => "Status with ID '5' does not exist."
        ]);

    }

    public function testStatus_UpdateAllocationWithValidStatus_Success()
    {

        // Arrange

        Eloquent::unguard();
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes' => 'Some notes']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes' => 'Some notes', 'cost_pounds' => 10]);
        Budget::create(['branch_code'=>'RED', 'year'=>Carbon::now()->year, 'amount' => 100]);
        Eloquent::reguard();

        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            'member_id'   => '172316', // Branch is BEN
            'provider_id' => 4,
            'course_id'   => 4,
            'status_id'   => 4,
            'budget_source' => 'branch',
            '_token'      => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('allocationsCreate'), $parameters);

        // Assert
        $this->assertResponseOk();

        // Get the id of the created record so we can compare.
        $latestAllocation = CourseAllocation::latest()->first();

        // Compare latest record to compare to response
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "OK",
            "Record" => [
                "id"                   => $latestAllocation["id"],
                "member_id"            => "172316",
                "member_name"          => "Peter Sutton",
                "branch_name"          => "Redditch & Bromsgrove",
                "created_at"           => $latestAllocation["created_at"]->toDateTimeString(),
                "course_name"          => "P4 Course four",
                "course_cost_pounds"   => 10.00,
                "status_id"            => 4,
                "status_name"          => "Failed",
                "budget_source" => "branch",
                "course_provider_name" => "Provider four"
            ]
        ]);

        // Act

        $parameters = [
            'id'        => $latestAllocation["id"],
            'status_id' => 3,
            '_token'    => csrf_token()
        ];

        $this->post(route('allocationsUpdate'), $parameters);

        // Assert
        $this->assertResponseOk();

        // Get the id of the update record so we can compare.
        $latestAllocation = CourseAllocation::latest()->first();
        $this->assertEquals(3, $latestAllocation->status_id);
        // Compare latest record to compare to response
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "OK",
            "Record" => [
                "status_id"   => $latestAllocation->status_id,
                "status_name" => "Passed"
            ]
        ]);
    }

    public function testStatus_UpdateAllocationWithInvalidStatus_Error()
    {

        // Arrange

        Eloquent::unguard();
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes' => 'Some notes']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes' => 'Some notes', 'cost_pounds' => 10]);
        Budget::create(['branch_code'=>'RED', 'year'=>Carbon::now()->year, 'amount' => 100]);
        Eloquent::reguard();

        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            'member_id'   => '172316', // Branch is RED
            'provider_id' => 4,
            'course_id'   => 4,
            'status_id'   => 4,
            'budget_source' => 'branch',
            '_token'      => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('allocationsCreate'), $parameters);

        // Assert
        $this->assertResponseOk();

        // Get the id of the created record so we can compare.
        $latestAllocation = CourseAllocation::latest()->first();

        // Compare latest record to compare to response
        $this->seeJsonEqualsTryNumericCheck([
            "Result" => "OK",
            "Record" => [
                "id"                   => $latestAllocation["id"],
                "member_id"            => "172316",
                "member_name"          => "Peter Sutton",
                "branch_name"          => "Redditch & Bromsgrove",
                "created_at"           => $latestAllocation["created_at"]->toDateTimeString(),
                "course_name"          => "P4 Course four",
                "course_cost_pounds"   => 10.00,
                "status_id"            => 4,
                "status_name"          => "Failed",
                "budget_source" => "branch",
                "course_provider_name" => "Provider four"
            ]
        ]);

        // Act

        $parameters = [
            'id'        => $latestAllocation["id"],
            'status_id' => 5,
            '_token'    => csrf_token()
        ];

        $this->post(route('allocationsUpdate'), $parameters);

        // Assert
        $this->assertResponseOK();

        $this->assertEquals(1, CourseAllocation::count());
        $latestAllocation = CourseAllocation::latest()->first();

        // Compare latest record to compare to response
        $this->assertEquals(4, $latestAllocation->status_id);
        $this->seeJsonEqualsTryNumericCheck([
            "Result"  => "ERROR",
            "Message" => "Status with ID '5' does not exist."
        ]);
    }

}