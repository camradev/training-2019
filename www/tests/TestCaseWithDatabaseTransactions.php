<?php
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TestCaseWithDatabaseTransactions extends TestCase
{
    use DatabaseTransactions;

    // Object used to save the underlying pdo connection between tests
    private static $pdo;
    private static $oldClassName;

    /**
     * This runs before every test
     */
    public function setUp()
    {
        parent::setUp();
        // Have we got a saved pdo object?
        if (self::$pdo) {
            // Yes - Restore the connection to being that saved object
            DB::connection()->setPdo(self::$pdo);
        } else {
            // No - Set up the database and save the connection
            self::$pdo = DB::connection()->getPdo();
        }

        // Have we changed unit test class files?
        $className = get_class($this);
        if ($className != self::$oldClassName) {
            // YES - Reset the database and configure it
            Artisan::call('migrate:refresh');
            $this->configureDatabase();
            // Remember which class we've done this for
            self::$oldClassName = $className;
        }
    }

    /**
     * Override this in the test class to configure anything for the database
     */
    protected function configureDatabase() {
    }
}