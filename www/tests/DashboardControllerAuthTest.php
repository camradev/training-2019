<?php

class DashboardControllerAuthTest extends TestCaseWithDatabaseTransactions
{

    protected function configureDatabase()
    {
        parent::configureDatabase();
    }

    public function testAuth_TryAccessDashboardRoutesWithoutAuth_RedirectedToLogin()
    {
        // Should redirect to login screen

        // Act
        $this->visit(route('dashboard'))
            ->see('Sign in')
            ->seePageIs('/auth/login');
    }

}