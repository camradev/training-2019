<?php

use App\Admin;
use App\Budget;
use App\BudgetChange;
use App\Course;
use App\CourseAllocation;
use App\CourseProvider;
use Carbon\Carbon;

class BudgetsControllerNationalBudgetTest extends TestCaseWithDatabaseTransactions
{

    // Branch Secretary
    public $branchAdminUsername = '999991'; // Based on user 006671, branch is RED
    public $branchAdminPassword = 'CPLTEST1';
    // Regional Director
    public $regionAdminUsername = '999992'; // Based on user 111678, two regions, covering 20 branches.
    public $regionAdminPassword = 'CPLTEST2';
    // Ryan
    public $superUserUsername = '999993'; // SuperUser
    public $superUserPassword = 'CPLTEST3';
    // Dan
    public $superViewerUsername = '999994'; // SuperViewer
    public $superViewerPassword = 'CPLTEST4';

    public $basicUserUsername = '999995'; // No perms
    public $basicUserPassword = 'CPLTEST5';

    protected function configureDatabase()
    {
        parent::configureDatabase();

        Admin::create([
            'member_id' => $this->superUserUsername, //999993
            'can_edit'  => 1
        ]);

        Admin::create([
            'member_id' => $this->superViewerUsername, //999994
            'can_edit'  => 0
        ]);

        Admin::create([
            'member_id' => 'bob', //No member record in db for this user, only in Admins
            'can_edit'  => 0
        ]);

        $year = Carbon::now()->year;
        Eloquent::unguard();
        Budget::create(['branch_code' => 'NATIONAL', 'amount' => 30000, 'year' => $year]);
        Budget::create(['branch_code' => 'RED', 'amount' => 100, 'year' => $year]);
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes' => 'Some notes']);
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'email_notes' => 'Some notes', 'cost_pounds' => 100]);
        CourseAllocation::create([
            'id'          => 1,
            'member_id'   => '212924',
            'member_name' => 'Steve Humeniuk',
            'branch_id'   => 'BED',
            'branch_name' => 'North Bedfordshire',
            'course_id'   => 4,
            'status_id'   => 1,
            'created_at'  => "$year-01-01 10:10:10",
            'updated_at'  => "$year-01-01 10:10:10"
        ]);
        Eloquent::reguard();
    }

    public function testNavigation_BranchAdminLoggedInCannotSeeNationalBudgetManagementLinkOrVisitLinkManually_SuccessRedirectedBackToDashboard()
    {
        # Note: SuperViewer cannot actually update the budget though - that is outside scope of this test.

        // Arrange
        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        // Check that logging in was successful and we don't see the Create Action, so button is missing.
        $this->visit(route('dashboard'))
            ->seePageIs('/')
            ->dontSee('Budget Management')
            ->visit(route('viewNationalBudget'))
            ->seePageIs(route('dashboard'));
    }

    public function testNavigation_RegionAdminLoggedInCannotSeeNationalBudgetManagementLinkOrVisitLinkManually_SuccessRedirectedBackToDashboard()
    {
        // Arrange
        Eloquent::unguard();
        Budget::create(['branch_code' => 'RED', 'amount' => 100, 'year' => Carbon::now()->year]);
        Eloquent::reguard();

        Session::start();

        Auth::loginUsingId($this->regionAdminUsername);
        Auth::user()->regionAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];
        Auth::user()->adminRegionList = [
            'EANG' => "East Anglia"
        ];

        // Check that logging in was successful and we don't see the Create Action, so button is missing.
        $this->visit(route('dashboard'))
            ->seePageIs('/')
            ->dontSee('Budget Management')
            ->seePageIs(route('dashboard'));
    }

    public function testNavigation_SuperViewerLoggedInCanSeeNationalBudgetManagementLinksAndFollowLink_Success()
    {
        # Note: SuperViewer cannot actually update the budget though - that is outside scope of this test.

        // Arrange
        Session::start();

        Auth::loginUsingId($this->superViewerUsername);

        // Check that logging in was successful and we don't see the Create Action, so button is missing.
        $this->visit(route('dashboard'))
            ->seePageIs('/')
            ->visit(route('viewNationalBudget'))
            ->see('Budget Management')
            ->type('30500', 'nationalBudget')
            ->press('submit')
            // The 'Are you sure?' modal can't be clicked for some reason, as if it's already been clicked.
            //->click('OK')
            ->see("You don't have permissions to modify budgets.")
            ->seePageIs(route('viewNationalBudget'))
            ->see('30000');

        // Get the budget record
        $budget = Budget::where('branch_code', 'NATIONAL')->first();
        // Confirm that it matches the original value
        $this->assertEquals('30000', $budget->amount);
    }

    public function testStatus_BranchAdminUpdatesNationalBudgetViaEndpoint_ErrorNotEnoughPerms()
    {
        // Arrange

        Session::start();

        Auth::loginUsingId($this->branchAdminUsername);
        Auth::user()->branchAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            '_token' => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('updateNationalBudget'), $parameters);

        // Assert
        $this->assertResponseStatus('302');
        $this->followRedirects();
        $this->assertContains("You don&#039;t have permissions to modify budgets.", $this->response->getContent());
    }

    public function testStatus_RegionAdminUpdatesNationalBudgetViaEndpoint_ErrorNotEnoughPerms()
    {
        // Arrange

        Session::start();

        Auth::loginUsingId($this->regionAdminUsername);
        Auth::user()->adminRegionList = [
            'EANG' => "East Anglia"
        ];
        Auth::user()->regionAdminBranchList = ['RED' => 'Redditch & Bromsgrove'];

        $parameters = [
            '_token' => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('updateNationalBudget'), $parameters);

        // Assert
        $this->assertResponseStatus('302');
        $this->followRedirects();
        $this->assertContains("You don&#039;t have permissions to modify budgets.", $this->response->getContent());
    }

    public function testStatus_SuperViewerUpdatesNationalBudgetViaEndpoint_ErrorNotEnoughPerms()
    {
        // Arrange

        Session::start();

        Auth::loginUsingId($this->superViewerUsername);

        $parameters = [
            '_token' => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('updateNationalBudget'), $parameters);

        // Assert
        $this->assertResponseStatus('302');
        $this->followRedirects();
        $this->assertContains("You don&#039;t have permissions to modify budgets.", $this->response->getContent());
    }

    public function testNavigation_SuperUserLoggedInCanSeeNationalBudgetManagementLinksAndFollow_Success()
    {
        // Arrange
        Session::start();

        Auth::loginUsingId($this->superUserUsername);

        // Check that logging in was successful and we don't see the Create Action, so button is missing.
        $this->visit(route('dashboard'))
            ->seePageIs('/')
            ->see('Budget Management')
            ->visit(route('viewNationalBudget'))
            ->seePageIs(route('viewNationalBudget'))
            ->type('30500', 'nationalBudget')
            ->press('submit')
            // The 'Are you sure?' modal can't be clicked for some reason, as if it's already been clicked.
            //->click('OK')
            ->seePageIs(route('viewNationalBudget'))
            ->see('30500');

        // Get the budget record
        $budget = Budget::where('branch_code', 'NATIONAL')->first();
        // Confirm that it matches the updated value
        $this->assertEquals('30500', $budget->amount);

    }

    public function testNavigation_SuperUserLoggedInTrySetNationalBudgetWithNoYearValue_ErrorYearRequired()
    {
        // Arrange
        Session::start();

        Auth::loginUsingId($this->superUserUsername);

        $parameters = [
            'nationalBudget' => 30000,
            '_token'         => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('updateNationalBudget'), $parameters);
        // Assert
        $this->assertResponseStatus(302);

        // DEBUG - See the errors
        #dd($errors = session('errors')->getMessages());

        $this->assertSessionHasErrors(['year' => 'A year value must be provided.']);

        // Get the budget record
        $budget = Budget::where('branch_code', 'NATIONAL')->first();
        // Confirm that it matches the original value
        $this->assertEquals('30000', $budget->amount);
    }

    public function testNavigation_SuperUserLoggedInTrySetNationalBudgetWithNonNumericYear_ErrorYearRequired()
    {
        // Arrange
        Session::start();

        Auth::loginUsingId($this->superUserUsername);

        $parameters = [
            'year' => 'bob',
            'nationalBudget' => 30000,
            '_token'         => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('updateNationalBudget'), $parameters);
        // Assert
        $this->assertResponseStatus(302);

        // DEBUG - See the errors
        #dd($errors = session('errors')->getMessages());

        $this->assertSessionHasErrors(['year' => 'The year value must be numeric.']);

        // Get the budget record
        $budget = Budget::where('branch_code', 'NATIONAL')->first();
        // Confirm that it matches the original value
        $this->assertEquals('30000', $budget->amount);
    }

    public function testNavigation_SuperUserLoggedInTrySetNationalBudgetWithYearTooLow_ErrorYearRequired()
    {
        // Arrange
        Session::start();

        Auth::loginUsingId($this->superUserUsername);

        $parameters = [
            'year' => 2014,
            'nationalBudget' => 30000,
            '_token'         => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('updateNationalBudget'), $parameters);
        // Assert
        $this->assertResponseStatus(302);

        // DEBUG - See the errors
        #dd($errors = session('errors')->getMessages());

        $this->assertSessionHasErrors(['year' => 'The year value cannot be less than the current year.']);

        // Get the budget record
        $budget = Budget::where('branch_code', 'NATIONAL')->first();
        // Confirm that it matches the original value
        $this->assertEquals('30000', $budget->amount);
    }

    public function testNavigation_SuperUserLoggedInTrySetNationalBudgetWithEmptyValue_ErrorBudgetRequired()
    {
        // Arrange
        Session::start();

        Auth::loginUsingId($this->superUserUsername);

        $parameters = [
            'year' => Carbon::now()->year,
            'nationalBudget' => '',
            '_token'         => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('updateNationalBudget'), $parameters);
        // Assert
        #dump($this->response->content());

        $this->assertResponseStatus(302);
        #$this->assertSessionHasErrors(['nationalBudget']);
        $this->assertSessionHasErrors(['nationalBudget' => 'A new value for the national budget must be provided.']);

        // Get the budget record
        $budget = Budget::where('branch_code', 'NATIONAL')->first();
        // Confirm that it matches the original value
        $this->assertEquals('30000', $budget->amount);
    }

    public function testNavigation_SuperUserLoggedInTrySetNationalBudgetWithTooSmallValue_ErrorBudgetRequired()
    {
        // Arrange
        Session::start();

        Auth::loginUsingId($this->superUserUsername);

        $parameters = [
            'year' => Carbon::now()->year,
            'nationalBudget' => 90,
            '_token'         => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('updateNationalBudget'), $parameters);
        // Assert
        $this->assertResponseStatus(302);

        // DEBUG - See the errors
        #dd($errors = session('errors')->getMessages());

        $this->assertSessionHasErrors(['nationalBudget' => 'The new value for the national budget cannot be less than the current usage.']);

        // Get the budget record
        $budget = Budget::where('branch_code', 'NATIONAL')->first();
        // Confirm that it matches the original value
        $this->assertEquals('30000', $budget->amount);
    }

    public function testNavigation_SuperUserLoggedInTrySetNationalBudgetWithNonNumericValue_ErrorNumericRequired()
    {
        // Arrange
        Session::start();

        Auth::loginUsingId($this->superUserUsername);

        $parameters = [
            'year' => Carbon::now()->year,
            'nationalBudget' => 'bob',
            '_token'         => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('updateNationalBudget'), $parameters);
        // Assert
        $this->assertResponseStatus(302);

        // DEBUG - See the errors
        #dd($errors = session('errors')->getMessages());

        $this->assertSessionHasErrors(['nationalBudget' => 'The new value for the national budget must be numeric.']);

        // Get the budget record
        $budget = Budget::where('branch_code', 'NATIONAL')->first();
        // Confirm that it matches the original value
        $this->assertEquals('30000', $budget->amount);
    }

    public function testNavigation_BudgetChangeLogUpdatedAfterSuperUserChangesNationalBudget_SuccessLogChanged()
    {
        // Arrange
        Session::start();

        Auth::loginUsingId($this->superUserUsername);

        $year = Carbon::now()->year;
        $parameters = [
            'year' => $year,
            'nationalBudget' => 30500,
            '_token' => csrf_token()
        ];

        // Act
        // Create new allocation
        $this->post(route('updateNationalBudget'), $parameters);

        // Assert
        // Should be redirected after success
        $this->assertResponseStatus('302');

        // Check budget written to DB as expected
        $this->assertEquals(30500, Budget::whereBranchCode('NATIONAL')->first()->amount);

        // Check change log updated
        $latestBudgetChangeLogEntry = BudgetChange::orderBy('created_at', 'desc')->whereBranchCode('NATIONAL')->first()->toArray();

        // Check that the expected changes are found in that log entry
        $this->assertArraySubset([
            'old_amount' => 30000,
            'new_amount' => 30500,
            'id' => '1',
            //'budget_event_id' => '9f1d41f9-077e-41e1-b3fc-f6de051ef3c7',
            'budget_id' => '1',
            'branch_code' => 'NATIONAL',
            'branch_description' => 'National Budget',
            'changed_by_member_id' => '999993',
            'changed_by_member_name' => 'cpltest3 test',
            'budget_year' => "$year",
        ], $latestBudgetChangeLogEntry);

    }

}