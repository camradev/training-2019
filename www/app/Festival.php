<?php

namespace App;

use Carbon\Carbon;
use Eloquent;

/**
 * App\Festival
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $organiser_member_id
 * @property-read mixed $name_with_code
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Budget[] $budgets
 * @property-read \App\Budget $budget
 * @property-read mixed $current_budget_used
 */
class Festival extends Eloquent
{
    public $timestamps = false;
    protected $table = "festivals";

    protected $visible = ['code', 'name', 'name_with_code', 'budgets', 'budget', 'current_budget_used'];
    protected $appends = ['name_with_code', 'current_budget_used'];

    public function getNameWithCodeAttribute() {
        return $this->attributes['name']. ' ('. $this->code.')';
    }

    public function budgets() {
        return $this->hasMany(Budget::class, "branch_code", "code");
    }

    public function budget() {
        return $this->hasOne(Budget::class, "branch_code", "code")->where("year", Carbon::now()->year);
    }

    /**
     * @return float
     */
    public function getCurrentBudgetUsedAttribute() {
        return CourseAllocation::getBranchUsageForYear(Carbon::now()->year, $this->code);
    }
}
