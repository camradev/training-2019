<?php

namespace App;

use Eloquent;

/**
 * App\BudgetChange
 *
 * @property integer $id
 * @property string $budget_event_id
 * @property integer $budget_id
 * @property string $branch_code
 * @property string $branch_description
 * @property string $changed_by_member_id
 * @property string $changed_by_member_name
 * @property float $old_amount
 * @property float $new_amount
 * @property integer $budget_year
 * @property integer $increase_request_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class BudgetChange extends Eloquent
{
}
