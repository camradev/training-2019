<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Eloquent;

/**
 * App\BudgetIncreaseRequests
 *
 * @property integer $id
 * @property string $member_id
 * @property string $member_name
 * @property string $branch_code
 * @property string $branch_name
 * @property string $description
 * @property string $escalated_at
 * @property string $resolved_at
 * @property string $resolved_by_member_id
 * @property string $resolved_by_member_name
 * @property integer $resolution_type_id
 * @property string $resolution_reason
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class BudgetIncreaseRequests extends Eloquent
{
    protected $visible = ['id', 'member_id', 'member_name', 'branch_code', 'branch_name', 'description', 'created_at'];

    protected $fillable = array('member_id', 'member_name', 'branch_code', 'branch_name', 'description', 'resolution_reason' ,'escalated_at');

    public static function openBudgetRequestDetails($code)
    {
        $details = BudgetIncreaseRequests::where('branch_code',$code)
            ->orderBy('created_at', 'desc')
            ->whereNull('resolution_type_id')
            #->select('id', 'created_at', 'description')
            ->first();
        return $details;
    }

    public static function addRequest($branchCode, $branchName, $description){
        // Check there isn't an open budget request for this branch.
        $openFound = self::openBudgetRequestDetails($branchCode);
        if(!$openFound){
            $currentUser = Auth::user();

            BudgetIncreaseRequests::create([
                'member_id' => $currentUser->id,
                'member_name' => $currentUser->forename.' '.$currentUser->surname,
                'branch_code' => $branchCode,
                'branch_name' => $branchName,
                'description' => $description
            ]);
            return true;
        } else {
            return false;
        }

    }

    public static function editRequest($branchCode, $description)
    {
        $openFound = self::openBudgetRequestDetails($branchCode);
        if($openFound){
            $openFound->description = $description;
            $openFound->save();
            return true;
        } else {
            return false;
        }
    }

    public static function cancelRequest($branchCode)
    {
        $openFound = self::openBudgetRequestDetails($branchCode);
        if($openFound){
            $currentUser = Auth::user();

            $openFound->resolved_at = Carbon::now();
            $openFound->resolved_by_member_id = $currentUser->id;
            $openFound->resolved_by_member_name = $currentUser->forename.' '.$currentUser->surname;
            $openFound->resolution_type_id = 3;
            $openFound->save();
            return true;
        } else {
            return false;
        }
    }

    public static function approveRequest($branchCode)
    {
        $openFound = self::openBudgetRequestDetails($branchCode);
        if($openFound){
            $currentUser = Auth::user();

            $openFound->resolved_at = Carbon::now();
            $openFound->resolved_by_member_id = $currentUser->id;
            $openFound->resolved_by_member_name = $currentUser->forename.' '.$currentUser->surname;
            #$openFound->resolution_reason = '';
            $openFound->resolution_type_id = 1;
            $openFound->save();
            return true;
        } else {
            return false;
        }
    }

    public static function denyRequest($branchCode, $denyReason)
    {
        $openFound = self::openBudgetRequestDetails($branchCode);
        if($openFound){
            $currentUser = Auth::user();

            $openFound->resolved_at = Carbon::now();
            $openFound->resolved_by_member_id = $currentUser->id;
            $openFound->resolved_by_member_name = $currentUser->forename.' '.$currentUser->surname;
            $openFound->resolution_reason = $denyReason;
            $openFound->resolution_type_id = 2;
            $openFound->save();
            return true;
        } else {
            return false;
        }
    }

    public static function getAllRequests()
    {
        $details = BudgetIncreaseRequests::orderBy('branch_code')
            ->whereNull('resolution_type_id')
            #->select('id', 'created_at', 'description')
            ->get();

        foreach($details as $request){
            // Add on the region code for each request.
            $request->region_code = getRegionCodeForBranchCode($request->branch_code);
        };

        return $details;
    }

    public static function getRequesterMemberDetails($branchCode)
    {
        $request = self::openBudgetRequestDetails($branchCode);

        $memberDetails = self::getMemberDetails($request->member_id);

        return $memberDetails;
    }

    public static function getAllOverdueRequestsThatNeedEscalating()
    {
        $details = BudgetIncreaseRequests::orderBy('branch_code')
            ->whereNull('resolution_type_id')
            ->whereNull('escalated_at')
            ->where('created_at','<=',Carbon::now()->subWeeks(2)->format('Y-m-d'))
            #->select('id', 'created_at', 'description')
            ->get();

        return $details;
    }

    public static function getEscalationContactInfo()
    {
        $contacts = BudgetIncreaseRequestEscalationContacts::all();
        $contactInfo = [];

        foreach ($contacts as $contact) {
            $contactInfo[$contact->member_id] = $memberContactDetails = self::getMemberDetails($contact->member_id);
        }
        return $contactInfo;
    }

    public static function getMemberDetails($memberId){
        return DB::connection('members')->table('members')->where('membershipnumber','=',$memberId)->selectRaw('emailMain as email, forename, surname')->first();
    }

}
