<?php

////////////
// Emails //
////////////
use App\Branch;
use App\Festival;
use App\Region;

/**
 * Returns true if the email is valid
 * @param string $email
 * @return bool
 */
function emailValid($email)
{
    // Check for valid emails: http://stackoverflow.com/questions/201323/using-a-regular-expression-to-validate-an-email-address/14075810#14075810
    preg_match("/([-!#-'*+\/-9=?A-Z^-~]+(\.[-!#-'*+\/-9=?A-Z^-~]+)*|\"([]!#-[^-~ \t]|(\\[\t -~]))+\")@[0-9A-Za-z]([0-9A-Za-z-]{0,61}[0-9A-Za-z])?(\.[0-9A-Za-z]([0-9A-Za-z-]{0,61}[0-9A-Za-z])?)+/", $email, $matches);
    return $matches ? true : false;
}

// Member Lookups

function getVolunteerDirectorMemberId(){
    $result = DB::connection('members')
        ->table('mem_committees')
        ->select(DB::raw('members.MembershipNumber as memberId, members.EmailMain as email, members.forename, members.surname'))
        ->join('members','mem_committees.memKey','=','members.MemberKey')
        ->where('mem_committees.CommCommitteeDesc','=','Volunteering')
        ->where('mem_committees.CommPositionDesc','=','Chairman')
        ->where(function ($query) {
            $query->where('mem_committees.CommStart', '<=', date('Y-m-d'))
                ->where('mem_committees.CommEnd', '>=', date('Y-m-d'))
                ->orWhere(function ($query) {
                    $query->whereNull('mem_committees.CommEnd');
                });
        })
        ->first();
    #dd($result);
   return $result;
}

//SELECT
//        `m`.`MembershipNumber` AS `membership_number`
//        #,`comm`.`CommCommitteeDesc` AS `committee_description`
//        #,`comm`.`CommPositionDesc` AS `committee_position`
//    FROM `mem_committees` `comm`
//	JOIN `members` `m` ON CONVERT( `comm`.`memKey` USING UTF8) = `m`.`MemberKey`
//    WHERE `comm`.`CommCommitteeDesc` = 'Volunteering'
//and `comm`.`CommPositionDesc` = 'Chairman'
//and (
//    (`comm`.`CommStart` <= SYSDATE() AND `comm`.`CommEnd` >= SYSDATE())
//    or ISNULL(`comm`.`CommEnd`)
//);

/////////////
// Lookups //
/////////////

/**
 * Retrieve the region code for a given branch code
 *
 * @param $branchCode
 * @return string
 */
function getRegionCodeForBranchCode($branchCode){
    return DB::connection('members')->table('branches')->whereCode($branchCode)->select('region')->first()->region;
}

function getRegionBranches(){
    // Get the list of regions with branches
    $regionBranches = Cache::remember('region_branches', 60, function() {
        return Region::with([
            // include the branches
            "branches" => function ($query) {
                // order the branches by their 'name' i.e. Description
                /** @var Builder $query */
                $query->orderBy("branches.Description");
            }
        ])
            // order the regions by their name
            ->orderBy("regions.regionname")
            ->get()->toArray();
    });
    return $regionBranches;
}

function getBranchesWithRegionInfo(){
    // Get the list of regions with branches
    $branches = Cache::remember('branches_regionInfo', 60, function() {
        $results = Branch::with("regionInfo")->get()->toArray();
        $branchLookup = [];
        foreach($results as $branch){
            $branchLookup[$branch['code']] = $branch;
        }
        return $branchLookup;
    });
    return $branches;
}

function getRegionNameForBranchCode($branchCode){
    $regionBranches = getBranchesWithRegionInfo();
    if(!isset($regionBranches[$branchCode])) return null;

    return $regionBranches[$branchCode]['region_info']['name'];
}

function getFestivalNameForCode($code) {
    $festival = Festival::where('code', $code)->first();
    return $festival->name;
}