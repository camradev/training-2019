<?php

namespace App\Providers;

use App\CamraUser;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);

        // Check whether user is in Admins table as SU or SV - if not, check if region or branch admin
        $gate->define('access-system', function ($user) {
            /** @var CamraUser $user */
            return $user->canAccessSystem();
        });

        $gate->define('view-budgets', function ($user) {
            /** @var CamraUser $user */
            return $user->canViewBudgets();
        });

        $gate->define('view-reports', function ($user) {
            /** @var CamraUser $user */
            return $user->canViewReports();
        });

        $gate->define('change-budgets', function ($user) {
            /** @var CamraUser $user */
            return $user->canChangeBudgets();
        });

    }
}
