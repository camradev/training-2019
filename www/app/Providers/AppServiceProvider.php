<?php

namespace App\Providers;

use App\Budget;
use App\CamraUser;
use App\CourseAllocation;
use App\Log;
use Auth;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Log all events for certain models
        Budget::created(function ($model) {self::addLogRecord($model, 'create');});
        Budget::updated(function ($model) {self::addLogRecord($model, 'update');});
        Budget::deleted(function ($model) {self::addLogRecord($model, 'delete');});
        CourseAllocation::created(function ($model) {self::addLogRecord($model, 'create');});
        CourseAllocation::updated(function ($model) {self::addLogRecord($model, 'update');});
        CourseAllocation::deleted(function ($model) {self::addLogRecord($model, 'delete');});
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * @param Model $model
     * @param string $transactionType
     * @throws Exception
     */
    public function addLogRecord($model, $transactionType)
    {
        /** @var CamraUser $user */
        $user = Auth::user();

        $log = new Log;
        $log->entity_primary_key = $model->getKey();
        $log->entity_type = get_class($model);
        $log->change_description = $this->getChangeDescription($model, $transactionType);
        $log->changed_in = $user ? "webui" : "provider_file_import";
        $log->changed_by_member_id = $user ? $user->id : 0;
        $log->changed_by_member_name = $user ? $user->forename . " " . $user->surname : null;
        $log->save();
    }

    /**
     * @param Model $model
     * @param string $transactionType
     * @return string
     * @throws Exception
     */
    private function getChangeDescription($model, $transactionType)
    {
        $description = "$transactionType";
        switch ($transactionType) {
            case 'create':
            case 'restore':
            case 'delete':
            $values = [];
                $attributes = $model->getAttributes();
                ksort($attributes);
                foreach ($attributes as $key => $value) {
                    $values[] = "$key => $value";
                }
                $description .= ": " . implode(", ", $values);
                break;
            case 'update':
                $changes = [];
                $dirty = $model->getDirty();
                ksort($dirty);
                foreach ($dirty as $key => $value) {
                    $original = $model->getOriginal($key);
                    $changes[] = "$key: $original => $value";
                }
                $description .= ": " . implode(", ", $changes);
                break;
            default:
                throw new Exception("Unexpected transaction type");
        }
        return $description;
    }
}
