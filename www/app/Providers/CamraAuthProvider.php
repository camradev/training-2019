<?php
/**
 * Created by PhpStorm.
 * User: danie
 * Date: 2015-11-20
 * Time: 13:40
 */

namespace App\Providers;


use Illuminate\Support\ServiceProvider;

class CamraAuthProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['auth']->extend('camra',function()
        {
            return new CamraUserProvider();
        });
    }



    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // TODO: Implement register() method.
    }
}