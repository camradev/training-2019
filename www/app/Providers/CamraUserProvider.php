<?php
namespace App\Providers;


use App\Admin;
use App\CamraUser;
use App\Festival;
use Cache;
use DB;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;

class CamraUserProvider implements UserProvider
{

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed $identifier
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveById($identifier)
    {
        debug("retrieveById");
        return self::getUserDetails($identifier);
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed $identifier
     * @param  string $token
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByToken($identifier, $token)
    {
        debug("retrieveByToken");
        // TODO: Implement retrieveByToken() method.
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  string $token
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        debug("updateRememberToken");
        // TODO: Implement updateRememberToken() method.
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        debug("retrieveByCredentials");
        return self::getUserDetails($credentials["username"]);
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  array $credentials
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        debug("validateCredentials");
        if (!$user || empty($user->getAuthPassword())) return false;
        $hash =  hash('sha256', env('MEMBERSDB_SALT').$credentials['password']);
        $dbHash = $user->getAuthPassword();
        return $hash === $dbHash;
    }

    public static function getUserDetails($membershipNumber)
    {
        // Pad the id out to 6 digits
        $membershipNumber = str_pad($membershipNumber, 6, "0", STR_PAD_LEFT );

        $cacheKey = 'member_details_'.$membershipNumber;
        if(Cache::has($cacheKey)){
            return Cache::get($cacheKey);
        }

        $user = DB::connection("members")
            ->table("members")
            ->select("MemberKey", "forename", "surname", "emailmain as email", "membershipnumber", "password", "branch", "branchdesc")
            ->where("membershipnumber", $membershipNumber)
            ->first();

        if ($user) {
            $camraUser = new CamraUser((array)$user);

            // Set the user id to be the membershipnumber and return the object
            // Use the membershipNumber that was padded
            $camraUser->id = $membershipNumber;

            // Check if user is an admin table
            /** @var Admin $status */
            $adminDetails = Admin::whereMemberId($camraUser->membershipnumber)->first();
            if ($adminDetails) {
                $camraUser->hasAdminRead = true;
                // Were found. If either is set, then a further branch/admin check is redundant.
                switch ($adminDetails->can_edit) {
                    case 1:
                        // Member is a SuperUser
                        $camraUser->hasAdminWrite = true;
                        break;
                    case 0:
                        // Member is a SuperViewer
                        break;
                }
            } else {
                // If not found, they aren't a superViewer/superUser, but might be a branch/region admin or festival organiser

                ////////////////////////
                // Festival Organiser //
                ////////////////////////

                // Check if the user is a festival organiser, retrieve their associated festivals:
                $camraUser->festivalOrganiserFestivalList = self::getFestivalOrganiserFestivalList($camraUser->membershipnumber);
#dd($camraUser->festivalOrganiserFestivalList);
                //////////////////
                // Branch Admin //
                //////////////////

                // Check if the user is a branch admin, retrieve the branch list:
                $camraUser->branchAdminBranchList = self::getBranchAdminBranchList($camraUser->MemberKey);

                //////////////////
                // Region Admin //
                //////////////////

                // Then check if the user is a region admin; for every region, get those branches.
                $regionAdminBranchListWithRegionCodes = self::getRegionAdminBranchList($camraUser->MemberKey);

                // Break it down into regions, and branches.

                $regionAdminBranches = [];
                $regions = [];
                foreach ($regionAdminBranchListWithRegionCodes as $key => $row) {
                    $regionAdminBranches[$row->branch_code] = $row->branch_name;

                    // Also grab the regions
                    if (!in_array($row->region_code, $regions)) {
                        $regions[$row->region_code] = $row->region_name;
                    }
                }

                // For the region admin, store the list of regions
                $camraUser->adminRegionList = $regions;
                $camraUser->regionAdminBranchList = $regionAdminBranches;
            }

            Cache::put($cacheKey, $camraUser, 1);

            return $camraUser;
        }
        return null;
    }

    public static function getFestivalOrganiserFestivalList($memberId)
    {
        $results = Festival::where('organiser_member_id',$memberId)
            ->select(['code','name'])
            ->orderBy('name')
            ->get();
        $festivals = [];
        #dd($results);
        foreach ($results as $key => $festival) {
            $festivals[$festival->code] = $festival->name;
        }
        return $festivals;
    }

    public static function getBranchAdminBranchList($memberKey)
    {
        $results = DB::connection('members')
            ->select("SELECT CommLocation as branch_code, CommLocationDesc as branch_name
                      FROM vMem_committees WHERE CommCommittee = 'BRN' AND CommPositionCode in ('CHR', 'BRS', 'BTC', 'FVO') AND (CommEnd is null or CommEnd >= sysdate()) AND memkey = ?", [$memberKey]);
        $branches = [];
        foreach ($results as $key => $branch) {
            $branches[$branch->branch_code] = $branch->branch_name;
        }
        return $branches;
    }

    public static function getRegionAdminBranchList($memberKey)
    {
        $results = DB::connection('members')
            ->select("SELECT distinct b.Branch as branch_code, b.BranchDesc as branch_name, b.BranchRegion as region_code, r.regionname as region_name
                      FROM vMem_committees as v inner join vBranches as b on v.CommLocation=b.BranchRegion inner join regions as r on b.BranchRegion = r.regioncode
                      WHERE v.CommCommittee = 'REG' AND v.CommPositionCode = 'DIR' AND (v.CommEnd is null or v.CommEnd >= sysdate()) and v.memkey = ?", [$memberKey]);

        return $results;
    }

}