<?php

namespace App;

use Eloquent;

/**
 * App\Budget
 *
 * @property integer $id
 * @property string $branch_code
 * @property float $amount
 * @property integer $year
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Budget extends Eloquent
{
    protected $visible = ['id', 'amount', 'year'];

    protected $casts = ["amount" => "double"];

}
