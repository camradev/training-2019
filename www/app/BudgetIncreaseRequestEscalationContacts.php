<?php

namespace App;

use Eloquent;

/**
 * App\BudgetIncreaseRequestEscalationContacts
 *
 * @property integer $id
 * @property string $member_id
 * @property string $member_name
 * @property string $notes
 */
class BudgetIncreaseRequestEscalationContacts extends Eloquent
{
    public $visible = ['member_id', 'member_name', 'notes'];
}
