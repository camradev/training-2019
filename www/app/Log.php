<?php

namespace App;

use Eloquent;

/**
 * App\Log
 *
 * @property integer $id
 * @property string $entity_primary_key
 * @property string $entity_type
 * @property string $change_description
 * @property string $changed_in
 * @property string $changed_by_member_id
 * @property string $changed_by_member_name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Log extends Eloquent
{
}
