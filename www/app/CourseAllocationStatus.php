<?php

namespace App;

use Eloquent;

/**
 * App\CourseAllocationStatus
 *
 * @property integer $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class CourseAllocationStatus extends Eloquent
{
    const STATUS_ALLOCATED = 1;
    const STATUS_STARTED = 2;
    const STATUS_PASSED = 3;
    const STATUS_FAILED = 4;
    //
}
