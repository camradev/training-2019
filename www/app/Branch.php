<?php

namespace App;

use Carbon\Carbon;
use Eloquent;

/**
 * App\Branch
 *
 * @property integer $id
 * @property string $Code
 * @property string $Description
 * @property string $Region
 * @property-read mixed $code
 * @property-read mixed $name
 * @property-read mixed $region
 * @property-read \Illuminate\Database\Eloquent\Collection|Budget[] $budgets
 * @property-read Budget $budget
 * @property-read mixed $current_budget_used
 * @method static \Illuminate\Database\Query\Builder|\App\Branch whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Branch whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Branch whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Branch whereRegion($value)
 */
class Branch extends Eloquent
{
    protected $connection = "members";
    protected $table = "branches";

    protected $visible = ['code', 'name', 'region', 'budgets', 'budget', 'current_budget_used', 'regionInfo'];
    protected $appends = ['code', 'name', 'region', 'current_budget_used'];

    /**
     * Rename Code to code
     * @return string
     */
    public function getCodeAttribute() {
        return $this->attributes['Code'];
    }

    /**
     * Rename Description to name
     * @return string
     */
    public function getNameAttribute() {
        return $this->attributes['Description']. ' ('. $this->attributes['Code'].')';
    }
    /**
     * Rename Region to region
     * @return string
     */
    public function getRegionAttribute() {
        return $this->attributes['Region'];
    }

    public function budgets() {
        return $this->hasMany(Budget::class, "branch_code", "Code");
    }

    public function budget() {
        return $this->hasOne(Budget::class, "branch_code", "Code")->where("year", Carbon::now()->year);
    }

    public function regionInfo(){
        return $this->hasOne(Region::class, "regioncode", "Region");
    }
    /**
     * @return float
     */
    public function getCurrentBudgetUsedAttribute() {
        return CourseAllocation::getBranchUsageForYear(Carbon::now()->year, $this->code);
    }

}
