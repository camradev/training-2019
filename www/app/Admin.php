<?php

namespace App;

use Eloquent;

/**
 * App\Admin
 *
 * @property integer $id
 * @property string $member_id
 * @property integer $can_edit
 * @property string $notes
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Admin extends Eloquent
{
    protected $visible = ['member_id', 'can_edit'];

    protected $fillable = ['member_id', 'can_edit'];
}
