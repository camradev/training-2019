<?php

namespace App;

use Eloquent;

/**
 * App\Region
 *
 * @property integer $id
 * @property string $regioncode
 * @property string $regionname
 * @property-read mixed $code
 * @property-read mixed $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Branch[] $branches
 */
class Region extends Eloquent
{
    protected $connection = "members";
    protected $table = "regions";
    protected $visible = ['code', 'name', 'branches'];
    protected $appends = ['code', 'name'];

    /**
     * Rename regioncode to code
     * @return string
     */
    public function getCodeAttribute() {
        return $this->attributes['regioncode'];
    }


    /**
     * Rename regionname to name
     * @return string
     */
    public function getNameAttribute() {
        return $this->attributes['regionname'].' ('.$this->attributes['regioncode'].')';
    }


    public function branches() {
        return $this->hasMany(Branch::class, "Region", "regioncode");
    }
}
