<?php

namespace App;

use Eloquent;

/**
 * App\Course
 *
 * @property integer $id
 * @property string $name
 * @property float $cost_pounds
 * @property integer $course_provider_id
 * @property string $provider_course_ref
 * @property string $email_notes
 * @property boolean $active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\CourseProvider $provider_email_notes
 */
class Course extends Eloquent
{
    public function provider_email_notes()
    {
        return $this->hasOne(CourseProvider::class, 'id', 'course_provider_id')->select(['email_notes']);
    }

}
