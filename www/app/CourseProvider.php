<?php

namespace App;

use Eloquent;

/**
 * App\CourseProvider
 *
 * @property integer $id
 * @property string $name
 * @property string $email_notes
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Course[] $courses
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Course[] $active_courses
 */
class CourseProvider extends Eloquent
{
    public function courses()
    {
        return $this->hasMany(Course::class);
    }

    public function active_courses()
    {
        return $this->hasMany(Course::class)->where('active',true);
    }
}
