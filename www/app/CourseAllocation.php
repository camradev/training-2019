<?php

namespace App;

use Auth;
use Carbon\Carbon;
use DB;
use Eloquent;
use Illuminate\Database\Query\Builder;

/**
 * App\CourseAllocation
 *
 * @property integer $id
 * @property integer $member_id
 * @property string $member_name
 * @property string $budget_source_type
 * @property string $branch_id
 * @property string $branch_name
 * @property integer $course_id
 * @property integer $status_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Course $course
 * @property-read \App\CourseAllocationStatus $status
 */
class CourseAllocation extends Eloquent
{
    // All fields we want to be materialised
    protected $visible = ["id", "course_id", "member_id", "member_name", "budget_source", "status_id", "created_at"];

    protected $fillable = ["member_id", "member_name", "branch_id", "branch_name", "budget_source", "course_id", "status_id", "started_at", "ended_at"];
    public static function getUsageForYear($year)
    {
        /** @var CourseAllocation[] $allocations */
        $allocations = CourseAllocation::with("course")->whereBetween("created_at", array($year."-01-01 00:00:00", $year."-12-31 23:59:59"))->get();
        $sum = 0;
        foreach ($allocations as $allocation) {
            $sum += $allocation->course->cost_pounds;
        }
        return $sum;
    }

    public static function getBranchUsageForYear($year, $branchId)
    {
        /** @var CourseAllocation[] $allocations */
        /** @var Builder $query */
        $query = CourseAllocation::with("course")->whereBetween("created_at", array($year."-01-01 00:00:00", $year."-12-31 23:59:59"));
        if (!empty($branchId)) {
            $query->where("branch_id", $branchId);
        }
        $allocations = $query->get();
        $sum = 0;
        foreach ($allocations as $allocation) {
            $sum += $allocation->course->cost_pounds;
        }
        return $sum;
    }

    // Used on the dashboard to get usage relative to the admin: Branch, Region or Super. Used in top right of page as part of budget remaining.
    public static function getSimplifiedAllocationTotalForYear($year)
    {
        $query = DB::table("course_allocations")
            ->join("courses", "courses.id", "=", "course_allocations.course_id")
            ->selectRaw("sum(courses.cost_pounds) as cost")
            ->whereBetween("course_allocations.created_at", array($year."-01-01", $year."-12-31 23:59:59"));

        /** @var CamraUser $user */
        $user = Auth::user();

        #Apply perms to list. If user isn't a SuperViewer/SuperUser, they must be a branch and/or region admin and/or festival organiser
        if (!$user->isSuperViewer()) {
            # Grouped subquery, handles if admin happens to be a regional/branch or festival admin.
            $query->where(function ($subQuery) use ($user) {
                $subQuery->whereIn('course_allocations.branch_id', array_merge($user->getFestivalOrganiserFestivalIdList(),$user->getAdminCombinedBranchIdList()));
            });
        }
        return $query->get();
    }

    /**
     * Returns allocations with provider info
     *
     * @param $year
     * @return array|static[]
     */
    public static function getAllocationSummaryForYear($year)
    {
        $query = DB::table("course_allocations")
            ->join("courses", "courses.id", "=", "course_allocations.course_id")
            ->join("course_providers", "course_providers.id", "=", "courses.course_provider_id")
            ->selectRaw("course_providers.name as provider_name, courses.name as course_name, count(*) as course_count, sum(courses.cost_pounds) as cost")
            ->whereBetween("course_allocations.created_at", array($year."-01-01", $year."-12-31 23:59:59"))
            ->groupBy("course_allocations.course_id")
            ->orderBy("provider_name")
            ->orderBy("course_name");

        /** @var CamraUser $user */
        $user = Auth::user();

        #Apply perms to list. If user isn't a SuperViewer/SuperUser, they must be a branch and/or region admin and/or festival organiser
        if (!$user->isSuperViewer()) {
            # Grouped subquery, handles if admin happens to be a regional/branch or festival admin.
            $query->where(function ($subQuery) use ($user) {
                $subQuery->whereIn('course_allocations.branch_id', array_merge($user->getFestivalOrganiserFestivalIdList(),$user->getAdminCombinedBranchIdList()));
            });
        }

        return $query->get();
    }

    /**
     * Returns allocations, broken out by provider
     *
     * @param $startDate
     * @param $endDate
     * @param null $statusName
     * @return array|static[]
     */
    public static function getAllAllocationReport($startDate, $endDate, $statusName=null)
    {
        $query = DB::table("course_allocations")
            ->join("courses", "courses.id", "=", "course_allocations.course_id")
            ->join("course_allocation_statuses", "course_allocation_statuses.id", "=", "course_allocations.status_id")
            ->join("course_providers", "course_providers.id", "=", "courses.course_provider_id")
            ->selectRaw(
                "course_allocations.id, " .
                "course_providers.name as course_provider_name, " .
                "course_allocations.member_id, " .
                "course_allocations.member_name, " .
                "course_allocations.branch_name, " .
                "courses.name as course_name, " .
                "courses.cost_pounds as course_cost_pounds, " .
                "course_allocation_statuses.id as status_id, " .
                "course_allocation_statuses.name as status_name, " .
                "course_allocations.created_at"
            )
            ->whereBetween("course_allocations.created_at", array($startDate, $endDate .' 23:59:59'))
            ->orderBy("course_providers.name")
            ->orderBy("course_allocations.created_at", "asc");

        if ($statusName){
            switch($statusName){
                case 'finished':
                    //dd(1);
                    $query->whereIn("course_allocation_statuses.name", ["Passed", "Failed"]);

                    break;
                default:
                    $query->where("course_allocation_statuses.name", "=", $statusName);
            }
        }

        $query = $query->get();

        // Restructure based on provider
        $results = [];
        foreach ($query as $key => $row) {
            // Re-group a couple of statuses, as per the preferred final Finance report output
            if (in_array($row->status_name, ['Passed', 'Failed'])) {
                $row->status_name = 'Finished';
            }
            $results[$row->course_provider_name]['allocations'][] = $row;
        }

        // Sum costs per provider
        foreach ($results as $course_provider_name => $allocations) {
            $sum = 0;
            foreach ($allocations['allocations'] as $row){
                $sum += $row->course_cost_pounds;
            }
            $results[$course_provider_name]['sum_course_costs_pounds'] = $sum;
        }

        return $results;
    }

    /**
     * Returns allocations with provider info, for CSV export
     *
     * @param $startDate
     * @param $endDate
     * @param null $statusName
     * @return array|static[]
     */
    public static function getAllAllocationReportForCsvExport($startDate, $endDate, $statusName=null)
    {
        $query = DB::table("course_allocations")
            ->join("courses", "courses.id", "=", "course_allocations.course_id")
            ->join("course_allocation_statuses", "course_allocation_statuses.id", "=", "course_allocations.status_id")
            ->join("course_providers", "course_providers.id", "=", "courses.course_provider_id")
            ->selectRaw(
                #"course_allocations.id, " .
                "course_providers.name as `Provider`, " .
                "course_allocations.member_id as `Member ID`, " .
                "course_allocations.member_name as `Member Name`, " .
                "course_allocations.branch_name as `Branch Name`, " .
                "courses.name as `Course Name`, " .
                "courses.cost_pounds as `Course Cost (GBP)`, " .
                #"course_allocation_statuses.id as status_id, " .
                "case when course_allocation_statuses.name in ('failed','passed') then 'finished' else course_allocation_statuses.name end as `Status`, " .
                "course_allocations.created_at as `Created`"
            )
            ->whereBetween("course_allocations.created_at", array($startDate, $endDate .' 23:59:59'))
            ->orderBy("course_providers.name")
            ->orderBy("course_allocations.created_at", "asc");

        if ($statusName){
            switch($statusName){
                case 'finished':
                    $query->whereIn("course_allocation_statuses.name", ["Passed", "Failed"]);

                    break;
                default:
                    $query->where("course_allocation_statuses.name", "=", $statusName);
            }
        }

        return $query->get();
    }

    /**
     * Returns summarised allocations per member, broken out by branch
     *
     * @param $startDate
     * @param $endDate
     * @param null $statusName
     * @return array|static[]
     */
    public static function getBranchAllocationReport($startDate, $endDate, $statusName=null)
    {
        // Get the list of regions with branches for the current year
        $regionBranches = Region::with([
            // include the branches
            "branches" => function ($query) {
                // order the branches by their 'name' i.e. Description
                /** @var Builder $query */
                $query->orderBy("branches.Description");
            }
        ])
            // order the regions by their name
            ->orderBy("regions.regionname")
            ->get()->toArray();

        $query = DB::table("course_allocations")
            ->join("courses", "courses.id", "=", "course_allocations.course_id")
            ->join("course_allocation_statuses", "course_allocation_statuses.id", "=", "course_allocations.status_id")
            ->join("course_providers", "course_providers.id", "=", "courses.course_provider_id")
            ->selectRaw(
                "course_allocations.branch_name, " .
                "course_allocations.branch_id, " .
                "course_providers.name as course_provider_name, " .
                "course_allocations.member_id, " .
                "course_allocations.member_name, " .
                "sum(courses.cost_pounds) as sum_course_cost_pounds, " .
                "course_allocation_statuses.id as status_id, " .
                "course_allocation_statuses.name as status_name "
            )
            ->groupBy("course_allocations.branch_name")
            ->groupBy("course_allocations.branch_id")
            ->groupBy("course_providers.name")
            ->groupBy("course_allocations.member_id")
            ->groupBy("course_allocations.member_name")
            ->groupBy("course_allocation_statuses.id")
            ->groupBy("course_allocation_statuses.name")
            ->whereBetween("course_allocations.created_at", array($startDate, $endDate .' 23:59:59'))
            ->orderBy("course_allocations.branch_name")
            ->orderBy("course_providers.name")
            ->orderBy("course_allocation_statuses.id");

        if ($statusName){
            switch($statusName){
                case 'finished':
                    $query->whereIn("course_allocation_statuses.name", ["Passed", "Failed"]);

                    break;
                default:
                    $query->where("course_allocation_statuses.name", "=", $statusName);
            }
        }

        $query = $query->get();

        // Restructure based on branch
        $results = [];
        foreach ($query as $key => $row) {
            // Re-group a couple of statuses, as per the preferred final Finance report output
            if (in_array($row->status_name, ['Passed', 'Failed'])) {
                $row->status_name = 'Finished';
            }
            $results[$row->branch_id]['allocations'][] = $row;
        }

        // Sum costs per branch
        foreach ($results as $branch_code => $allocations) {
            $sum = 0;
            foreach ($allocations['allocations'] as $row){
                $sum += $row->sum_course_cost_pounds;
            }
            $results[$branch_code]['sum_course_costs_pounds'] = $sum;
        }

        // Destination structure that will have region codes and branch codes as indexes.
        foreach($regionBranches as $regionIndex => $region){
            // Create new element based on region code.
            $regionBranches[$region['code']] = $region;

            $regionBranches[$region['code']]['regionTotal'] = 0;

            // For that new element, loop through all branches
            foreach($regionBranches[$region['code']]['branches'] as $index => $branch){
                //Populate each branch with our other results
                if(isset($results[$branch['code']])){
                    $branch['allocations'] = $results[$branch['code']]['allocations'];
                    // Update the regional course costs tally
                    $regionBranches[$region['code']]['regionTotal'] += $results[$branch['code']]['sum_course_costs_pounds'];

                    // Copy the branch to a new index position based on it's branch code
                    $regionBranches[$region['code']]['branches'][$branch['code']] = $branch;
                }

                // Remove the old integer-indexed branch element
                unset($regionBranches[$region['code']]['branches'][$index]);
            }

            # If the region had no allocations, remove it
            if ($regionBranches[$region['code']]['regionTotal'] == 0) {
                unset($regionBranches[$region['code']]);
            };
            // Remove the old integer-indexed region element
            unset($regionBranches[$regionIndex]);
        }

        return $regionBranches;
    }

    /**
     * Returns allocations with provider info, for CSV export
     *
     * @param $startDate
     * @param $endDate
     * @param null $statusName
     * @return array|static[]
     */
    public static function getBranchAllocationReportForCsvExport($startDate, $endDate, $statusName=null)
    {
        $query = DB::table("course_allocations")
            ->join("courses", "courses.id", "=", "course_allocations.course_id")
            ->join("course_allocation_statuses", "course_allocation_statuses.id", "=", "course_allocations.status_id")
            ->join("course_providers", "course_providers.id", "=", "courses.course_provider_id")
            ->selectRaw(
                "course_allocations.branch_name as `Branch Name`, " .
                "course_allocations.branch_id as `Branch Code`, " .
                "course_providers.name as `Provider`, " .
                "course_allocations.member_id as `Member ID`, " .
                "course_allocations.member_name as `Member Name`, " .
                "sum(courses.cost_pounds) as `Course Cost`, " .
                "case when course_allocation_statuses.name in ('failed','passed') then 'finished' else course_allocation_statuses.name end as `Status` "
            )
            ->groupBy("course_allocations.branch_name")
            ->groupBy("course_allocations.branch_id")
            ->groupBy("course_providers.name")
            ->groupBy("course_allocations.member_id")
            ->groupBy("course_allocations.member_name")
            ->groupBy("course_allocation_statuses.name")
            ->whereBetween("course_allocations.created_at", array($startDate, $endDate .' 23:59:59'))
            ->orderBy("course_allocations.branch_name")
            ->orderBy("course_providers.name")
            ->orderBy("course_allocation_statuses.id");

        if ($statusName){
            switch($statusName){
                case 'finished':
                    $query->whereIn("course_allocation_statuses.name", ["Passed", "Failed"]);

                    break;
                default:
                    $query->where("course_allocation_statuses.name", "=", $statusName);
            }
        }

        $results = $query->get();
        foreach($results as $key=>$result){
            $result->{'Region Name'} = getRegionNameForBranchCode($result->{"Branch Code"});
        }

        return $results;
    }

    /**
     * Returns regional summary with budget and course count stats
     *
     * @param $startDate
     * @param $endDate
     * @return array|static[]
     */
    public static function getRegionSummaryReport($startDate, $endDate)
    {
        // The start date and end date are in the same year for this report, so get either year to use for budgets join
        $year = Carbon::createFromFormat('Y-m-d', $startDate)->year;

        // Get the list of regions with branches
        $regionBranches = Region::with([
            // include the branches
            "branches" => function ($query) {
                // order the branches by their 'name' i.e. Description
                /** @var Builder $query */
                $query->orderBy("branches.Description");
            }
        ])
            // order the regions by their name
            ->orderBy("regions.regionname")
            ->get()->toArray();

//        $query = DB::table("budgets")
//            ->leftJoin("course_allocations", function($join) use ($year)
//            {
//                $join->on("course_allocations.branch_id", "=", "budgets.branch_code")
//                    ->where("course_allocations.created_at", '>=', "$year-01-01")
//                    ->where("course_allocations.created_at", '<=', "$year-12-31 23:59:59");
//            })

        $query = DB::table("course_allocations")
            ->leftJoin("budgets", function($join) use ($year) {
                $join->on('budgets.branch_code', '=', 'course_allocations.branch_id')
                    ->where('budgets.year', '=', $year);
            })
            ->join("courses", "courses.id", "=", "course_allocations.course_id")
            ->leftJoin("courses as coursesNotStarted", function($join) {
                $join->on('coursesNotStarted.id', '=', 'course_allocations.course_id')
                    ->where('course_allocations.status_id', '=', 1);
            })
            ->leftJoin("courses as coursesStarted", function($join) {
                $join->on('coursesStarted.id', '=', 'course_allocations.course_id')
                    ->where('course_allocations.status_id', '=', 2);
            })
            ->leftJoin("courses as coursesCompleted", function($join) {
                $join->on('coursesCompleted.id', '=', 'course_allocations.course_id')
                    ->whereIn('course_allocations.status_id', [3,4]);
            })
            ->selectRaw(
                "course_allocations.branch_name, " .
                "course_allocations.branch_id, " .
                "budgets.amount as budget_pounds, " .
                "sum(courses.cost_pounds) as total_allocation_pounds, " .
                "sum(coursesNotStarted.cost_pounds) as coursesNotStartedSpent, " .
                "sum(coursesStarted.cost_pounds) as coursesStartedSpent, " .
                "sum(coursesCompleted.cost_pounds) as coursesCompletedSpent, " .
                "count(coursesNotStarted.id) as courses_not_started_count, " .
                "count(coursesStarted.id) as courses_started_count, " .
                "count(coursesCompleted.id) as courses_completed_count "
            )
            ->groupBy("course_allocations.branch_name")
            ->groupBy("course_allocations.branch_id")
            ->groupBy("budgets.amount")
            ->whereBetween("course_allocations.created_at", array($startDate, $endDate .' 23:59:59'))
            ->orderBy("course_allocations.branch_name");

        #dump($query->toSql());
        #dd($query->getBindings());
        $query = $query->get();

        // Restructure based on branch
        $results = [];
        foreach ($query as $key => $row) {
            $results[$row->branch_id]['allocations'] = $row;
        }

        #dd($results);

        // Sum costs per branch
        #foreach ($results as $branch_code => $allocations) {
        #    $sum = 0;
        #    foreach ($allocations['allocations'] as $row){
        #        $sum += $row->sum_course_cost_pounds;
        #    }
        #    $results[$branch_code]['sum_course_costs_pounds'] = $sum;
        #}

        // Destination structure that will have region codes and branch codes as indexes.
        foreach($regionBranches as $regionIndex => $region){
            // Create new element based on region code.
            $regionBranches[$region['code']] = $region;

            $regionBranches[$region['code']]['regionTotalBudgetPounds'] = 0;
            $regionBranches[$region['code']]['regionTotalAllocationPounds'] = 0;
            $regionBranches[$region['code']]['regionTotalSpentPounds'] = 0;
            $regionBranches[$region['code']]['regionTotalCoursesNotStartedCount'] = 0;
            $regionBranches[$region['code']]['regionTotalCoursesStartedCount'] = 0;
            $regionBranches[$region['code']]['regionTotalCoursesCompletedCount'] = 0;

            // For that new element, loop through all branches
            foreach($regionBranches[$region['code']]['branches'] as $index => $branch){
                //Populate each branch with our other results
                if(isset($results[$branch['code']])){
                    #dd((int)$results[$branch['code']]['allocations'][0]->total_spent_pounds);
                    #$branch['allocations'] = $results[$branch['code']]['allocations'];
                    $branch["budget_pounds"] = $results[$branch['code']]['allocations']->budget_pounds ? (int)$results[$branch['code']]['allocations']->budget_pounds : 0;
                    $branch["total_allocation_pounds"] = $results[$branch['code']]['allocations']->total_allocation_pounds ? (int)$results[$branch['code']]['allocations']->total_allocation_pounds : 0;

                    #$coursesNotStartedSpent = (int)$results[$branch['code']]['allocations']->coursesNotStartedSpent ?: 0;
                    $coursesStartedSpent = (int)($results[$branch['code']]['allocations']->coursesStartedSpent ?: 0);
                    $coursesCompletedSpent = (int)($results[$branch['code']]['allocations']->coursesCompletedSpent ?: 0);
                    $branch["total_spent_pounds"] = $coursesStartedSpent + $coursesCompletedSpent;

                    $branch["courses_not_started_count"] = $results[$branch['code']]['allocations']->courses_not_started_count ? (int)$results[$branch['code']]['allocations']->courses_not_started_count : 0;
                    $branch["courses_started_count"] = $results[$branch['code']]['allocations']->courses_started_count ? (int)$results[$branch['code']]['allocations']->courses_started_count : 0;
                    $branch["courses_completed_count"] = $results[$branch['code']]['allocations']->courses_completed_count ? (int)$results[$branch['code']]['allocations']->courses_completed_count : 0;
                    // Update the regional course costs tally
                    $regionBranches[$region['code']]['regionTotalBudgetPounds'] += $results[$branch['code']]['allocations']->budget_pounds;
                    $regionBranches[$region['code']]['regionTotalAllocationPounds'] += $results[$branch['code']]['allocations']->total_allocation_pounds;
                    $regionBranches[$region['code']]['regionTotalSpentPounds'] += $branch["total_spent_pounds"];
                    $regionBranches[$region['code']]['regionTotalCoursesNotStartedCount'] += $results[$branch['code']]['allocations']->courses_not_started_count;
                    $regionBranches[$region['code']]['regionTotalCoursesStartedCount'] += $results[$branch['code']]['allocations']->courses_started_count;
                    $regionBranches[$region['code']]['regionTotalCoursesCompletedCount'] += $results[$branch['code']]['allocations']->courses_completed_count;

                    // Remove current_budget_used as it relates to yearly budget assigned to course allocations, but report is for variable date ranges so removing for clarity
                    unset($branch['current_budget_used']);

                    // Copy the branch to a new index position based on it's branch code
                    $regionBranches[$region['code']]['branches'][$branch['code']] = $branch;

                }

                // Remove the old integer-indexed branch element
                unset($regionBranches[$region['code']]['branches'][$index]);
            }

            # If the region had no allocations, remove it
//            if ($regionBranches[$region['code']]['regionTotalSpentPounds'] == 0) {
//                unset($regionBranches[$region['code']]);
//            };

            // Remove the old integer-indexed region element
            unset($regionBranches[$regionIndex]);
        }
#dd($regionBranches);
        return $regionBranches;
    }

    /**
     * Returns regional summary with budget and course count stats
     *
     * @param $startDate
     * @param $endDate
     * @return array|static[]
     */
    public static function getRegionSummaryReportForCsvExport($startDate, $endDate)
    {
        // The start date and end date are in the same year for this report, so get either year to use for budgets join
        $year = Carbon::createFromFormat('Y-m-d', $startDate)->year;

        $query = DB::table("course_allocations")
            ->join("budgets", function($join) use ($year) {
                $join->on('budgets.branch_code', '=', 'course_allocations.branch_id')
                    ->where('budgets.year', '=', $year);
            })
            ->join("courses", "courses.id", "=", "course_allocations.course_id")
            ->leftJoin("courses as coursesNotStarted", function($join) {
                $join->on('coursesNotStarted.id', '=', 'course_allocations.course_id')
                    ->where('course_allocations.status_id', '=', 1);
            })
            ->leftJoin("courses as coursesStarted", function($join) {
                $join->on('coursesStarted.id', '=', 'course_allocations.course_id')
                    ->where('course_allocations.status_id', '=', 2);
            })
            ->leftJoin("courses as coursesCompleted", function($join) {
                $join->on('coursesCompleted.id', '=', 'course_allocations.course_id')
                    ->whereIn('course_allocations.status_id', [3,4]);
            })
            ->selectRaw(
                "course_allocations.branch_name as `Branch Name`, " .
                "course_allocations.branch_id as `Branch Code`, " .
                "budgets.amount as `Budget`, " .
                "sum(courses.cost_pounds) as `Total Course Allocations Cost`, " .
                "sum(coursesNotStarted.cost_pounds) as coursesNotStartedSpent, " .
                "sum(coursesStarted.cost_pounds) as coursesStartedSpent, " .
                "sum(coursesCompleted.cost_pounds) as coursesCompletedSpent, " .
                "count(coursesNotStarted.id) as `Courses Not Started`, " .
                "count(coursesStarted.id) as `Courses Started`, " .
                "count(coursesCompleted.id) as `Courses Completed` "
            )
            ->groupBy("course_allocations.branch_name")
            ->groupBy("course_allocations.branch_id")
            ->groupBy("budgets.amount")
            ->whereBetween("course_allocations.created_at", array($startDate, $endDate .' 23:59:59'))
            ->orderBy("course_allocations.branch_name");

        $results = $query->get();

        foreach($results as $key=>$result){
            #dd($result);
            // Add region name
            $result->{'Region Name'} = getRegionNameForBranchCode($result->{"Branch Code"});

            // Process the spent values
            $coursesStartedSpent = (int)($result->{"coursesStartedSpent"} ?: 0);
            $coursesCompletedSpent = (int)($result->{"coursesCompletedSpent"} ?: 0);
            $result->{'Total Spent'} = $coursesStartedSpent + $coursesCompletedSpent;
        }
#dd($results);
        return $results;
    }

    /**
     * Returns budgets for a given year, with optional allocations; restricted by perms of current user to branch, regional or all budgets.
     *
     * @param $year
     * @return array|static[]
     */
    public static function getAllocationRegionalSummaryForYear($year)
    {
        $query = DB::table("budgets")
            ->leftJoin("course_allocations", function($join) use ($year)
            {
                $join->on("course_allocations.branch_id", "=", "budgets.branch_code")
                    ->where("course_allocations.created_at", '>=', "$year-01-01")
                    ->where("course_allocations.created_at", '<=', "$year-12-31 23:59:59");
            })
            ->leftJoin("courses", "courses.id", "=", "course_allocations.course_id")
            ->selectRaw("budgets.branch_code, budgets.amount as budget_allocated, sum(courses.cost_pounds) as budget_used")
            ->where("budgets.year", "=", $year)
            ->groupBy("budgets.branch_code");

        /** @var CamraUser $user */
        $user = Auth::user();

        #Apply perms to list. If user isn't a SuperViewer/SuperUser, they must be a branch and/or region admin and/or festival organiser
        if (!$user->isSuperViewer()) {
            # Grouped subquery, handles if admin happens to be a regional/branch or festival admin.
            $query->where(function ($subQuery) use ($user) {
                $subQuery->whereIn('budgets.branch_code', array_merge($user->getFestivalOrganiserFestivalIdList(),$user->getAdminCombinedBranchIdList()));
            });
        }
        #dd($query->get());
        #dump($query->toSql());
        #dd($query->getBindings());
        return $query->get();
    }

    /**
     * Returns the amount spent on all allocated courses for a given year.
     *
     * @param $year
     * @return integer
     */
    public static function getNationalCourseAllocationTotalForYear($year)
    {
        $query = DB::table("budgets")
            ->leftJoin("course_allocations", function($join) use ($year)
            {
                $join->on("course_allocations.branch_id", "=", "budgets.branch_code")
                    ->where("course_allocations.created_at", '>=', "$year-01-01")
                    ->where("course_allocations.created_at", '<=', "$year-12-31 23:59:59");
            })
            ->leftJoin("courses", "courses.id", "=", "course_allocations.course_id")
            ->selectRaw("sum(courses.cost_pounds) as spent")
            ->where("budgets.year", "=", $year);

        $result = $query->first()->spent ? $query->first()->spent : 0;
        return $result;
    }

    public static function getBudgetStats($year, $requestType = null, $code = null)
    {
        $query = DB::table("budgets")
            ->leftJoin("course_allocations", function($join) use ($year)
            {
                $join->on("course_allocations.branch_id", "=", "budgets.branch_code")
                    ->where("course_allocations.created_at", '>=', "$year-01-01")
                    ->where("course_allocations.created_at", '<=', "$year-12-31 23:59:59");
            })
            ->leftJoin("courses", "courses.id", "=", "course_allocations.course_id")
            ->selectRaw("budgets.branch_code, budgets.amount as budget_allocated, sum(courses.cost_pounds) as budget_used")
            ->where("budgets.year", "=", $year)
            // TODO 2016-01-12 Ryan - Anything else we add to Budgets table, like festivals, needs excluding here. Need to consider alternative workaround
            // as table isn't just branches anymore.
            ->where("budgets.branch_code", "<>", "NATIONAL")
            ->groupBy("budgets.branch_code");

        /** @var CamraUser $user */
        $user = Auth::user();
        #Apply perms to list. If user isn't a SuperViewer/SuperUser, they must be a branch and/or region admin
        if ($requestType == 'b') {
            $query = $query->where('budgets.branch_code', '=', $code);
        } elseif ($requestType == 'r') {
            $branchesRaw = self::getBranchesWithinRegionCode($code);
            foreach($branchesRaw as $key=>$row)
            {
                $branches[]=($row->branch_code);
            }
            $query = $query->whereIn('budgets.branch_code', $branches);
        }
        #dd($query->get());
        #dd($query->toSql());
        $results = $query->get();
#dd($results[0]->budget_allocated);
        $total_budget_allocated = 0;
        $total_budget_used = 0;

        // Now add in the budget stats.
        foreach ($results as $key => $row) {
            // Branch budget info

            // Update the total budgets allocated
            $total_budget_allocated += ($row->budget_allocated == null ? 0 : (int)$row->budget_allocated);

            // Update the total budget usage
            $total_budget_used += ($row->budget_used == null ? 0 : (int)$row->budget_used);
        }

        return [ 'total' => $total_budget_allocated, 'remaining' => $total_budget_allocated - $total_budget_used];
    }

    private static function getBranchesWithinRegionCode($code)
    {
        $query = DB::connection("members")->table("branches")
            ->selectRaw("code as branch_code")
            ->where("Region", "=", $code);

        return $query->get();
    }


    ///////////////////
    // Relationships //
    ///////////////////
    /**
     * The relationship between CourseAllocation and Course
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function course()
    {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }

    /**
     * The relationship between CourseAllocation and CourseAllocationStatus
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function status()
    {
        return $this->hasOne(CourseAllocationStatus::class, 'id', 'status_id');
    }

    /**
     * A query to bring back allocations with course name and status name for ordering
     * This is a direct DB query and so is not a "CourseAllocation" object
     * All columns returned from here will be available once converted to json
     * @param null $allocationId
     * @return $this
     */
    public static function allocationsForListViewQuery($allocationId = null)
    {
        $query = DB::table("course_allocations")
            ->join("courses", "courses.id", "=", "course_allocations.course_id")
            ->join("course_allocation_statuses", "course_allocation_statuses.id", "=", "course_allocations.status_id")
            ->join("course_providers", "course_providers.id", "=", "courses.course_provider_id")
            ->selectRaw(
                "course_allocations.id, " .
                "course_allocations.member_id, " .
                "course_allocations.member_name, " .
                "course_allocations.branch_name, " .
                "course_allocations.created_at, " .
                #"case when course_allocations.budget_source_type = 'branch' then 'Branch' else 'Festival' end as budget_source_type, " .
                "budget_source, " .
                "courses.name as course_name, " .
                "courses.cost_pounds as course_cost_pounds, " .
                "course_allocation_statuses.id as status_id, " .
                "course_allocation_statuses.name as status_name, " .
                "course_providers.name as course_provider_name"
            );
        if ($allocationId) {
            $query = $query->where('course_allocations.id', $allocationId);
        }

        /** @var CamraUser $user */
        $user = Auth::user();

        #Apply perms to list. If user isn't a SuperViewer/SuperUser, they must be a branch and/or region admin and/or festival organiser
        if (!$user->isSuperViewer()) {
            # Grouped subquery, handles if admin happens to be a regional/branch or festival admin.
            $query->where(function ($subQuery) use ($user) {
                $subQuery->whereIn('course_allocations.branch_id', array_merge($user->getFestivalOrganiserFestivalIdList(),$user->getAdminCombinedBranchIdList()));
            });
        }
        return $query;
    }
}
