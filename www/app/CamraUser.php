<?php

namespace App;

use Illuminate\Auth\GenericUser;
use Illuminate\Foundation\Auth\Access\Authorizable;

//  Branch Secretary = 006671;
//  Regional Director = 111678;
//  SELECT * FROM test_cra_data.members where membershipnumber in ('006671','111678')


/**
 * @property int id
 * @property string membershipnumber
 * @property string forename
 * @property string surname
 * @property string email
 * @property string[] adminRegionList
 * @property string[] branchAdminBranchList
 * @property string[] regionAdminBranchList
 * @property string[] festivalOrganiserFestivalList
 * @property bool hasAdminRead
 * @property bool hasAdminWrite
 * @property mixed fullName
 */
class CamraUser extends GenericUser
{
    use Authorizable;

    /**
     * Create a new generic User object.
     *
     * @param  array $attributes
     */
    public function __construct(array $attributes)
    {
        parent::__construct($attributes);

        if (!isset($this->hasAdminRead)) $this->hasAdminRead = false;
        if (!isset($this->hasAdminWrite)) $this->hasAdminWrite = false;
        if (!isset($this->adminRegionList)) $this->adminRegionList = [];
        if (!isset($this->branchAdminBranchList)) $this->branchAdminBranchList = [];
        if (!isset($this->regionAdminBranchList)) $this->regionAdminBranchList = [];
        if (!isset($this->festivalOrganiserFestivalList)) $this->festivalOrganiserFestivalList = [];
    }

    public function canAccessSystem()
    {
        return $this->isSuperUser() || $this->isSuperViewer() || $this->isRegionAdmin() || $this->isBranchAdmin() || $this->isFestivalOrganiser();
    }

    public function isRegionAdmin()
    {
        return count($this->adminRegionList) > 0;
    }

    public function isBranchAdmin()
    {
        // This is the count of only the branches that the branch admin can manage, used to deduce if they are a branch admin, so should be 1 or 0. If they were a region admin too for some reason, the total set of branches they can admin is higher.
        return count($this->branchAdminBranchList) > 0;
    }

    public function isFestivalOrganiser()
    {
        return count($this->festivalOrganiserFestivalList) > 0;
    }

    public function isSuperUser()
    {
        return $this->hasAdminWrite;
    }

    public function isSuperViewer()
    {
        return $this->hasAdminRead || $this->hasAdminWrite;
    }

    public function canViewBudgets()
    {
        // The user can view budgets if they are a superViewer
        return $this->isSuperViewer();
    }

    public function canChangeBudgets()
    {
        // The user can change budgets if they are a superAdmin
        return $this->isSuperUser();
    }

    public function canViewReports()
    {
        // The user can view budgets if they are a superViewer
        return $this->isSuperViewer();
    }

    public function getFestivalOrganiserFestivalList()
    {
        // Return the festivals user is responsible for
        return $this->festivalOrganiserFestivalList;
    }

    public function getFestivalOrganiserFestivalIdList()
    {
        // Return the festival ids that user is responsible for
        return array_keys($this->festivalOrganiserFestivalList);
    }

    public function getBranchAdminBranchList()
    {
        // Combine the branch and region admin list of branches.
        return $this->branchAdminBranchList;
    }

    public function getAdminCombinedBranchList()
    {
        // Combine the branch and region admin list of branches.
        return array_merge($this->regionAdminBranchList, $this->branchAdminBranchList);
    }

    public function getAdminCombinedBranchIdList()
    {
        // Combine the branch and region admin list of branches.
        return array_keys($this->getAdminCombinedBranchList());
    }

    public function getRegionAdminList()
    {
        return $this->adminRegionList;
    }

    public function getFullName()
    {
        return $this->forename . ' ' . $this->surname;
    }

    public function getAdminRegionNames()
    {
        $names = [];
        foreach($this->adminRegionList as $regionCode => $regionName){
            $names[] = $regionName;
        }
        return count($names) > 0 ? implode(', ', $names) : 'No region';
    }
}