<?php

namespace App\Console\Commands;

use App\BudgetIncreaseRequests;
use Exception;
use Illuminate\Console\Command;
use Mail;
use Swift_RfcComplianceException;

class ProcessBudgetIncreaseRequestEscalations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'escalations:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process the open budget increase requests and escalate any over two weeks old.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get the list of overdue requests.
        $overdueRequests = BudgetIncreaseRequests::getAllOverdueRequestsThatNeedEscalating();

        // Only update escalation field and send emails if at least one found.
        if (!$overdueRequests->isEmpty()) {

            // Get the escalation team contact info
            $escalationContactInfo = BudgetIncreaseRequests::getEscalationContactInfo();

            try {
                // update each request with a timestamp of when escalated.
                foreach ($overdueRequests as $request) {
                    // Add the branch's region code to each request, for the email button to the budget distribution page.
                    $request->region_code = getRegionCodeForBranchCode($request->branch_code);

                    // Set the escalation time on this open request
                    //dd($request);
                    BudgetIncreaseRequests::where('id', '=', $request->id)->update(['escalated_at' => date('Y-m-d H:i:s')]);
                }
            } catch (Exception $ex) {
                $this->info('Failed to mark the overdue budget increase requests due to the following message:' . $ex->getMessage());
            }

            // Prep $data for the email template
            $data['requests'] = $overdueRequests;

            // now email the escalation contacts the above list of overdue requests.
            try {
                // Send allocation confirmation to the email template.
                Mail::send('emails.birEscalationNotification', $data, function ($message) use ($escalationContactInfo) {
                    $message->from('noreply@camra.org.uk', 'NoReply');
                    foreach ($escalationContactInfo as $contactMemberId => $contactDetails) {
                        $message->to($contactDetails->email, $contactDetails->forename . ' ' . $contactDetails->surname);
                    }
                    $message->subject('CAMRA Budget Increase Request (Escalations)');

                });
                $this->info('Escalation email sent.');
            } catch (Swift_RfcComplianceException $ex) {
                $exceptionMsg = "The escalation request was successful, however, could not notify the escalation team due to mailer reporting the following message:" . $ex->getMessage();
                $this->info($exceptionMsg);
            }

        } else {
            $this->info('No overdue requests found.');
        }
    }
}
