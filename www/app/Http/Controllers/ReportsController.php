<?php

namespace App\Http\Controllers;

use App\CourseAllocation;
use App\Http\Requests;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Input;
use Redirect;
use Response;

class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Don't go further if user has no view or edit perms.
        if ($request->user()->cannot('access-system')) {
            Auth::logout();
            // Redirect user back to login page with a flash message
            return Redirect::to('/auth/login')->with('message', 'You don\'t have permissions to access this system.');
        }

        // Display the report links
        return view("reports.index");
    }

    /**
 * Display a listing of the resource.
 *
 * @param Request $request
 * @return \Illuminate\Http\Response
 */
    public function viewAllAllocations(Request $request)
    {
        $statusName = Input::get('statusName');
        $startDate = Input::get('startDate');
        $endDate = Input::get('endDate');

        // Don't go further if user has no view or edit perms.
        if ($request->user()->cannot('access-system')) {
            Auth::logout();
            // Redirect user back to login page with a flash message
            return Redirect::to('/auth/login')->with('message', 'You don\'t have permissions to access this system.');
        }

        // Set date filters if not set from page reload.
        if (!isset($startDate)) {
            $startDate = Carbon::now()->subMonth(1)->startOfMonth()->format("Y-m-d");
        }
        if (!isset($endDate)) {
            $endDate = Carbon::now()->subMonth(1)->endOfMonth()->format("Y-m-d H:i:s");
        }

        // Get the allocations
        $results = CourseAllocation::getAllAllocationReport($startDate, $endDate, $statusName);

        // Prep set of dates to use in the daterangepicker list
        $dates = [];
        for($i=1; $i <=3; $i++){
            $dates[$i] = Carbon::now()->subMonth($i)->format("M Y");
        }

        // Display the report
        return view("reports.allAllocations", compact(['results', 'dates', "statusName", "startDate", "endDate"]));
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function viewAllAllocationsForCsvExport(Request $request)
    {
        $statusName = Input::get('statusName');
        $startDate = Input::get('startDate');
        $endDate = Input::get('endDate');

        // Don't go further if user has no view or edit perms.
        if ($request->user()->cannot('access-system')) {
            Auth::logout();
            // Redirect user back to login page with a flash message
            return Redirect::to('/auth/login')->with('message', 'You don\'t have permissions to access this system.');
        }

        // Set date filters if not set from page reload.
        if (!isset($startDate)) {
            $startDate = Carbon::now()->subMonth(1)->startOfMonth()->format("Y-m-d");
        }
        if (!isset($endDate)) {
            $endDate = Carbon::now()->subMonth(1)->endOfMonth()->format("Y-m-d H:i:s");
        }

        // Get the allocations
        $results = CourseAllocation::getAllAllocationReportForCsvExport($startDate, $endDate, $statusName);
        #dd($results);
        #exit;

        // To debug, set content-type to text/html and comment out content-disposition, then vardump.
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=allAllocations '.$startDate.' - '.$endDate.'.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        # add headers for each column in the CSV download
        #array_unshift($list, array_keys($list[0]));
        $callback = function() use ($results)
        {
            # The results are an array of objects
            # var_dump($results[0]);
            # var_dump((array)$results[0]);

            $FH = fopen('php://output', 'w');

            // output the column headings
            fputcsv($FH, array_keys((array)$results[0]));

            foreach ($results as $key => $row) {
                #dd($row->toArray());
                fputcsv($FH, (array)$row);
            }
            fclose($FH);
        };

        return Response::stream($callback, 200, $headers);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function viewBranchAllocations(Request $request)
    {
        $statusName = Input::get('statusName');
        $startDate = Input::get('startDate');
        $endDate = Input::get('endDate');

        // Don't go further if user has no view or edit perms.
        if ($request->user()->cannot('access-system')) {
            Auth::logout();
            // Redirect user back to login page with a flash message
            return Redirect::to('/auth/login')->with('message', 'You don\'t have permissions to access this system.');
        }

        // Set date filters if not set from page reload.
        if (!isset($startDate)) {
            $startDate = Carbon::now()->subMonth(1)->startOfMonth()->format("Y-m-d");
        }
        if (!isset($endDate)) {
            $endDate = Carbon::now()->subMonth(1)->endOfMonth()->format("Y-m-d");
        }

        // Get the allocations
        $results = CourseAllocation::getBranchAllocationReport($startDate, $endDate, $statusName);

        // Prep set of dates to use in the daterangepicker list
        $dates = [];
        for($i=1; $i <=3; $i++){
            $dates[$i] = Carbon::now()->subMonth($i)->format("M Y");
        }
#dd($results);
        // Display the report
        return view("reports.branchAllocations", compact(['results', 'dates', "statusName", "startDate", "endDate"]));
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function viewBranchAllocationsForCsvExport(Request $request)
    {
        $statusName = Input::get('statusName');
        $startDate = Input::get('startDate');
        $endDate = Input::get('endDate');

        // Don't go further if user has no view or edit perms.
        if ($request->user()->cannot('access-system')) {
            Auth::logout();
            // Redirect user back to login page with a flash message
            return Redirect::to('/auth/login')->with('message', 'You don\'t have permissions to access this system.');
        }

        // Set date filters if not set from page reload.
        if (!isset($startDate)) {
            $startDate = Carbon::now()->subMonth(1)->startOfMonth()->format("Y-m-d");
        }
        if (!isset($endDate)) {
            $endDate = Carbon::now()->subMonth(1)->endOfMonth()->format("Y-m-d");
        }

        // Get the allocations
        $results = CourseAllocation::getBranchAllocationReportForCsvExport($startDate, $endDate, $statusName);

        #exit;

        // To debug, set content-type to text/html and comment out content-disposition, then vardump.
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=branchAllocations '.$startDate.' - '.$endDate.'.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        # add headers for each column in the CSV download
        #array_unshift($list, array_keys($list[0]));
        $callback = function() use ($results)
        {
            # The results are an array of objects
            # var_dump($results[0]);
            # var_dump((array)$results[0]);

            $FH = fopen('php://output', 'w');

            // output the column headings
            fputcsv($FH, [
                'Region Name',
                'Branch Name',
                'Branch Code',
                'Provider',
                'Member ID',
                'Member Name',
                'Course Cost',
                'Status'
            ]);

            foreach ($results as $key => $row) {
                $row = [$row->{'Region Name'},
                    $row->{'Branch Name'},
                    $row->{'Branch Code'},
                    $row->{'Provider'},
                    $row->{'Member ID'},
                    $row->{'Member Name'},
                    $row->{'Course Cost'},
                    $row->{'Status'}
                ];

                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return Response::stream($callback, 200, $headers);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function viewRegionSummary(Request $request)
    {
        $startDate = Input::get('startDate');
        $endDate = Input::get('endDate');

        $rules = [
            'start_date'        => 'date_format:d/m/Y|after:tomorrow',
            'end_date'          => 'date_format:d/m/Y|after:start_date',
        ];

        // Check all the values we can before hitting db
        $this->validate($request,
            [
                'start_date'        => 'date_format:Y-m-d|after:tomorrow',
                'end_date'          => 'date_format:Y-m-d|after:start_date',
            ],
            [
                'year.required' => 'A year value must be provided.',
                'year.numeric' => 'The year value must be numeric.',
                'year.min' => 'The year value cannot be less than the current year.'
            ]
        );

        // Set date filters if not set from page reload.
        if (isset($startDate) && isset($endDate)) {
            $startDateYear = Carbon::createFromFormat('Y-m-d', $startDate)->year;
            $endDateYear = Carbon::createFromFormat('Y-m-d', $endDate)->year;
            if($startDateYear != $endDateYear) {
                return Redirect::to('/reports/region-summary')->with('alert-info', "Attempt to access report with start and end dates that aren't in the same budget year; reloading report with valid defaults.");
            }
        }

        // Don't go further if user has no view or edit perms.
        if ($request->user()->cannot('access-system')) {
            Auth::logout();
            // Redirect user back to login page with a flash message
            return Redirect::to('/auth/login')->with('message', 'You don\'t have permissions to access this system.');
        }

        // Set date filters if not set from page reload.
        if (!isset($startDate)) {
            $startDate = Carbon::now()->startOfMonth()->format("Y-m-d");
        }
        if (!isset($endDate)) {
            $endDate = Carbon::now()->today()->format("Y-m-d");
        }

        // Get the allocations
        $results = CourseAllocation::getRegionSummaryReport($startDate, $endDate);

        // Prep set of dates to use in the daterangepicker list
        $dates = [];
//        for($i=1; $i <=3; $i++){
//            $dates[$i] = Carbon::now()->subMonth($i)->format("M Y");
//        }
#dd($results);
        // Display the report
        return view("reports.regionSummary", compact(['results', 'dates', "startDate", "endDate"]));
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function viewRegionSummaryForCsvExport(Request $request)
    {
        $startDate = Input::get('startDate');
        $endDate = Input::get('endDate');

        // Don't go further if user has no view or edit perms.
        if ($request->user()->cannot('access-system')) {
            Auth::logout();
            // Redirect user back to login page with a flash message
            return Redirect::to('/auth/login')->with('message', 'You don\'t have permissions to access this system.');
        }

        // Set date filters if not set from page reload.
        if (!isset($startDate)) {
            $startDate = Carbon::now()->subMonth(1)->startOfMonth()->format("Y-m-d");
        }
        if (!isset($endDate)) {
            $endDate = Carbon::now()->subMonth(1)->endOfMonth()->format("Y-m-d H:i:s");
        }

        // Get the allocations
        $results = CourseAllocation::getRegionSummaryReportForCsvExport($startDate, $endDate);

        #exit;

        // To debug, set content-type to text/html and comment out content-disposition, then vardump.
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
            ,   'Content-type'        => 'text/csv'
            ,   'Content-Disposition' => 'attachment; filename=regionSummary '.$startDate.' - '.$endDate.'.csv'
            ,   'Expires'             => '0'
            ,   'Pragma'              => 'public'
        ];

        # add headers for each column in the CSV download
        #array_unshift($list, array_keys($list[0]));
        $callback = function() use ($results)
        {
            # The results are an array of objects
            # var_dump($results[0]);
            # var_dump((array)$results[0]);

            $FH = fopen('php://output', 'w');

            // output the column headings
            fputcsv($FH, [
                'Region Name',
                'Branch Name',
                'Budget',
                'Total Course Allocations Cost',
                'Total Spent',
                'Courses Not Started',
                'Courses Started',
                'Courses Completed'
            ]);

            foreach ($results as $key => $row) {
                $row = [
                    $row->{'Region Name'},
                    $row->{'Branch Name'},
                    $row->{'Budget'},
                    $row->{'Total Course Allocations Cost'},
                    $row->{'Total Spent'},
                    $row->{'Courses Not Started'},
                    $row->{'Courses Started'},
                    $row->{'Courses Completed'}
                ];

                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return Response::stream($callback, 200, $headers);
    }
}
