<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function jTableOptions($records)
    {
        $response["Result"] = "OK";
        $response["Options"] = $records;
        return $response;
    }

    protected function jTableListResponse($records)
    {
        $response["Result"] = "OK";
        $response["Records"] = $records;
        return $response;
    }

    protected function jTablePagedListResponse($records, $totalRecordCount)
    {
        $response["Result"] = "OK";
        $response["Records"] = $records;
        $response["TotalRecordCount"] = $totalRecordCount;
        return $response;
    }

    protected function jTableCreateResponse($record)
    {
        $response["Result"] = "OK";
        $response["Record"] = $record;
        return $response;
    }

    protected function jTableErrorResponse($message)
    {
        $response["Result"] = "ERROR";
        $response["Message"] = $message;
        return $response;
    }

    protected function jsonErrorMessage($message, $code = 400) {
        return response()->json(['message' => $message], $code);
    }
}
