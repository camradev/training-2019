<?php

namespace App\Http\Controllers;

use App\BudgetIncreaseRequests;
use App\CourseAllocation;
use App\Http\Requests;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Redirect;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Pass any previously set filters, so we can reapply them.
        $filters = [
            'memberIdFilter' => $request->input("memberIdFilter"),
            'memberNameFilter' => $request->input("memberNameFilter"),
            'courseNameFilter' => $request->input("courseNameFilter"),
            'branchNameFilter' => $request->input("branchNameFilter")
        ];

        // Don't go further if user has no view or edit perms.
        if ($request->user()->cannot('access-system')) {
            Auth::logout();
            // Redirect user back to login page with a flash message
            return Redirect::to('/auth/login')->with('message', 'You don\'t have permissions to access this system.');
        }

        //////////////////////////////////////////////
        // Prep the RegionSummary info that appears beneath the dashboard allocations, visible for RegionAdmins only.
        if ($request->user()->isRegionAdmin()) {
            // Get the regionSummary info for this year e.g all region' branch budgets, with the budget allocated, and the budget used.
            $regionSummaryRows = CourseAllocation::getAllocationRegionalSummaryForYear(Carbon::now()->year);
            $regionSummary = [];

            // For each of the region admin's list of branches, build the regionSummary array, per branch code, with the branch code and name.
            foreach ($request->user()->getAdminCombinedBranchList() as $code => $name) {
                $regionSummary['branches'][$code]['code'] = $code;
                $regionSummary['branches'][$code]['name'] = $name;
                // Set these to zero, since they may not get populated in the $regionSummaryRows loop, if getAllocationRegionalSummaryForYear has no budget or allocations for this branch
                $regionSummary['branches'][$code]['budget_allocated'] = 0;
                $regionSummary['branches'][$code]['budget_used'] = 0;
                $regionSummary['branches'][$code]['progress_pct'] = 0;
            }
            // Initialise.
            $regionSummary['total_allocation'] = 0;
            $regionSummary['total_usage'] = 0;
            $regionSummary['total_progress_pct'] = 0;

            // Re-sort the summary based on the branch names
            foreach ($regionSummary['branches'] as $key => $row) {
                // replace 0 with the field's index/key
                $branch_names[$key] = $row['name'];
            }
            array_multisort($branch_names, SORT_ASC, $regionSummary['branches']);

            // Now add in the budget stats.
            foreach ($regionSummaryRows as $regionSummaryRow) {
                // Branch budget info

                $budgetAllocated = isset($regionSummaryRow->budget_allocated) ? $regionSummaryRow->budget_allocated : 0;
                $budgetUsed = isset($regionSummaryRow->budget_used) ? $regionSummaryRow->budget_used : 0;
                $budgetUsagePct = isset($regionSummaryRow->budget_allocated) ? ($regionSummaryRow->budget_used / $regionSummaryRow->budget_allocated) * 100 : 0;

                $regionSummary['branches'][$regionSummaryRow->branch_code]['budget_allocated'] = $budgetAllocated;
                $regionSummary['branches'][$regionSummaryRow->branch_code]['budget_used'] = $budgetUsed;
                $regionSummary['branches'][$regionSummaryRow->branch_code]['progress_pct'] = $budgetUsagePct;

                // Update the total budgets allocated
                $regionSummary['total_allocation'] =
                    isset($regionSummary['total_allocation'])
                        ? $regionSummary['total_allocation'] + $budgetAllocated
                        : $budgetAllocated;

                // Update the total budget usage
                $regionSummary['total_usage'] =
                    isset($regionSummary['total_usage'])
                        ? $regionSummary['total_usage'] + $budgetUsed
                        : $budgetUsed;

                // Update the total progress pct
                $regionSummary['total_progress_pct'] =
                    ($budgetAllocated != 0
                        ? ($regionSummary['total_usage'] / $regionSummary['total_allocation']) * 100
                        : 0);
            }
        }
        // End of RegionSummary prep.
        //////////////////////////////////////////////
        // Build the providerSummary on mid right side of dashboard.

        $providerSummaryRows = CourseAllocation::getAllocationSummaryForYear(Carbon::now()->year);
        $providerSummary = [];
        foreach ($providerSummaryRows as $providerSummaryRow) {
            $providerSummary[$providerSummaryRow->provider_name]['courses'][] = $providerSummaryRow;

            // Count the course costs
            $providerSummary[$providerSummaryRow->provider_name]['count'] =
                isset($providerSummary[$providerSummaryRow->provider_name]['count'])
                    ? $providerSummary[$providerSummaryRow->provider_name]['count'] + $providerSummaryRow->course_count
                    : $providerSummaryRow->course_count;

            // Total the course costs
            $providerSummary[$providerSummaryRow->provider_name]['total_cost'] =
                isset($providerSummary[$providerSummaryRow->provider_name]['total_cost'])
                    ? $providerSummary[$providerSummaryRow->provider_name]['total_cost'] + $providerSummaryRow->cost
                    : $providerSummaryRow->cost;
        }
        // End of providerSummary prep.
        ///////////////////////////////////////////////
        // Budget remaining

        if ($request->user()->isBranchAdmin()) {
            foreach ($request->user()->getBranchAdminBranchList() as $code => $name) {
                $budgetStatsResults = CourseAllocation::getBudgetStats(Carbon::now()->year, 'b', $code);
                $budgetStats[trim($name)." branch budget"]['total'] = $budgetStatsResults['total'];
                $budgetStats[trim($name)." branch budget"]['remaining'] = $budgetStatsResults['remaining'];
                $budgetStats[trim($name)." branch budget"]['branch_code'] = $code;
                $budgetStats[trim($name)." branch budget"]['branch_name'] = $name;

                // Check if an increase has been requested, if so, grab the details.
                $budgetIncreaseDetails = BudgetIncreaseRequests::openBudgetRequestDetails($code);
                $budgetStats[trim($name)." branch budget"]['budget_increase_raised'] = $budgetIncreaseDetails ? $budgetIncreaseDetails->created_at->toFormattedDateString() : null;
                $budgetStats[trim($name)." branch budget"]['budget_increase_description'] = $budgetIncreaseDetails ? $budgetIncreaseDetails->description : null;
            }
        }

        if ($request->user()->isFestivalOrganiser()) {
            foreach ($request->user()->getFestivalOrganiserFestivalList() as $code => $name) {
                $budgetStatsResults = CourseAllocation::getBudgetStats(Carbon::now()->year, 'b', $code);
                $budgetStats[trim($name)." festival budget"]['total'] = $budgetStatsResults['total'];
                $budgetStats[trim($name)." festival budget"]['remaining'] = $budgetStatsResults['remaining'];
                $budgetStats[trim($name)." festival budget"]['branch_code'] = $code;
                $budgetStats[trim($name)." festival budget"]['branch_name'] = $name;
            }
        }

        if ($request->user()->isRegionAdmin()) {
            foreach ($request->user()->getRegionAdminList() as $code => $name){
                $budgetStatsResults = CourseAllocation::getBudgetStats(Carbon::now()->year, 'r', $code);
                $budgetStats[trim($name)." region budget"]['total'] = $budgetStatsResults['total'];
                $budgetStats[trim($name)." region budget"]['remaining'] = $budgetStatsResults['remaining'];
            }
        }
        if ($request->user()->isSuperViewer()) {
            $budgetStatsResults = CourseAllocation::getBudgetStats(Carbon::now()->year);
            $budgetStats['Allocated national budget']['total'] = $budgetStatsResults['total'];
            $budgetStats['Allocated national budget']['remaining'] = $budgetStatsResults['remaining'];
        }

        if ($request->user()->can('change-budgets') || $request->user()->isSuperUser()) {
            $requests = BudgetIncreaseRequests::getAllRequests();
        }

        #dd($requests->toArray());
        // Display the dashboard
        return view("dashboard", compact(["budgetStats", "regionSummary", "providerSummary", "requests", "filters"]));
    }

}
