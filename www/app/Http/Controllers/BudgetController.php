<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Budget;
use App\BudgetChange;
use App\BudgetIncreaseRequests;
use App\CamraUser;
use App\CourseAllocation;
use App\Festival;
use App\Region;
use Auth;
use Carbon\Carbon;
use Datatables;
use DB;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use App\Http\Requests;
use Mail;
use Redirect;
use Swift_RfcComplianceException;
use Webpatser\Uuid\Uuid;

class BudgetController extends Controller
{
    public function index(Request $request)
    {
        // If the user cannot view this page then redirect user back to the dashboard with a flash message
        if ($request->user()->cannot('view-budgets')) {
            return response()->redirectToRoute("dashboard")->with('alert-danger', 'You don\'t have permissions to view budgets.');
        }

        return view("budgets.index");
    }

    public function viewNationalBudget(Request $request)
    {
        // If the user cannot view this page then redirect user back to the dashboard with a flash message
        if ($request->user()->cannot('view-budgets')) {
            return response()->redirectToRoute("dashboard")->with('alert-danger', 'You don\'t have permissions to view budgets.');
        }
        // Get the national budget
        $year = Carbon::today()->year;
        /** @var Budget $budget */
        $budget = Budget::where('branch_code', 'NATIONAL')->where('year', $year)->first();

        if (!$budget) {
            return response()->redirectToRoute("dashboard")->with('alert-danger', 'Budgets are not available for the current financial year.');
        }

        $budgetAmount = $budget->amount;

        $allocated = Budget::where('branch_code', '<>', 'NATIONAL')->where('year', $year)->sum('amount');
        $spent = CourseAllocation::getNationalCourseAllocationTotalForYear($year);

        return view("budgets.viewNationalBudget", compact('year', 'budgetAmount', 'allocated', 'spent'));
    }

    public function updateNationalBudget(Request $request)
    {
        // If the user cannot change budgets then send back a 403.3
        if ($request->user()->cannot('change-budgets')) {
            return back()->withInput()->with("alert-danger", "You don't have permissions to modify budgets.");
        }

        // Check all the values we can before hitting db
        $this->validate($request,
            [
                'year' => 'required|numeric|min:2015',
            ],
            [
                'year.required' => 'A year value must be provided.',
                'year.numeric' => 'The year value must be numeric.',
                'year.min' => 'The year value cannot be less than the current year.'
            ]
        );
        $year = $request->input("year");
        $currentUsage = CourseAllocation::getUsageForYear($year);

        // Check all the values we can before hitting db
        $this->validate($request,
            [
                'nationalBudget' => 'required|numeric|min:' . $currentUsage
            ],
            [
                'nationalBudget.required' => 'A new value for the national budget must be provided.',
                'nationalBudget.numeric' => 'The new value for the national budget must be numeric.',
                'nationalBudget.min' => 'The new value for the national budget cannot be less than the current usage.'
            ]
        );

        // Save the change
        /** @var Budget $budget */
        DB::beginTransaction();
        $budget = Budget::where('branch_code', 'NATIONAL')->where('year', $year)->first();
        if (!$budget) {
            return back()->withInput()->with("alert-danger", "Couldn't find the national budget for $year to update.");
        }
        /** @var CamraUser $userDetails */
        $userDetails = $request->user();

        $changeLog = new BudgetChange();
        $changeLog->budget_id = $budget->id;
        $changeLog->budget_year = $budget->year;
        $changeLog->budget_event_id = Uuid::generate(4)->string;
        $changeLog->changed_by_member_id = $userDetails->id;
        $changeLog->changed_by_member_name = $userDetails->getFullName();
        $changeLog->branch_code = $budget->branch_code;
        $changeLog->branch_description = "National Budget";
        $changeLog->old_amount = $budget->amount;
        $changeLog->new_amount = $request->input("nationalBudget");

        // Make the change
        $budget->amount = $request->input("nationalBudget");

        // Save the details
        $budget->save();
        $changeLog->save();

        DB::commit();
        return back();
    }

    public function viewBudgetChangeLogs(Request $request)
    {
        // If the user cannot view this page then redirect user back to the dashboard with a flash message
        if ($request->user()->cannot('view-budgets')) {
            return response()->redirectToRoute("dashboard")->with('alert-danger', 'You don\'t have permissions to view budgets.');
        }
        $years = Budget::distinct()->lists("year")->toArray();
        return view("budgets.viewChangeLogs", compact("years"));
    }

    public function getBudgetChangeLogsJson(Request $request)
    {
        // If the user cannot view this page then redirect user back to the dashboard with a flash message
        if ($request->user()->cannot('view-budgets')) {
            return $this->jsonErrorMessage("You don't have permissions to retrieve budget change log data.");
        }
        $jtStartIndex = $request->input("jtStartIndex");
        $jtPageSize = $request->input("jtPageSize");
        $filterYear = $request->has("filterYear") ? $request->input("filterYear") : Carbon::now()->year;

        $totalRecordCount = BudgetChange::where("budget_year", $filterYear)->count("id");

        $records = BudgetChange::select("*")
            ->where("budget_year", $filterYear)
            ->orderBy("id", "desc")
            ->skip($jtStartIndex)
            ->take($jtPageSize)
            ->get();
        return $this->jTablePagedListResponse($records, $totalRecordCount);
    }

    /**
     * Manage the budget allocations
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function manageBudgetDistribution(Request $request)
    {
        // Pass the regionFilter through from Dashboard 'Action Request' button or email links
        $regionFilter = $request->input('regionFilter');

        // If the user cannot view this page then redirect user back to the dashboard with a flash message
        if ($request->user()->cannot('view-budgets')) {
            return response()->redirectToRoute("dashboard")->with('alert-danger', 'You don\'t have permissions to view budgets.');
        }

        return view("budgets.budgetDistribution", compact('regionFilter'));
    }

    public function getBudgetsJson(Request $request)
    {
        // If the user cannot view budgets then send back a 403.2
        if ($request->user()->cannot('view-budgets')) {
            return $this->jsonErrorMessage("User does not have permissions to view budgets.", 403.2);
        }

//        if ($request->session()->token() != $request->input('_token'))
//        {
//            throw new TokenMismatchException;
//        }

        $year = Carbon::now()->year;
        /** @var Budget $nationalBudget */
        $nationalBudget = Budget::where('branch_code', 'NATIONAL')->where('year', $year)->first();
        if (!$nationalBudget) {
            return $this->jsonErrorMessage("Couldn't find the national budget for $year.", 500);
        }
        $sumBudgetsYear = Budget::where('branch_code', '<>', 'NATIONAL')->where('year', $year)->sum('amount');
        $unallocatedBudget = $nationalBudget->amount - $sumBudgetsYear;

        // Get the list of regions with branches for the current year
        $regionBudgets = Region::with([
            // include the branches
            "branches" => function ($query) {
                // order the branches by their 'name' i.e. Description
                /** @var Builder $query */
                $query->orderBy("branches.Description");
            },
            // include the budget for the current year with each branch
            "branches.budget"
        ])
            // order the regions by their name
            ->orderBy("regions.regionname")
            ->get();

        // Get the list of regions with branches for the current year
        $festivalBudgets = Festival::with("budget")
            ->orderBy("name")
            ->selectRaw("concat(name,' (',code,')') as name, code")
            ->get();

        // Splice in the festival budgets
        $regionBudgets->add([
            'code' => 'FEST',
            'name' => 'Festivals',
            'branches' => $festivalBudgets->toArray()
        ]);

        // Get budget increase requests
        $results = BudgetIncreaseRequests::getAllRequests();

        $requests = [];
        // Reorganise the array
        foreach ($results as $details) {
            $requests[] = $details;
//            $requests[$details['branch_code']]->created_at=$details->created_at->toFormattedDateString();
        }

        return [
            'regionBudgets' => $regionBudgets,
            'unallocatedBudget' => $unallocatedBudget,
            'budgetIncreaseRequests' => $requests
        ];
    }

    /**
     * Save updates to budgets
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function saveBudgetsJson(Request $request)
    {
        // If the user cannot change budgets then send back a 403.3
        if ($request->user()->cannot('change-budgets')) {
            return $this->jsonErrorMessage("User does not have permissions to change budgets.", 403.3);
        }

        $changes = $request->input("budgetChanges");
        if (!$changes) {
            return response()->json(['Message' => 'No Budget Changes Supplied'], 400);
        }

        // Create an id to use to group the changes in the log
        $changeEventId = Uuid::generate(4)->string;

        /** @var CamraUser $userDetails */
        $userDetails = $request->user();

        $year = Carbon::today()->year;

        // Used for the budget increase request emails
        $birApproved = false;
        $requestMemberDetails = null;
        $openRequestDetails = null;
        $requestMemberName = null;

        DB::beginTransaction();
        foreach ($changes as $change) {
            // Create a log event
            $changeLog = new BudgetChange();
            $changeLog->budget_event_id = $changeEventId;
            $changeLog->changed_by_member_id = $userDetails->id;
            $changeLog->changed_by_member_name = $userDetails->getFullName();


            $budgets = Budget::whereBranchCode($change["branch_code"])->where('year', $year)->get();
            // There should only be 1 entry
            if (count($budgets) > 1) {
                DB::rollBack();
                return $this->jsonErrorMessage('More than 1 budget was returned for budget ' . $change["branch_code"] . ' for the year ' . $year);
            }
            if (count($budgets) < 1) {
                DB::rollBack();
                return $this->jsonErrorMessage('Budget ' . $change["branch_code"] . ' was not found for the year ' . $year);
            }

            /** @var Budget $budget */
            $budget = $budgets[0];
            // Save the log details
            $changeLog->budget_id = $budget->id;
            $changeLog->budget_year = $budget->year;
            $changeLog->branch_code = $budget->branch_code;

            if (strlen($budget->branch_code)==3) {
                /** @var Branch $branchDetails */
                $branchDetails = Branch::where("code", $budget->branch_code)->first();
                if (!$branchDetails) {
                    DB::rollBack();
                    return $this->jsonErrorMessage("Couldn't find details for branch with code " . $budget->branch_code);
                }
                $changeLog->branch_description = $branchDetails->Description;
            } else {
                // Must be a festival (4 chars)
                /** @var Festival $branchDetails */
                $festivalDetails = Festival::where("code", $budget->branch_code)->first();
                if (!$festivalDetails) {
                    DB::rollBack();
                    return $this->jsonErrorMessage("Couldn't find details for festival with code " . $budget->branch_code);
                }
                $changeLog->branch_description = $festivalDetails->name;
            }

            $changeLog->old_amount = $budget->amount;
            $changeLog->new_amount = $change["new_amount"];

            // Link the change to the ID of a budget increase request if present, then close the BIR
            if (isset($change["request_id"])) {
                $changeLog->increase_request_id = $change["request_id"];

                // Get the contact details for the open BIR, before we close it
                $requestMemberDetails = BudgetIncreaseRequests::getRequesterMemberDetails($budget->branch_code);
                $openRequestDetails = BudgetIncreaseRequests::openBudgetRequestDetails($budget->branch_code);
                // Now close the BIR
                $birApproved = BudgetIncreaseRequests::approveRequest($budget->branch_code);
                if (!$birApproved) {
                    DB::rollBack();
                    return $this->jsonErrorMessage("Couldn't approve the budget increase request for branch with code " . $budget->branch_code);
                }
            }

            // Make the change
            $budget->amount = $change["new_amount"];

            // Save the details
            $budget->save();
            $changeLog->save();
        }
        DB::commit();

        ////
        // Now transaction is complete, if a BIR was linked to a budget change, then let the requesting member know of the approval.

        // If successfully denied, try to let the requesting branch member know.
        if ($birApproved) {
            try {
                $requestMemberName = $requestMemberDetails->forename . ' ' . $requestMemberDetails->surname;

                $data['memberName'] = $requestMemberName;
                $data['branchCode'] = $openRequestDetails->branch_code;
                $data['branchName'] = $openRequestDetails->branch_name;
                $data['description'] = $openRequestDetails->description;
                $data['memberForename'] = $requestMemberDetails->forename;
                // Send allocation confirmation to the email template.
                Mail::send('emails.birApprovalResponse', $data, function ($message) use ($requestMemberDetails, $requestMemberName) {
                    $message->from('noreply@camra.org.uk', 'NoReply');
                    $message->to($requestMemberDetails->email, $requestMemberName)->subject('CAMRA Budget Increase Request');
                });
            } catch (Swift_RfcComplianceException $ex) {
                $exceptionMsg = "The budget increase request was approved, however, could not notify the requesting member ($requestMemberName) due to mailer reporting the following message:<p>" . $ex->getMessage();
                return response()->redirectToRoute("dashboard")->with('alert-warning', $exceptionMsg);
            }
        }

        //
        ////

        return ['Result' => 'OK'];
    }

    public function addBudgetIncreaseRequest(Request $request)
    {
        // Just get the branch code for now
        $addModalBranchCode = $request->input('addModalBranchCode');

        // Don't go further if user isn't a branch admin, and can't admin the branch they are trying to add.
        if (!($request->user()->isBranchAdmin() && in_array($addModalBranchCode, array_keys($request->user()->branchAdminBranchList)))) {
            // Redirect user back to login page with a flash message
            return response()->redirectToRoute("dashboard")->with('alert-danger', 'You don\'t have permissions to create this budget increase request.');
        }

        // Get remaining inputs now we know we'll need them
        $addModalBranchName = $request->input('addModalBranchName');
        $addModalDescription = $request->input('addModalDescription');

        // Search for an open request for the supplied branch code and update if found.
        $success = BudgetIncreaseRequests::addRequest($addModalBranchCode, $addModalBranchName, $addModalDescription);

        if (!$success) {
            return response()->redirectToRoute("dashboard")->with('alert-danger', 'Error, unable to create budget increase request.');
        } else {
            // Get VolunteerDirector details
            $volunteerDirector = getVolunteerDirectorMemberId();
            $volunteerDirectorName = $volunteerDirector->forename . ' ' . $volunteerDirector->forename;

            // Alert the Volunteer Director to the new BIR
            if (!emailValid($volunteerDirector->email)) {
                $exceptionMsg = "The budget increase request was successful, however, could not notify the Volunteer Director ($volunteerDirectorName) due to " . (empty($volunteerDirector->email) ? "missing email address." : "invalid email address: " . $volunteerDirector->email);
                return response()->redirectToRoute("dashboard")->with('alert-warning', $exceptionMsg);
            } else {
                try {
                    $data['memberName'] = $request->user()->forename . ' ' . $request->user()->surname;
                    $data['branchCode'] = $addModalBranchCode;
                    $data['branchName'] = $addModalBranchName;
                    $data['description'] = $addModalDescription;
                    $data['regionCode'] = getRegionCodeForBranchCode($addModalBranchCode);
                    $data['volunteerDirectorForename'] = $volunteerDirector->forename;
                    // Send allocation confirmation to the email template.
                    Mail::send('emails.birVolunteerDirectorNotification', $data, function ($message) use ($volunteerDirector, $volunteerDirectorName) {
                        $message->from('noreply@camra.org.uk', 'NoReply');
                        $message->to($volunteerDirector->email, $volunteerDirectorName)->subject('CAMRA Budget Increase Request');
                    });
                } catch (Swift_RfcComplianceException $ex) {
                    $exceptionMsg = "The budget increase request was successful, however, could not notify the Volunteer Director ($volunteerDirectorName) due to mailer reporting the following message:<p>" . $ex->getMessage();
                    return response()->redirectToRoute("dashboard")->with('alert-warning', $exceptionMsg);
                }

            };
            return response()->redirectToRoute("dashboard")->with('alert-success', 'Budget increase request created, and a notification sent to the Volunteer Director.');
        }
    }

    public function editBudgetIncreaseRequest(Request $request)
    {
        // Just get the branch code for now
        $editModalBranchCode = $request->input('editModalBranchCode');

        // Don't go further if user isn't a branch admin, and can't admin the branch they are trying to edit.
        if (!($request->user()->isBranchAdmin() && in_array($editModalBranchCode, array_keys($request->user()->branchAdminBranchList)))) {
            // Redirect user back to login page with a flash message
            return response()->redirectToRoute("dashboard")->with('alert-danger', 'You don\'t have permissions to update this budget increase request.');
        }

        // Get remaining inputs now we know we'll need them
        $editModalDescription = $request->input('editModalDescription');

        // Search for an open request for the supplied branch code and update if found.
        $success = BudgetIncreaseRequests::editRequest($editModalBranchCode, $editModalDescription);

        if (!$success) {
            return response()->redirectToRoute("dashboard")->with('alert-danger', 'Error, unable to update budget increase request - request not found.');
        } else {
            return response()->redirectToRoute("dashboard")->with('alert-success', 'Budget increase request updated.');
        }
    }

    public function cancelBudgetIncreaseRequest(Request $request)
    {
        $cancelModalBranchCode = $request->input('cancelModalBranchCode');
        // Don't go further if user isn't a branch admin, and can't admin the branch they are trying to cancel.
        if (!($request->user()->isBranchAdmin() && in_array($cancelModalBranchCode, array_keys($request->user()->branchAdminBranchList)))) {
            // Redirect user back to login page with a flash message
            return response()->redirectToRoute("dashboard")->with('alert-danger', 'You don\'t have permissions to cancel this budget increase request.');
        }

        // Search for an open request for the supplied branch code and cancel if found.
        $success = BudgetIncreaseRequests::cancelRequest($cancelModalBranchCode);

        if (!$success) {
            return response()->redirectToRoute("dashboard")->with('alert-danger', 'Error, unable to cancel budget increase request - request not found.');
        } else {
            return response()->redirectToRoute("dashboard")->with('alert-success', 'Budget increase request cancelled.');
        }
    }

    public function denyBudgetIncreaseRequest(Request $request)
    {
        // Just get the branch code for now
        $denyModalBranchCode = $request->input('denyModalBranchCode');

        // Don't go further if user isn't a branch admin, and can't admin the branch they are trying to edit.
        if ($request->user()->cannot('change-budgets')) {
            // Redirect user back to login page with a flash message
            return response()->redirectToRoute("dashboard")->with('alert-danger', 'You don\'t have permissions to deny this budget increase request.');
        }

        // Get remaining inputs now we know we'll need them
        $denyModalBranchName = $request->input('denyModalBranchName');
        $denyModalDescription = $request->input('denyModalDescription');
        $denyModalReason = $request->input('denyModalReason');

        // Get the contact details for the open BIR
        $requestMemberDetails = BudgetIncreaseRequests::getRequesterMemberDetails($denyModalBranchCode);
        $requestMemberName = $requestMemberDetails->forename . ' ' . $requestMemberDetails->surname;

        // Search for an open request for the supplied branch code and deny if found.
        $success = BudgetIncreaseRequests::denyRequest($denyModalBranchCode, $denyModalReason);

        // If successfully denied, try to let the requesting branch member know.
        if ($success) {
            try {
                $data['memberName'] = $requestMemberName;
                $data['branchCode'] = $denyModalBranchCode;
                $data['branchName'] = $denyModalBranchName;
                $data['description'] = $denyModalDescription;
                $data['response'] = $denyModalReason;
                $data['memberForename'] = $requestMemberDetails->forename;
                // Send allocation confirmation to the email template.
                Mail::send('emails.birDenialResponse', $data, function ($message) use ($requestMemberDetails, $requestMemberName) {
                    $message->from('noreply@camra.org.uk', 'NoReply');
                    $message->to($requestMemberDetails->email, $requestMemberName)->subject('CAMRA Budget Increase Request');
                });
            } catch (Swift_RfcComplianceException $ex) {
                $exceptionMsg = "The budget increase request was denied, however, could not notify the requesting member ($requestMemberName) due to mailer reporting the following message:<p>" . $ex->getMessage();
                return response()->redirectToRoute("dashboard")->with('alert-warning', $exceptionMsg);
            }
        }

        if ($request->ajax()) {
            // This request came from the budget distribution Deny via ajax query.
            if (!$success) {
                return response()->json(["state" => "error", "message" => "Error, unable to deny budget increase request - request not found."], 400);
            } else {
                return response()->json(["state" => "ok", "message" => "Budget increase request denied."]);
            }
        } else {
            // This request came from the dashboard Deny.
            if (!$success) {
                return response()->redirectToRoute("dashboard")->with('alert-danger', 'Error, unable to deny budget increase request - request not found.');
            } else {
                return response()->redirectToRoute("dashboard")->with('alert-success', 'Budget increase request denied.');
            }
        }

    }

    /**
     * Returns a json object representing the available budget sources
     * @return array
     */
    public function getBudgetSourceOptions(Request $request)
    {
        $data = [];

        // If branch admin then add this, should always be "branch"
        if ($request->user()->isBranchAdmin() || $request->user()->isSuperViewer()) {
            $data[] = ["Value" => "branch", "DisplayText" => "Member's Branch"];
        }

        // Only add relevant festivals
        if ($request->user()->isSuperViewer() || $request->user()->isFestivalOrganiser()) {
            if ($request->user()->isSuperViewer()) {
                $festivals = Festival::orderBy('name')->get();
                foreach($festivals as $key => $festival){
                    $data[] = ["Value" => $festival->code, "DisplayText" => "Festival - " . $festival->nameWithCode];
                }
            } else {
                $festivals = $request->user()->festivalOrganiserFestivalList;
                foreach($festivals as $code => $name){
                    $data[] = ["Value" => $code, "DisplayText" => "Festival - " . $name . " ($code)"];
                }
            }


        }

        return $this->jTableOptions($data);
    }
}
