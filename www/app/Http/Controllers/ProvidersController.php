<?php

namespace App\Http\Controllers;

use App\CourseProvider;
use Illuminate\Http\Request;
use App\Http\Requests;

class ProvidersController extends Controller
{



    /**
     * Returns a json object representing the available providers
     * @return array
     */
    public function getProviderOptions()
    {
        /** @var CourseProvider[] $providers */
        // Only get providers that have courses
        $providers = CourseProvider::has('active_courses')->get();
        $data = [];
        foreach($providers as $provider) {
            $data[] = ["Value" => $provider->id, "DisplayText" => $provider->name];
        }
        return $this->jTableOptions($data);
    }
}