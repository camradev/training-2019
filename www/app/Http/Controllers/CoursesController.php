<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;
use App\Http\Requests;

class CoursesController extends Controller
{

    public function index(Request $request){

        // Pass any previously set filters, so we can reapply them.
        $filters = [
            'memberIdFilter' => $request->input("memberIdFilter"),
            'memberNameFilter' => $request->input("memberNameFilter"),
            'courseNameFilter' => $request->input("courseNameFilter"),
            'branchNameFilter' => $request->input("branchNameFilter")
        ];

        // Don't go further if user has no view or edit perms.
        if ($request->user()->cannot('access-system')) {
            Auth::logout();
            // Redirect user back to login page with a flash message
            return Redirect::to('/auth/login')->with('message', 'You don\'t have permissions to access this system.');
        }
        return view("courses.allocations", compact(["filters"]));
    }

    /**
     * Returns a json object representing the available courses for a given provider
     * @param Request $request
     * @return array
     */
    public function getCourseOptions(Request $request)
    {
        /** @var Course[] $courses */
        //debug("Getting courses");
        $courses = Course::where('course_provider_id',$request->input("providerId"))->where('active',true)->get();
        //debug($courses);
        $data = [];
        foreach($courses as $course) {
            $data[] = ["Value" => $course->id, "DisplayText" => "$course->name - £$course->cost_pounds"];
        }
        return $this->jTableOptions($data);
    }
}
