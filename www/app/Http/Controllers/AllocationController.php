<?php

namespace App\Http\Controllers;

use App\Budget;
use App\CamraUser;
use App\Course;
use App\CourseAllocation;
use App\CourseAllocationStatus;
use App\Http\Requests;
use App\Providers\CamraUserProvider;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;
use Swift_RfcComplianceException;

class AllocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        start_measure("fetch", "Getting data");
        // Create a new query we can build on. We use a custom query for the listing to be efficient and easily
        // sort on any of the returned columns as well as make them available in the returned JSON response
        /** @var \Illuminate\Database\Query\Builder $query */
        $query = CourseAllocation::allocationsForListViewQuery();

        // jtSorting is sent through by jTable
        $sort = $request->input("jtSorting");
        $jtStartIndex = $request->input("jtStartIndex");
        $jtPageSize = $request->input("jtPageSize");
        $memberIdFilter = $request->input("memberIdFilter");
        $memberNameFilter = $request->input("memberNameFilter");
        $courseNameFilter = $request->input("courseNameFilter");
        $branchNameFilter = $request->input("branchNameFilter");

        // Prep the query

        // Apply the memberIdFilter if we have one
        if (!empty($memberIdFilter)) {
            $query->where("member_id", "like", "%$memberIdFilter%");
        }

        // Apply the memberNameFilter if we have one
        if (!empty($memberNameFilter)) {
            $query->where("member_name", "like", "%$memberNameFilter%");
        }

        // Apply the courseNameFilter if we have one
        if (!empty($courseNameFilter)) {
            $query->whereRaw("`courses`.`name` like '%$courseNameFilter%'");
        }

        // Apply the branchNameFilter if we have one
        if (!empty($branchNameFilter)) {
            $query->where("branch_name", "like", "%$branchNameFilter%");
        }

        $totalRecordCount = $query->count();

        // Allowed sortable columns
        $allowedSortableColumns = ["member_id", "member_name", "branch_name", "course_name", "course_cost_pounds", "status_name", "created_at", "budget_source"];
        if ($sort) {
            // Check the sort can be split into two parts on space
            // the first part must in the allowedSortableColumns
            // the second part is ASC or DESC
            $parts = preg_split("/ /", $sort);
            if (!in_array($parts[0], $allowedSortableColumns) || !in_array($parts[1], ["ASC", "DESC"])) {
                return self::jTableErrorResponse("Sorting on $sort is not supported");
            }
            $query = $query->orderBy($parts[0], $parts[1]);
        }

        // Apply any paging logic
        $query = $query->skip($jtStartIndex)->take($jtPageSize);

        // Execute the query now that any ordering has been applied
        $results = $query->get();

        stop_measure("fetch");
        return $this->jTablePagedListResponse($results, $totalRecordCount);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $memberId = $request->input("member_id");
        $memberDetails = CamraUserProvider::getUserDetails($memberId);
        if ($memberDetails == null) {
            return $this->jTableErrorResponse("Member with ID '$memberId' does not exist.");
        }

        // Check course id and status id are valid
        $budgetSource = $request->input("budget_source");
        if ($budgetSource==null) {
            return $this->jTableErrorResponse("Budget Source not provided.");
        }

        // The following check confirms whether this string is valid, since it has to exist in one of the branch/region/festival key arrays.

        /** @var CamraUser $user */
        $user = Auth::user();
        if (!$user->isSuperUser()) {
            // The user may be a branch and/or region admin AND also a festival organiser, so the budget source type governs which perms to check against.
            if ($budgetSource == 'branch') {
                # Check that the Admins' list of branches covers the member's branch.
                if (!in_array($memberDetails->branch, array_keys($user->getAdminCombinedBranchList()))) {
                    return $this->jTableErrorResponse("You do not have the required permissions to allocate courses for member with ID '$memberId'.");
                }
            } else {
                //dd($user->getFestivalOrganiserFestivalIdList());
                // User needs to have perms for the requested festival budget.
                if (!in_array($budgetSource, $user->getFestivalOrganiserFestivalIdList())) {
                    return $this->jTableErrorResponse("You do not have the required permissions to allocate courses against a festival budget for member with ID '$memberId'.");
                }

            }
        }

        // Check course id and status id are valid
        $courseId = $request->input("course_id");

        // Check that member doesn't already have an open allocation
        $memberAllocations = CourseAllocation::where('member_id',$memberId)->where('course_id',$courseId)->whereIn('status_id',[1,2])->lists('id');
        if (count($memberAllocations)) {
                return $this->jTableErrorResponse("An incomplete allocation already exists for course ID '$courseId', against member with ID '$memberId' - the course must be completed before a new allocation can be made.");
        }

        /** @var Course $course */
        $course = Course::find($courseId);
        if ($course == null) {
            return $this->jTableErrorResponse("Course with ID '$courseId' does not exist.");
        }

        $statusId = $request->input("status_id");
        if (CourseAllocationStatus::find($statusId) == null) {
            return $this->jTableErrorResponse("Status with ID '$statusId' does not exist.");
        }

        // Continue with create
        $memberName = $memberDetails->forename . ' ' . $memberDetails->surname;

        // Save either branch or festival info, depending on budget used.
        if ($budgetSource == "branch"){
            $memberBranchId = $memberDetails->branch;
            $memberBranchName = $memberDetails->branchdesc;
        } else {
            $memberBranchId = $budgetSource;
            $memberBranchName = getFestivalNameForCode($budgetSource);
            $budgetSource = "festival";
        }

        // Check that there are sufficient funds available for the current year for the allocated member's branch, or a festival budget
        $year = Carbon::now()->year;
        // Check using either the member's branch code or the festival code
        $branchUsageForYear = CourseAllocation::getBranchUsageForYear($year, $memberBranchId);
        //dd($branchUsageForYear);
        // Get the budget for the member's branch code or the festival code
        $budget = Budget::where('branch_code', $memberBranchId)->where('year', $year)->first();
        //dd($budget);
        $budgetAmount = $budget ? $budget->amount : 0;
        #dd($budgetAmount);
        if ($budgetAmount - $branchUsageForYear < $course->cost_pounds) {
            return $this->jTableErrorResponse("Insufficient budget available to allocate this course.");
        }

        $newAllocation = CourseAllocation::create([
            "member_id" => $memberId,
            "member_name" => $memberName,
            "budget_source" => $budgetSource,
            "branch_id" => $memberBranchId,
            "branch_name" => $memberBranchName,
            "course_id" => $courseId,
            "status_id" => $statusId
        ]);
        $allocation = CourseAllocation::allocationsForListViewQuery($newAllocation->id)->first();

        // Send member details for email checking
        if (!emailValid($memberDetails->email)) {
            $exceptionMsg = "The allocation was successful, however, could not notify member $memberName ($memberId) of allocation due to " . (empty($memberDetails->email) ? "missing email address." : "invalid email address: $memberDetails->email");
            $allocation->_validationExceptionMessage = $exceptionMsg;
        } else {
            $data['memberForename'] = $memberDetails->forename;
            $data['courseName'] = $course->name;
            $data['courseEmailNotes'] = $course->email_notes;
            $data['providerName'] = $allocation->course_provider_name;
            $data['providerEmailNotes'] = $course->provider_email_notes->email_notes;

            // Not all members have an email address, so only send notifications if present. jquery will display dialog if empty/invalid, or exception raised during send (via catch below).
            if ($memberDetails->email !== null) {
                try {
                    // Send allocation confirmation to the email template.
                    Mail::send('emails.allocationNotification', $data, function ($message) use ($memberDetails, $memberName) {
                        $message->from('noreply@camra.org.uk', 'NoReply');
                        $message->to($memberDetails->email, $memberName)->subject('CAMRA Training Notification');
                    });
                } catch (Swift_RfcComplianceException $ex) {
                    $allocation->_validationExceptionMessage = "The allocation was successful, however, could not notify member $memberName ($memberId) of allocation due to mailer reporting the following message:<p>" . $ex->getMessage();
                }
            }
        };
        return $this->jTableCreateResponse($allocation);
    }

    /**
     * Store an edited resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // Retrieve the allocation
        $allocationId = $request->input("id");
        $allocation = CourseAllocation::find($allocationId)->first();
        if ($allocationId == null) {
            return $this->jTableErrorResponse("Allocation with ID '$allocationId' does not exist.");
        }

        // Check member ID is valid
        $memberId = $allocation->member_id;
        $memberDetails = CamraUserProvider::getUserDetails($memberId);
        if ($memberDetails == null) {
            return $this->jTableErrorResponse("Member with ID '$memberId' does not exist.");
        }

        // Check user has perms to update the record
        /** @var CamraUser $user */
        $user = Auth::user();
        if (!$user->isSuperUser()) {
            # Check that the Admins' list of branches covers the member's branch.
            if (!in_array($memberDetails->branch, array_keys($user->branchAdminBranchList))) {
                return $this->jTableErrorResponse("You do not have the required permissions to allocate courses for member with ID '$memberId'.");
            }
        }

        // Get the new status
        $statusId = $request->input("status_id");

        // Check status is valid
        if (CourseAllocationStatus::find($statusId) == null) {
            return $this->jTableErrorResponse("Status with ID '$statusId' does not exist.");
        }

        // Set and save the new status
        $allocation->status_id = $statusId;
        $allocation->save();
        return $this->jTableCreateResponse([
            // Return just this field, to update that cell in table.
            'status_id' => $allocation->status_id,
            'status_name' => $allocation->status->name
        ]);
    }
}
