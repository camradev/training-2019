<?php

namespace App\Http\Controllers;

use App\CourseAllocationStatus;
use Illuminate\Http\Request;
use App\Http\Requests;

class StatusesController extends Controller
{
    /**
     * Returns a json object representing the available statuses
     * @return array
     */
    public function getStatusOptions()
    {
        /** @var CourseAllocationStatus[] $statuses */
        $statuses = CourseAllocationStatus::get();
        $data = [];
        foreach($statuses as $status) {
            $data[] = ["Value" => $status->id, "DisplayText" => $status->name];
        }
        return $this->jTableOptions($data);
    }
}