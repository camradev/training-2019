<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

Route::group(['middleware' => 'auth'], function () {
    // Main controllers
    Route::get('/', ['as' => 'dashboard', 'middleware' => 'auth', 'uses' => 'DashboardController@index']);
    Route::get('/courseAllocations', ['as' => 'courseAllocations', 'middleware' => 'auth', 'uses' => 'CoursesController@index']);
    Route::get('/budgets', ['as' => 'budgetOptions', 'uses' => 'BudgetController@index']);
    Route::get('/budgets/manage', ['as' => 'manageBudgetDistribution', 'uses' => 'BudgetController@manageBudgetDistribution']);
    Route::get('/budgets/manage/budgets', ['as' => 'getBudgetJson', 'uses' => 'BudgetController@getBudgetsJson']);
    Route::get('/budgets/manage/budget-increase-requests', ['as' => 'getBudgetIncreaseRequestsJson', 'uses' => 'BudgetController@getBudgetIncreaseRequestsJson']);
    Route::post('/budgets/manage/budgets', ['as' => 'saveBudgetJson', 'uses' => 'BudgetController@saveBudgetsJson']);
    Route::get('/budgets/national-budget', ['as' => 'viewNationalBudget', 'uses' => 'BudgetController@viewNationalBudget']);
    Route::post('/budgets/national-budget', ['as' => 'updateNationalBudget', 'uses' => 'BudgetController@updateNationalBudget']);
    Route::get('/budgets/logs', ['as' => 'viewBudgetChangeLogs', 'uses' => 'BudgetController@viewBudgetChangeLogs']);
    Route::get('/budgets/request/add', ['as' => 'addBudgetIncreaseRequest', 'uses' => 'BudgetController@addBudgetIncreaseRequest']);
    Route::get('/budgets/request/edit', ['as' => 'editBudgetIncreaseRequest', 'uses' => 'BudgetController@editBudgetIncreaseRequest']);
    Route::get('/budgets/request/cancel', ['as' => 'cancelBudgetIncreaseRequest', 'uses' => 'BudgetController@cancelBudgetIncreaseRequest']);
    Route::post('/budgets/request/deny', ['as' => 'denyBudgetIncreaseRequest', 'uses' => 'BudgetController@denyBudgetIncreaseRequest']);
    Route::get('/reports', ['as' => 'reports', 'middleware' => 'auth', 'uses' => 'ReportsController@index']);
    Route::get('/reports/all-allocations', ['as' => 'viewReportAllAllocations', 'middleware' => 'auth', 'uses' => 'ReportsController@viewAllAllocations']);
    Route::get('/reports/all-allocations-for-csv-export', ['as' => 'viewAllAllocationsForCsvExport', 'middleware' => 'auth', 'uses' => 'ReportsController@viewAllAllocationsForCsvExport']);
    Route::get('/reports/branch-allocations', ['as' => 'viewReportBranchAllocations', 'middleware' => 'auth', 'uses' => 'ReportsController@viewBranchAllocations']);
    Route::get('/reports/branch-allocations-for-csv-export', ['as' => 'viewBranchAllocationsForCsvExport', 'middleware' => 'auth', 'uses' => 'ReportsController@viewBranchAllocationsForCsvExport']);
    Route::get('/reports/region-summary', ['as' => 'viewReportRegionSummary', 'middleware' => 'auth', 'uses' => 'ReportsController@viewRegionSummary']);
    Route::get('/reports/region-summary-for-csv-export', ['as' => 'viewReportRegionSummaryForCsvExport', 'middleware' => 'auth', 'uses' => 'ReportsController@viewRegionSummaryForCsvExport']);

    // Endpoints for jTable allocations
    Route::post('/allocations/list', ['as' => 'allocationsList', 'uses' => 'AllocationController@show']);
    Route::post('/allocations/create', ['as' => 'allocationsCreate', 'uses' => 'AllocationController@store']);
    Route::post('/allocations/update', ['as' => 'allocationsUpdate', 'uses' => 'AllocationController@update']);
    Route::post('/courses', ['as' => 'courseOptions', 'uses' => 'CoursesController@getCourseOptions']);
    Route::post('/providers', ['as' => 'providerOptions', 'uses' => 'ProvidersController@getProviderOptions']);
    Route::post('/statuses', ['as' => 'statusOptions', 'uses' => 'StatusesController@getStatusOptions']);
    Route::post('/budgets/budget-sources', ['as' => 'budgetSourceOptions', 'uses' => 'BudgetController@getBudgetSourceOptions']);

    // Endpoints for jTable budget changelog
    Route::post('/budgets/logs/json', ['as' => 'viewBudgetChangeLogsJson', 'uses' => 'BudgetController@getBudgetChangeLogsJson']);

});