<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBudgetSourceToCourseAllocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_allocations', function (Blueprint $table) {
            $table->string('budget_source')->after('member_name')->default('branch');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // For SQLite, dropping a column involves creating a new table and dropping the old one.
        // We need to disable key constraints briefly, but only for SQLite

        if (DB::connection() instanceof \Illuminate\Database\SQLiteConnection) {
            DB::statement(DB::raw('PRAGMA foreign_keys=0'));
        }

        Schema::table('course_allocations', function (Blueprint $table) {
            $table->dropColumn('budget_source');
        });

        if (DB::connection() instanceof \Illuminate\Database\SQLiteConnection) {
            DB::statement(DB::raw('PRAGMA foreign_keys=1'));
        }
    }
}
