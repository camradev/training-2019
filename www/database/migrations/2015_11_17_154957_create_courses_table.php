<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->decimal("cost_pounds", 8, 2);
            $table->unsignedInteger('course_provider_id');
            $table->string('provider_course_ref');
            $table->string('email_notes', 4096);
            $table->timestamps();

            $table->foreign('course_provider_id')->references('id')->on('course_providers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('courses');
    }
}
