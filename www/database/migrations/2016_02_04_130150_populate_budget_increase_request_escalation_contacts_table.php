<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PopulateBudgetIncreaseRequestEscalationContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('budget_increase_request_escalation_contacts')->insert(
            array(
                'member_id' => '468481',
                'member_name' => 'Ganesh Gudka',
                'notes' => 'Head of Finance',
            )
        );

        DB::table('budget_increase_request_escalation_contacts')->insert(
            array(
                'member_id' => '156356',
                'member_name' => 'John Cottrell',
                'notes' => 'Membership and Volunteer services',
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
