<?php

use Illuminate\Database\Migrations\Migration;

class PopulateBudgetIncreaseRequestResolutionTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $now = date('Y-m-d H:i:s');

        DB::table('budget_increase_request_resolution_types')->insert(
            array(
                'id' => 1,
                'description' => 'Approved',
                'created_at' => $now,
                'updated_at' => $now
            )
        );

        DB::table('budget_increase_request_resolution_types')->insert(
            array(
                'id' => 2,
                'description' => 'Denied',
                'created_at' => $now,
                'updated_at' => $now
            )
        );

        DB::table('budget_increase_request_resolution_types')->insert(
            array(
                'id' => 3,
                'description' => 'Cancelled',
                'created_at' => $now,
                'updated_at' => $now
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    //
    }
}
