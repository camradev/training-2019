<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSchemaBudgetChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_changes', function (Blueprint $table) {
            $table->integer('increase_request_id')->unsigned()->nullable()->after('budget_year');

            $table->foreign('increase_request_id')->references('id')->on('budget_increase_requests');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // For SQLite, dropping a column involves creating a new table and dropping the old one.
        // We need to disable key constraints briefly, but only for SQLite

        if (DB::connection() instanceof \Illuminate\Database\SQLiteConnection) {
            DB::statement(DB::raw('PRAGMA foreign_keys=0'));
        }

        Schema::table('budget_changes', function (Blueprint $table) {
            $table->dropForeign('budget_changes_increase_request_id_foreign');
            $table->dropColumn('increase_request_id');
        });

        if (DB::connection() instanceof \Illuminate\Database\SQLiteConnection) {
            DB::statement(DB::raw('PRAGMA foreign_keys=1'));
        }
    }
}
