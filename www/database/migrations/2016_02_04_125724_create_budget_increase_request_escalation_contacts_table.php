<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetIncreaseRequestEscalationContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_increase_request_escalation_contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('member_id',6)->unique();
            $table->string('member_name');
            $table->string('notes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('budget_increase_request_escalation_contacts');
    }
}
