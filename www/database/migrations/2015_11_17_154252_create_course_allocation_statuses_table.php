<?php

use App\CourseAllocationStatus;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseAllocationStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_allocation_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->timestamps();
        });
        Eloquent::unguard();
        CourseAllocationStatus::create(['id' => CourseAllocationStatus::STATUS_ALLOCATED, 'name' => 'Allocated']);
        CourseAllocationStatus::create(['id' => CourseAllocationStatus::STATUS_STARTED, 'name' => 'Started']);
        CourseAllocationStatus::create(['id' => CourseAllocationStatus::STATUS_PASSED, 'name' => 'Passed']);
        CourseAllocationStatus::create(['id' => CourseAllocationStatus::STATUS_FAILED, 'name' => 'Failed']);
        Eloquent::reguard();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('course_allocation_statuses');
    }
}
