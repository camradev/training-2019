<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetIncreaseRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_increase_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('member_id',6);
            $table->string('member_name');
            $table->string('branch_code');
            $table->string('branch_name');
            $table->string('description', 500);
            $table->dateTime('escalated_at')->nullable();
            $table->dateTime('resolved_at')->nullable();
            $table->string('resolved_by_member_id',6)->nullable();
            $table->string('resolved_by_member_name')->nullable();
            $table->integer('resolution_type_id')->unsigned()->nullable();
            $table->string('resolution_reason', 500)->nullable();
            $table->timestamps();

            $table->foreign('resolution_type_id')->references('id')->on('budget_increase_request_resolution_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('budget_increase_requests');
    }
}
