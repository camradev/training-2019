<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_changes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('budget_event_id');
            $table->unsignedInteger('budget_id');
            $table->string('branch_code');
            $table->string('branch_description');
            $table->string('changed_by_member_id');
            $table->string('changed_by_member_name');
            $table->decimal('old_amount', 8, 2);
            $table->decimal('new_amount', 8, 2);
            $table->unsignedInteger('budget_year');
            $table->timestamps();

            $table->foreign('budget_id')->references('id')->on('budgets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('budget_changes');
    }
}
