<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('entity_primary_key');
            $table->string('entity_type');
            $table->string('change_description');
            // This is which system the change was made in, webui for example
            $table->string('changed_in')->nullable();
            $table->string('changed_by_member_id')->nullable();
            $table->string('changed_by_member_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('logs');
    }
}
