<?php

use App\Admin;
use App\Budget;
use App\Course;
use App\CourseAllocation;
use App\CourseAllocationStatus;
use App\CourseProvider;
use App\Festival;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DevSeeder extends Seeder
{
    public function run()
    {
        parent::run();

        Eloquent::unguard();

        DB::table('logs')->delete();
        DB::table('budgets')->delete();
        DB::table('course_allocations')->delete();
        DB::table('courses')->delete();
        DB::table('course_providers')->delete();
        DB::table('admins')->delete();


        $memberBob = [
            "member_id"   => "111111",
            "member_name" => "Bob Smith",
            "branch_id"   => "RED",
            "branch_name" => "Redditch & Bromsgrove"
        ];

        $memberHarry = [
            "member_id"   => "222222",
            "member_name" => "Harry Jones",
            "branch_id"   => "ENW",
            "branch_name" => "North West Essex"
        ];

        $memberMark = [
            "member_id"   => "333333",
            "member_name" => "Mark Jeffries",
            "branch_id"   => "BEN",
            "branch_name" => "North Bedfordshire"
        ];

        $memberCplTest1 = [
            "member_id"   => "999991",
            "member_name" => "CPLTest1",
            "branch_id"   => "ENW",
            "branch_name" => "North West Essex"
        ];

        $memberCplTest2 = [
            "member_id"   => "999992",
            "member_name" => "CPLTest2",
            "branch_id"   => "BEN",
            "branch_name" => "North Bedfordshire"
        ];

        $memberCplTest3 = [
            "member_id"   => "999993",
            "member_name" => "CPLTest3",
            "branch_id"   => "ENW",
            "branch_name" => "North West Essex"
        ];

        $memberCplTest4 = [
            "member_id"   => "999994",
            "member_name" => "CPLTest4",
            "branch_id"   => "BEN",
            "branch_name" => "North Bedfordshire"
        ];

        $memberCplTest5 = [
            "member_id"   => "999995",
            "member_name" => "CPLTest5",
            "branch_id"   => "BEN",
            "branch_name" => "North Bedfordshire"
        ];

        $memberRyan = [
            "member_id"   => "489748",
            "member_name" => "Ryan Halley",
            "branch_id"   => "SHE",
            "branch_name" => "South Hertfordshire"
        ];

        $memberDan = [
            "member_id"   => "457051",
            "member_name" => "Daniel Abbatt",
            "branch_id"   => "SHE",
            "branch_name" => "South Hertfordshire"
        ];

        // Populate some festivals
        Festival::create(['id' => 1, 'code' => 'FESTIVAL_GBBF', 'name' => 'Great British Beer Festival', 'organiser_member_id' => '226731']);
        Festival::create(['id' => 2, 'code' => 'FESTIVAL_ABCD', 'name' => 'Another Festival', 'organiser_member_id' => '226731']);
        Festival::create(['id' => 3, 'code' => 'FESTIVAL_EFGH', 'name' => 'Yet Another Festival', 'organiser_member_id' => '212081']);

        // Populate some course providers
        CourseProvider::create(['id' => 4, 'name' => 'Provider four', 'email_notes' => 'Lots of <b>useful</b> provider info<br>Lots of <b>useful</b> provider info<br>Lots of <b>useful</b> provider info']);
        CourseProvider::create(['id' => 5, 'name' => 'Provider five', 'email_notes' => 'Lots of <b>useful</b> provider info<br>Lots of <b>useful</b> provider info<br>Lots of <b>useful</b> provider info']);
        CourseProvider::create(['id' => 6, 'name' => 'Provider six', 'email_notes' => 'Lots of <b>useful</b> provider info<br>Lots of <b>useful</b> provider info<br>Lots of <b>useful</b> provider info']);
        CourseProvider::create(['id' => 7, 'name' => 'Provider seven', 'email_notes' => 'Lots of <b>useful</b> provider info<br>Lots of <b>useful</b> provider info<br>Lots of <b>useful</b> provider info']);
        CourseProvider::create(['id' => 8, 'name' => 'CPL', 'email_notes' => 'Info about CPL']);

        // Populate some courses
        Course::create(['id' => 4, 'name' => 'P4 Course four', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx4', 'cost_pounds' => 10, 'email_notes' =>'Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info']);
        Course::create(['id' => 5, 'name' => 'P4 Course five', 'course_provider_id' => 4, 'provider_course_ref' => 'CPLxx5', 'cost_pounds' => 20, 'email_notes' =>'Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info']);
        Course::create(['id' => 6, 'name' => 'P5 Course six', 'course_provider_id' => 5, 'provider_course_ref' => 'CPLxx6', 'cost_pounds' => 30, 'email_notes' =>'Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info']);
        Course::create(['id' => 7, 'name' => 'P5 Course seven', 'course_provider_id' => 5, 'provider_course_ref' => 'CPLxx7', 'cost_pounds' => 10, 'email_notes' =>'Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info']);
        Course::create(['id' => 8, 'name' => 'P5 Course eight', 'course_provider_id' => 5, 'provider_course_ref' => 'CPLxx8', 'cost_pounds' => 20, 'email_notes' =>'Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info']);
        Course::create(['id' => 9, 'name' => 'P5 Course nine', 'course_provider_id' => 5, 'provider_course_ref' => 'CPLxx9', 'cost_pounds' => 40, 'email_notes' =>'Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info']);
        Course::create(['id' => 10, 'name' => 'P6 Course ten', 'course_provider_id' => 6, 'provider_course_ref' => 'CPLx10', 'cost_pounds' => 35, 'email_notes' =>'Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info']);

        // Populate some courses
        Course::create(['id' => 3, 'name' => 'CPL Course 3', 'course_provider_id' => 8, 'provider_course_ref' => 'CPLxx3', 'cost_pounds' => 10, 'email_notes' =>'Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info']);
        Course::create(['id' => 233, 'name' => 'CPL Course 233', 'course_provider_id' => 8, 'provider_course_ref' => 'CPLxx233', 'cost_pounds' => 20, 'email_notes' =>'Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info']);
        Course::create(['id' => 18, 'name' => 'CPL Course 18', 'course_provider_id' => 8, 'provider_course_ref' => 'CPLxx18', 'cost_pounds' => 30, 'email_notes' =>'Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info']);
        Course::create(['id' => 31, 'name' => 'CPL Course 31', 'course_provider_id' => 8, 'provider_course_ref' => 'CPLxx31', 'cost_pounds' => 10, 'email_notes' =>'Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info']);
        Course::create(['id' => 81, 'name' => 'CPL Course 81', 'course_provider_id' => 8, 'provider_course_ref' => 'CPLxx81', 'cost_pounds' => 20, 'email_notes' =>'Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info']);
        Course::create(['id' => 65, 'name' => 'CPL Course 65', 'course_provider_id' => 8, 'provider_course_ref' => 'CPLxx65', 'cost_pounds' => 40, 'email_notes' =>'Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info']);
        Course::create(['id' => 229, 'name' => 'CPL Course 229', 'course_provider_id' => 8, 'provider_course_ref' => 'CPLx229', 'cost_pounds' => 35, 'email_notes' =>'Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info</br>Lots of <b>useful</b> course info']);


        // Populate some course allocations
        $members = [$memberBob, $memberHarry, $memberMark];
        $courseIds = Course::all()->lists('id')->toArray();
        $statusIds = CourseAllocationStatus::all()->lists('id')->toArray();

        // These were all created 'yesterday' at 1 minute intervals
        $created_at = Carbon::now()->addDay(-1);
        for ($i = 0; $i < 100; $i++) {
            $member = $members[array_rand($members)];
            $status_id = $statusIds[array_rand($statusIds)];
            CourseAllocation::create([
                'member_id'   => $member["member_id"],
                'member_name' => $member["member_name"],
                'branch_id'   => $member["branch_id"],
                'branch_name' => $member["branch_name"],
                'course_id'   => $courseIds[array_rand($courseIds)],
                'created_at'  => $created_at->addMinute($i),
                'status_id'   => $status_id,
                'started_at' => $status_id >= 2 ? $created_at->addMinute($i+10) : null,
                'ended_at' => $status_id >= 3 ? $created_at->addMinute($i+20) : null
            ]);
        }
        // These were all created 'from last month' at 1 day intervals (for AllAllocations reports)
        $created_at = Carbon::now()->startOfMonth(); // First course will be one day before
        for ($i = 0; $i < 100; $i++) {
            $member = $members[array_rand($members)];
            CourseAllocation::create([
                'member_id'   => $member["member_id"],
                'member_name' => $member["member_name"],
                'branch_id'   => $member["branch_id"],
                'branch_name' => $member["branch_name"],
                'course_id'   => $courseIds[array_rand($courseIds)],
                'created_at'  => $created_at->subDay(1),
                'status_id'   => $statusIds[array_rand($statusIds)],
            ]);
        }

        ////////////////////
        // CPL Test Accounts

        CourseAllocation::create([
            'member_id'   => $memberCplTest1["member_id"],
            'member_name' => $memberCplTest1["member_name"],
            'branch_id'   => $memberCplTest1["branch_id"],
            'branch_name' => $memberCplTest1["branch_name"],
            'course_id'   => 3,
            'created_at'  => $created_at,
            'status_id'   => 1,
            'started_at' => null,
            'ended_at' => null
        ]);
        CourseAllocation::create([
            'member_id'   => $memberCplTest2["member_id"],
            'member_name' => $memberCplTest2["member_name"],
            'branch_id'   => $memberCplTest2["branch_id"],
            'branch_name' => $memberCplTest2["branch_name"],
            'course_id'   => 233,
            'created_at'  => $created_at,
            'status_id'   => 2,
            'started_at' => '2015-09-02 10:00:00',
            'ended_at' => null
        ]);
        CourseAllocation::create([
            'member_id'   => $memberCplTest2["member_id"],
            'member_name' => $memberCplTest2["member_name"],
            'branch_id'   => $memberCplTest2["branch_id"],
            'branch_name' => $memberCplTest2["branch_name"],
            'course_id'   => 18,
            'created_at'  => $created_at,
            'status_id'   => 4,
            'started_at' => '2015-09-09 09:00:00',
            'ended_at' => '2015-09-10 16:00:00'
        ]);
        CourseAllocation::create([
            'member_id'   => $memberCplTest3["member_id"],
            'member_name' => $memberCplTest3["member_name"],
            'branch_id'   => $memberCplTest3["branch_id"],
            'branch_name' => $memberCplTest3["branch_name"],
            'course_id'   => 31,
            'created_at'  => $created_at,
            'status_id'   => 3,
            'started_at' => '2015-09-23 14:00:00',
            'ended_at' => '2015-09-23 16:00:00'
        ]);
        CourseAllocation::create([
            'member_id'   => $memberCplTest4["member_id"],
            'member_name' => $memberCplTest4["member_name"],
            'branch_id'   => $memberCplTest4["branch_id"],
            'branch_name' => $memberCplTest4["branch_name"],
            'course_id'   => 31,
            'created_at'  => $created_at,
            'status_id'   => 2,
            'started_at' => '2015-09-10 09:00:00',
            'ended_at' => null
        ]);
        CourseAllocation::create([
            'member_id'   => $memberCplTest4["member_id"],
            'member_name' => $memberCplTest4["member_name"],
            'branch_id'   => $memberCplTest4["branch_id"],
            'branch_name' => $memberCplTest4["branch_name"],
            'course_id'   => 18,
            'created_at'  => $created_at,
            'status_id'   => 2,
            'started_at' => '2015-09-17 09:00:00',
            'ended_at' => null
        ]);
        CourseAllocation::create([
            'member_id'   => $memberCplTest5["member_id"],
            'member_name' => $memberCplTest5["member_name"],
            'branch_id'   => $memberCplTest5["branch_id"],
            'branch_name' => $memberCplTest5["branch_name"],
            'course_id'   => 81,
            'created_at'  => $created_at,
            'status_id'   => 1,
            'started_at' => null,
            'ended_at' => null
        ]);
        CourseAllocation::create([
            'member_id'   => $memberCplTest5["member_id"],
            'member_name' => $memberCplTest5["member_name"],
            'branch_id'   => $memberCplTest5["branch_id"],
            'branch_name' => $memberCplTest5["branch_name"],
            'course_id'   => 65,
            'created_at'  => $created_at,
            'status_id'   => 1,
            'started_at' => null,
            'ended_at' => null
        ]);
        CourseAllocation::create([
            'member_id'   => $memberCplTest5["member_id"],
            'member_name' => $memberCplTest5["member_name"],
            'branch_id'   => $memberCplTest5["branch_id"],
            'branch_name' => $memberCplTest5["branch_name"],
            'course_id'   => 229,
            'created_at'  => $created_at,
            'status_id'   => 1,
            'started_at' => null,
            'ended_at' => null
        ]);


        // Create super admin
        Admin::create([
            'member_id' => $memberRyan["member_id"],
            'can_edit'  => 1,
            'notes'     => 'Ryan - SuperUser'
        ]);
        // Create super viewer
        Admin::create([
            'member_id' => $memberDan["member_id"],
            'can_edit'  => 1,
            'notes'     => 'Dan - SuperUser'
        ]);

        $year = Carbon::today()->year;

        /////////////
        // Budgets //
        /////////////
        $budgets = [
            [ // row #0
                'branch_code' => 'ABC',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #0
                'branch_code' => 'ABC',
                'amount'      => 100,
                'year'        => $year - 1,
            ],
            [ // row #1
                'branch_code' => 'ABD',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #2
                'branch_code' => 'ABE',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #3
                'branch_code' => 'AMV',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #4
                'branch_code' => 'ASH',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #5
                'branch_code' => 'AVN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #6
                'branch_code' => 'AYL',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #7
                'branch_code' => 'AYR',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #8
                'branch_code' => 'BAR',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #9
                'branch_code' => 'BAT',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #10
                'branch_code' => 'BEE',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #11
                'branch_code' => 'BEL',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #12
                'branch_code' => 'BEN',
                'amount'      => 2000,
                'year'        => $year,
            ],
            [ // row #13
                'branch_code' => 'BES',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #14
                'branch_code' => 'BEV',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #15
                'branch_code' => 'BEW',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #16
                'branch_code' => 'BEX',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #17
                'branch_code' => 'BFW',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #18
                'branch_code' => 'BIR',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #19
                'branch_code' => 'BNS',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #20
                'branch_code' => 'BOL',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #21
                'branch_code' => 'BOT',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #22
                'branch_code' => 'BRA',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #23
                'branch_code' => 'BRI',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #24
                'branch_code' => 'BRO',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #25
                'branch_code' => 'BRR',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #26
                'branch_code' => 'BUN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #27
                'branch_code' => 'CAM',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #28
                'branch_code' => 'CAN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #29
                'branch_code' => 'CAR',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #30
                'branch_code' => 'CAT',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #31
                'branch_code' => 'CER',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #32
                'branch_code' => 'CHE',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #33
                'branch_code' => 'CHI',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #34
                'branch_code' => 'CHS',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #35
                'branch_code' => 'CLN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #36
                'branch_code' => 'CLV',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #37
                'branch_code' => 'CLW',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #38
                'branch_code' => 'CME',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #39
                'branch_code' => 'CNC',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #40
                'branch_code' => 'COL',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #41
                'branch_code' => 'COR',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #42
                'branch_code' => 'COV',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #43
                'branch_code' => 'CRA',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #44
                'branch_code' => 'CRO',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #45
                'branch_code' => 'CUM',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #46
                'branch_code' => 'DAD',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #47
                'branch_code' => 'DAR',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #48
                'branch_code' => 'DDS',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #49
                'branch_code' => 'DEN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #50
                'branch_code' => 'DER',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #51
                'branch_code' => 'DEV',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #52
                'branch_code' => 'DON',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #53
                'branch_code' => 'DUD',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #54
                'branch_code' => 'DUM',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #55
                'branch_code' => 'DUR',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #56
                'branch_code' => 'EDN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #57
                'branch_code' => 'EHA',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #58
                'branch_code' => 'ELC',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #59
                'branch_code' => 'ELY',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #60
                'branch_code' => 'ENF',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #61
                'branch_code' => 'ENW',
                'amount'      => 2000,
                'year'        => $year,
            ],
            [ // row #62
                'branch_code' => 'ERE',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #63
                'branch_code' => 'ESE',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #64
                'branch_code' => 'ESW',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #65
                'branch_code' => 'EXE',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #66
                'branch_code' => 'FEN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #67
                'branch_code' => 'FIF',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #68
                'branch_code' => 'FOV',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #69
                'branch_code' => 'FUR',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #70
                'branch_code' => 'GAT',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #71
                'branch_code' => 'GLA',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #72
                'branch_code' => 'GLM',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #73
                'branch_code' => 'GLO',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #74
                'branch_code' => 'GRA',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #75
                'branch_code' => 'GRN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #76
                'branch_code' => 'GUE',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #77
                'branch_code' => 'HAL',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #78
                'branch_code' => 'HAN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #79
                'branch_code' => 'HAR',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #80
                'branch_code' => 'HAS',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #81
                'branch_code' => 'HEB',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #82
                'branch_code' => 'HEN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #83
                'branch_code' => 'HES',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #84
                'branch_code' => 'HFD',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #85
                'branch_code' => 'HIN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #86
                'branch_code' => 'HIP',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #87
                'branch_code' => 'HLT',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #88
                'branch_code' => 'HOU',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #89
                'branch_code' => 'HOW',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #90
                'branch_code' => 'HUL',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #91
                'branch_code' => 'INV',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #92
                'branch_code' => 'IOM',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #93
                'branch_code' => 'IOW',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #94
                'branch_code' => 'JER',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #95
                'branch_code' => 'KEN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #96
                'branch_code' => 'KID',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #97
                'branch_code' => 'KIN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #98
                'branch_code' => 'KIR',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #99
                'branch_code' => 'KLN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #100
                'branch_code' => 'LAE',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #101
                'branch_code' => 'LAS',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #102
                'branch_code' => 'LAW',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #103
                'branch_code' => 'LEE',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #104
                'branch_code' => 'LEI',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #105
                'branch_code' => 'LGH',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #106
                'branch_code' => 'LIN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #107
                'branch_code' => 'LNE',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #108
                'branch_code' => 'LOU',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #109
                'branch_code' => 'LSE',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #110
                'branch_code' => 'LUN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #111
                'branch_code' => 'MAB',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #112
                'branch_code' => 'MAC',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #113
                'branch_code' => 'MAI',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #114
                'branch_code' => 'MAN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #115
                'branch_code' => 'MAS',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #116
                'branch_code' => 'MAT',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #117
                'branch_code' => 'MCH',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #118
                'branch_code' => 'MDX',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #119
                'branch_code' => 'MED',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #120
                'branch_code' => 'MER',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #121
                'branch_code' => 'MMD',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #122
                'branch_code' => 'MNS',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #123
                'branch_code' => 'MON',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #124
                'branch_code' => 'NCO',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #125
                'branch_code' => 'NDE',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #126
                'branch_code' => 'NEB',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #127
                'branch_code' => 'NEK',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #128
                'branch_code' => 'NEW',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #129
                'branch_code' => 'NLD',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #130
                'branch_code' => 'NON',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #131
                'branch_code' => 'NOR',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #132
                'branch_code' => 'NOT',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #133
                'branch_code' => 'NSX',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #134
                'branch_code' => 'NSY',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #135
                'branch_code' => 'NTH',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #136
                'branch_code' => 'NUN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #137
                'branch_code' => 'OXF',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #138
                'branch_code' => 'OXN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #139
                'branch_code' => 'PEM',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #140
                'branch_code' => 'PET',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #141
                'branch_code' => 'PLY',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #142
                'branch_code' => 'POR',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #143
                'branch_code' => 'POT',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #144
                'branch_code' => 'RAD',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #145
                'branch_code' => 'REA',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #146
                'branch_code' => 'RED',
                'amount'      => 2000,
                'year'        => $year,
            ],
            [ // row #147
                'branch_code' => 'REI',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #148
                'branch_code' => 'REN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #149
                'branch_code' => 'ROB',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #150
                'branch_code' => 'ROT',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #151
                'branch_code' => 'RUG',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #152
                'branch_code' => 'RUT',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #153
                'branch_code' => 'SAL',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #154
                'branch_code' => 'SCA',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #155
                'branch_code' => 'SCH',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #156
                'branch_code' => 'SCU',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #157
                'branch_code' => 'SDE',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #158
                'branch_code' => 'SEB',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #159
                'branch_code' => 'SED',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #160
                'branch_code' => 'SEL',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #161
                'branch_code' => 'SEV',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #162
                'branch_code' => 'SHA',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #163
                'branch_code' => 'SHB',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #164
                'branch_code' => 'SHE',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #165
                'branch_code' => 'SHF',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #166
                'branch_code' => 'SHR',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #167
                'branch_code' => 'SIR',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #168
                'branch_code' => 'SKI',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #169
                'branch_code' => 'SOL',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #170
                'branch_code' => 'SOU',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #171
                'branch_code' => 'SOW',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #172
                'branch_code' => 'SOX',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #173
                'branch_code' => 'STA',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #174
                'branch_code' => 'STH',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #175
                'branch_code' => 'STN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #176
                'branch_code' => 'STR',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #177
                'branch_code' => 'SUF',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #178
                'branch_code' => 'SUN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #179
                'branch_code' => 'SWA',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #180
                'branch_code' => 'SWD',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #181
                'branch_code' => 'SWL',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #182
                'branch_code' => 'SWM',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #183
                'branch_code' => 'SWN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #184
                'branch_code' => 'SWS',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #185
                'branch_code' => 'TAM',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #186
                'branch_code' => 'TAU',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #187
                'branch_code' => 'TAY',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #188
                'branch_code' => 'TEI',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #189
                'branch_code' => 'TEL',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #190
                'branch_code' => 'THA',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #191
                'branch_code' => 'TND',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #192
                'branch_code' => 'TRA',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #193
                'branch_code' => 'TTW',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #194
                'branch_code' => 'TYN',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #195
                'branch_code' => 'VAL',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #196
                'branch_code' => 'WAK',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #197
                'branch_code' => 'WAL',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #198
                'branch_code' => 'WAT',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #199
                'branch_code' => 'WAV',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #200
                'branch_code' => 'WHS',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #201
                'branch_code' => 'WIR',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #202
                'branch_code' => 'WLD',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #203
                'branch_code' => 'WLV',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #204
                'branch_code' => 'WNW',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #205
                'branch_code' => 'WOO',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #206
                'branch_code' => 'WOR',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #207
                'branch_code' => 'WSB',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #208
                'branch_code' => 'WSM',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #209
                'branch_code' => 'WSX',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #210
                'branch_code' => 'XYZ',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #211
                'branch_code' => 'YNW',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #212
                'branch_code' => 'YOR',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #213
                'branch_code' => 'FESTIVAL_GBBF',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #214
                'branch_code' => 'FESTIVAL_ABCD',
                'amount'      => 100,
                'year'        => $year,
            ],
            [ // row #215
                'branch_code' => 'FESTIVAL_EFGH',
                'amount'      => 100,
                'year'        => $year,
            ],
        ];

        foreach ($budgets as $budget) {
            Budget::create($budget);
        }

        // Add a national budget
        Budget::create(['branch_code' => 'NATIONAL', 'amount' => 30000, 'year' => $year]);

        Eloquent::reguard();
    }
}