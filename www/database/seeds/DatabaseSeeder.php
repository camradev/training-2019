<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();


        if( App::environment() === 'local' )
        {
            $this->call(DevSeeder::class);
        }

        // Phpunit
        if( App::environment() === 'testing' )
        {

        }

        if( App::environment() === 'staging' )
        {
            $this->call(DevSeeder::class);
        }


        Model::reguard();
    }
}
