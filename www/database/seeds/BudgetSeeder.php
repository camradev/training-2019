<?php

use Illuminate\Database\Seeder;
use App\Budget;
use Carbon\Carbon;

class BudgetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $year = Carbon::today()->year;
        $amount = 100;
        $budgets = [
            [ // row #1
                'branch_code' => 'ABC',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #1
                'branch_code' => 'ABD',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #2
                'branch_code' => 'ABE',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #3
                'branch_code' => 'AMV',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #4
                'branch_code' => 'ASH',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #5
                'branch_code' => 'AVN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #6
                'branch_code' => 'AYL',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #7
                'branch_code' => 'AYR',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #8
                'branch_code' => 'BAR',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #9
                'branch_code' => 'BAT',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #10
                'branch_code' => 'BEE',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #11
                'branch_code' => 'BEL',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #12
                'branch_code' => 'BEN',
                'amount'      => 2000,
                'year'        => $year,
            ],
            [ // row #13
                'branch_code' => 'BES',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #14
                'branch_code' => 'BEV',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #15
                'branch_code' => 'BEW',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #16
                'branch_code' => 'BEX',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #17
                'branch_code' => 'BFW',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #18
                'branch_code' => 'BIR',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #19
                'branch_code' => 'BNS',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #20
                'branch_code' => 'BOL',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #21
                'branch_code' => 'BOT',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #22
                'branch_code' => 'BRA',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #23
                'branch_code' => 'BRI',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #24
                'branch_code' => 'BRO',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #25
                'branch_code' => 'BRR',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #26
                'branch_code' => 'BUN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #27
                'branch_code' => 'CAM',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #28
                'branch_code' => 'CAN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #29
                'branch_code' => 'CAR',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #30
                'branch_code' => 'CAT',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #31
                'branch_code' => 'CER',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #32
                'branch_code' => 'CHE',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #33
                'branch_code' => 'CHI',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #34
                'branch_code' => 'CHS',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #35
                'branch_code' => 'CLN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #36
                'branch_code' => 'CLV',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #37
                'branch_code' => 'CLW',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #38
                'branch_code' => 'CME',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #39
                'branch_code' => 'CNC',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #40
                'branch_code' => 'COL',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #41
                'branch_code' => 'COR',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #42
                'branch_code' => 'COV',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #43
                'branch_code' => 'CRA',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #44
                'branch_code' => 'CRO',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #45
                'branch_code' => 'CUM',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #46
                'branch_code' => 'DAD',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #47
                'branch_code' => 'DAR',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #48
                'branch_code' => 'DDS',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #49
                'branch_code' => 'DEN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #50
                'branch_code' => 'DER',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #51
                'branch_code' => 'DEV',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #52
                'branch_code' => 'DON',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #53
                'branch_code' => 'DUD',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #54
                'branch_code' => 'DUM',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #55
                'branch_code' => 'DUR',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #56
                'branch_code' => 'EDN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #57
                'branch_code' => 'EHA',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #58
                'branch_code' => 'ELC',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #59
                'branch_code' => 'ELY',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #60
                'branch_code' => 'ENF',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #61
                'branch_code' => 'ENW',
                'amount'      => 2000,
                'year'        => $year,
            ],
            [ // row #62
                'branch_code' => 'ERE',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #63
                'branch_code' => 'ESE',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #64
                'branch_code' => 'ESW',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #65
                'branch_code' => 'EXE',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #66
                'branch_code' => 'FEN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #67
                'branch_code' => 'FIF',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #68
                'branch_code' => 'FOV',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #69
                'branch_code' => 'FUR',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #70
                'branch_code' => 'GAT',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #71
                'branch_code' => 'GLA',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #72
                'branch_code' => 'GLM',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #73
                'branch_code' => 'GLO',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #74
                'branch_code' => 'GRA',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #75
                'branch_code' => 'GRN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #76
                'branch_code' => 'GUE',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #77
                'branch_code' => 'HAL',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #78
                'branch_code' => 'HAN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #79
                'branch_code' => 'HAR',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #80
                'branch_code' => 'HAS',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #81
                'branch_code' => 'HEB',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #82
                'branch_code' => 'HEN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #83
                'branch_code' => 'HES',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #84
                'branch_code' => 'HFD',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #85
                'branch_code' => 'HIN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #86
                'branch_code' => 'HIP',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #87
                'branch_code' => 'HLT',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #88
                'branch_code' => 'HOU',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #89
                'branch_code' => 'HOW',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #90
                'branch_code' => 'HUL',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #91
                'branch_code' => 'INV',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #92
                'branch_code' => 'IOM',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #93
                'branch_code' => 'IOW',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #94
                'branch_code' => 'JER',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #95
                'branch_code' => 'KEN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #96
                'branch_code' => 'KID',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #97
                'branch_code' => 'KIN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #98
                'branch_code' => 'KIR',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #99
                'branch_code' => 'KLN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #$amount
                'branch_code' => 'LAE',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #101
                'branch_code' => 'LAS',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #102
                'branch_code' => 'LAW',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #103
                'branch_code' => 'LEE',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #104
                'branch_code' => 'LEI',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #105
                'branch_code' => 'LGH',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #106
                'branch_code' => 'LIN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #107
                'branch_code' => 'LNE',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #108
                'branch_code' => 'LOU',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #109
                'branch_code' => 'LSE',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #110
                'branch_code' => 'LUN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #111
                'branch_code' => 'MAB',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #112
                'branch_code' => 'MAC',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #113
                'branch_code' => 'MAI',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #114
                'branch_code' => 'MAN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #115
                'branch_code' => 'MAS',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #116
                'branch_code' => 'MAT',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #117
                'branch_code' => 'MCH',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #118
                'branch_code' => 'MDX',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #119
                'branch_code' => 'MED',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #120
                'branch_code' => 'MER',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #121
                'branch_code' => 'MMD',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #122
                'branch_code' => 'MNS',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #123
                'branch_code' => 'MON',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #124
                'branch_code' => 'NCO',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #125
                'branch_code' => 'NDE',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #126
                'branch_code' => 'NEB',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #127
                'branch_code' => 'NEK',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #128
                'branch_code' => 'NEW',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #129
                'branch_code' => 'NLD',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #130
                'branch_code' => 'NON',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #131
                'branch_code' => 'NOR',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #132
                'branch_code' => 'NOT',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #133
                'branch_code' => 'NSX',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #134
                'branch_code' => 'NSY',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #135
                'branch_code' => 'NTH',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #136
                'branch_code' => 'NUN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #137
                'branch_code' => 'OXF',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #138
                'branch_code' => 'OXN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #139
                'branch_code' => 'PEM',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #140
                'branch_code' => 'PET',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #141
                'branch_code' => 'PLY',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #142
                'branch_code' => 'POR',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #143
                'branch_code' => 'POT',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #144
                'branch_code' => 'RAD',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #145
                'branch_code' => 'REA',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #146
                'branch_code' => 'RED',
                'amount'      => 2000,
                'year'        => $year,
            ],
            [ // row #147
                'branch_code' => 'REI',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #148
                'branch_code' => 'REN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #149
                'branch_code' => 'ROB',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #150
                'branch_code' => 'ROT',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #151
                'branch_code' => 'RUG',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #152
                'branch_code' => 'RUT',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #153
                'branch_code' => 'SAL',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #154
                'branch_code' => 'SCA',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #155
                'branch_code' => 'SCH',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #156
                'branch_code' => 'SCU',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #157
                'branch_code' => 'SDE',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #158
                'branch_code' => 'SEB',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #159
                'branch_code' => 'SED',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #160
                'branch_code' => 'SEL',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #161
                'branch_code' => 'SEV',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #162
                'branch_code' => 'SHA',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #163
                'branch_code' => 'SHB',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #164
                'branch_code' => 'SHE',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #165
                'branch_code' => 'SHF',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #166
                'branch_code' => 'SHR',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #167
                'branch_code' => 'SIR',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #168
                'branch_code' => 'SKI',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #169
                'branch_code' => 'SOL',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #170
                'branch_code' => 'SOU',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #171
                'branch_code' => 'SOW',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #172
                'branch_code' => 'SOX',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #173
                'branch_code' => 'STA',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #174
                'branch_code' => 'STH',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #175
                'branch_code' => 'STN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #176
                'branch_code' => 'STR',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #177
                'branch_code' => 'SUF',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #178
                'branch_code' => 'SUN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #179
                'branch_code' => 'SWA',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #180
                'branch_code' => 'SWD',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #181
                'branch_code' => 'SWL',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #182
                'branch_code' => 'SWM',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #183
                'branch_code' => 'SWN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #184
                'branch_code' => 'SWS',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #185
                'branch_code' => 'TAM',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #186
                'branch_code' => 'TAU',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #187
                'branch_code' => 'TAY',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #188
                'branch_code' => 'TEI',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #189
                'branch_code' => 'TEL',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #190
                'branch_code' => 'THA',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #191
                'branch_code' => 'TND',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #192
                'branch_code' => 'TRA',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #193
                'branch_code' => 'TTW',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #194
                'branch_code' => 'TYN',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #195
                'branch_code' => 'VAL',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #196
                'branch_code' => 'WAK',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #197
                'branch_code' => 'WAL',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #198
                'branch_code' => 'WAT',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #199
                'branch_code' => 'WAV',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #200
                'branch_code' => 'WHS',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #201
                'branch_code' => 'WIR',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #202
                'branch_code' => 'WLD',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #203
                'branch_code' => 'WLV',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #204
                'branch_code' => 'WNW',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #205
                'branch_code' => 'WOO',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #206
                'branch_code' => 'WOR',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #207
                'branch_code' => 'WSB',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #208
                'branch_code' => 'WSM',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #209
                'branch_code' => 'WSX',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #211
                'branch_code' => 'YNW',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #212
                'branch_code' => 'YOR',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #213
                'branch_code' => 'UDS',
                'amount'      => $amount,
                'year'        => $year,
            ],
            [ // row #214
                'branch_code' => 'CMA',
                'amount'      => $amount,
                'year'        => $year,
            ],
        ];

        Eloquent::unguard();

        foreach ($budgets as $budget) {
            Budget::create($budget);
        }

        Eloquent::reguard();

    }
}
